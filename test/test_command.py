# Copyright (c) 2002, 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test the Command class
"""

__version__ = "$Revision: 537 $"
# $Source$
# $Id: test_command.py 537 2003-03-14 20:43:50Z bh $


import unittest

import support
support.initthuban()

from Thuban.UI.command import Command, ToolCommand

class MockContext:

    pass


class BaseCommandTest(unittest.TestCase):

    def setUp(self):
        self.command_args = None

    def command(self, *args):
        """Method to use as the command function.

        Bind all args except self to self.command_args as a tuple.
        """
        self.command_args = args

    def context(self):
        """Return a context object"""
        return MockContext()

class TestCommand(BaseCommandTest):

    def test_static_command(self):
        """Test Command object with no callbacks"""
        cmd = Command("do_something", "Do Something", self.command)
        self.assertEquals(cmd.Name(), "do_something")
        self.assertEquals(cmd.Title(), "Do Something")
        self.assertEquals(cmd.HelpText(), "")
        self.assertEquals(cmd.Icon(), "")
        self.failIf(cmd.IsDynamic())
        self.failIf(cmd.IsTool())

        context = self.context()
        self.assert_(cmd.Sensitive(context))
        self.failIf(cmd.Checked(context))

        # Execute the command with just the context
        cmd.Execute(context)
        self.assertEquals(self.command_args, (context,))

        # Execute the command with additional parameters
        cmd.Execute(context, "abc")
        self.assertEquals(self.command_args, (context, "abc"))

class TestDynamicCommand(BaseCommandTest):

    def setUp(self):
        BaseCommandTest.setUp(self)
        self.is_sensitive = 0
        self.is_checked = 0
        self.dynamic_text = ""

    def sensitive(self, context):
        return self.is_sensitive

    def checked(self, context):
        return self.is_checked

    def dyntext(self, context):
        return self.dynamic_text

    def context(self):
        """Return a context object"""
        return MockContext()

    def test_dynamic_sensitivity(self):
        """Test Command object with dynamic sensitivity"""
        cmd = Command("do_something", "Do Something", self.command,
                      sensitive = self.sensitive)
        self.assert_(cmd.IsDynamic())

        context = self.context()
        self.failIf(cmd.Sensitive(context))
        self.is_sensitive = 1
        self.assert_(cmd.Sensitive(context))

    def test_dynamic_checked(self):
        """Test Command object with dynamic checked flag"""
        cmd = Command("do_something", "Do Something", self.command,
                      checked = self.checked)
        self.assert_(cmd.IsDynamic())

        context = self.context()
        self.failIf(cmd.Checked(context))
        self.is_checked = 1
        self.assert_(cmd.Checked(context))

    def test_dynamic_title(self):
        """Test Command object with dynamic title"""
        cmd = Command("do_something", "Do Something", self.command,
                      dyntext = self.dyntext)
        self.assert_(cmd.IsDynamic())
        self.assert_(cmd.HasDynText())

        context = self.context()
        self.assertEquals(cmd.DynText(context), "")
        self.dynamic_text = "A Dynamic Title"
        self.assertEquals(cmd.DynText(context), "A Dynamic Title")

    def test_tool_command(self):
        """Test ToolCommand object"""
        cmd = ToolCommand("do_something", "Do Something", self.command,
                          checked = self.checked)
        self.assert_(cmd.IsDynamic())
        self.assert_(cmd.IsTool())

if __name__ == "__main__":
    unittest.main()
