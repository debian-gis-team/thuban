# Copyright (C) 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

"""Test for the xmlsupport.py module
"""

__version__ = "$Revision: 1280 $"
# $Source$
# $Id: test_xmlsupport.py 1280 2003-06-20 18:29:16Z bh $

import unittest

import xmlsupport
import support

class TestValidation(unittest.TestCase, xmlsupport.ValidationTest):

    def test_simple(self):
        """test xmlsupport validating valid XML

        The test succeeds if validate_data doesn't raise any exception
        """
        data = ('<?xml version="1.0" encoding="UTF-8"?>\n'
                '<!DOCTYPE session SYSTEM "thuban.dtd">\n'
                '<session title="empty session">\n</session>\n')
        self.validate_data(data)

    def test_invalid(self):
        """test xmlsupport validating invalid XML

        The test succeeds if validate_data raises an assertion error
        """
        data = ('<?xml version="1.0" encoding="UTF-8"?>\n'
                '<!DOCTYPE session SYSTEM "thuban.dtd">\n'
                '<session foo="bar">\n</session>\n')
        # only really run this test when pyRXP is available
        if xmlsupport.pyRXP is not None:
            self.assertRaises(AssertionError, self.validate_data, data)



class TestEventList(unittest.TestCase):

    """Test cases for sax_eventlist"""

    def test_even_list_simple(self):
        """Test sax_eventlist on very simple XML"""
        data = "\n".join(['<?xml version="1.0" encoding="UTF-8"?>'
                          '<!DOCTYPE session SYSTEM "thuban.dtd">'
                          '<session title="single map&amp;layer">'
                          '</session>'])

        self.assertEquals(xmlsupport.sax_eventlist(data = data),
                          [('start', (None, u'session'),
                            [((None, u'title'), u'single map&layer')]),
                           ('end', (None, u'session'))])

    def test_even_list_namespace(self):
        """Test sax_eventlist on XML with a default namespace"""
        data = "\n".join(['<?xml version="1.0" encoding="UTF-8"?>'
                          '<!DOCTYPE session SYSTEM "thuban.dtd">'
                          '<session title="single map&amp;layer"'
                            ' xmlns="http://example.com/example.dtd">'
                          '</session>'])

        self.assertEquals(xmlsupport.sax_eventlist(data = data),
                          [('start', (u'http://example.com/example.dtd',
                                      u'session'),
                            [((None, u'title'), u'single map&layer')]),
                           ('end', (u'http://example.com/example.dtd',
                                    u'session'))])

    def test_even_list_id_normalization(self):
        """Test sax_eventlist id normalization"""
        data1 = "\n".join(['<?xml version="1.0" encoding="UTF-8"?>'
                           '<!DOCTYPE session SYSTEM "thuban.dtd">'
                           '<session title="bla">'
                           '   <table id="foo"/>'
                           '   <tableref id="foo"/>'
                           '</session>'])

        data2 = "\n".join(['<?xml version="1.0" encoding="UTF-8"?>'
                           '<!DOCTYPE session SYSTEM "thuban.dtd">'
                           '<session title="bla">'
                           '   <table id="bar"/>'
                           '   <tableref id="bar"/>'
                           '</session>'])
        ids = [((None, "table"), (None, "id"))]
        idrefs = [((None, "tableref"), (None, "id"))]
        self.assertEquals(xmlsupport.sax_eventlist(data = data1, ids = ids,
                                                   idrefs = idrefs),
                          xmlsupport.sax_eventlist(data = data2, ids = ids,
                                                   idrefs = idrefs))

if __name__ == "__main__":
    # Fake the __file__ global because it's needed by a test
    import sys
    __file__ = sys.argv[0]
    support.run_tests()
