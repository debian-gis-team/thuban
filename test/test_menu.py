# Copyright (c) 2002 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test the Menu
"""

__version__ = "$Revision: 2205 $"
# $Source$
# $Id: test_menu.py 2205 2004-05-12 20:50:33Z jan $

import unittest

import support
support.initthuban()

from Thuban.UI.menu import Menu


class MenuTest(unittest.TestCase):

    def compare_menus(self, menu1, menu2):
        eq = self.assertEquals
        ass = self.assert_
        ass(isinstance(menu1, Menu))
        ass(isinstance(menu2, Menu))
        eq(menu1.name, menu2.name)
        self.compare_items(menu1.items, menu2.items)

    def compare_items(self, items1, items2):
        eq = self.assertEquals
        eq(len(items1), len(items2), "Item lists have different lengths")
        for i in range(len(items1)):
            i1 = items1[i]
            i2 = items2[i]
            if isinstance(i1, Menu):
                self.compare_menus(i1, i2)
            else:
                eq(i1, i2)

    def test(self):
        """Menu operations (adding new items and submenus)"""
        eq = self.assertEquals

        # Build a typical main menu for an application. Here we have
        # only the file menu as sub-menu at first.
        # Make sure we copy the file_items list so that it won't be
        # modified
        file_items = ["new", None, "open", "save"]
        menu = Menu("<main>", "<main>",
                    [Menu("file", "File", file_items[:])])

        # fetch the file submenu
        filemenu = menu.find_menu("file")
        # check whether it's the right one
        self.compare_menus(filemenu, Menu("file", "File", file_items[:]))

        # append a separator and an item to the file menu
        filemenu.InsertSeparator()
        filemenu.InsertItem("exit")
        file_items.extend([None, "exit"])
        self.compare_menus(filemenu, Menu("file", "File", file_items[:]))

        # append an item to the file menu
        filemenu.InsertItem("save_as", after="save")
        file_items.insert(4, "save_as")
        self.compare_menus(filemenu, Menu("file", "File", file_items[:]))

        # add a new sub-menu to the main menu
        help_items = ["about", "manual"]
        helpmenu = menu.InsertMenu("help", "Help")
        helpmenu.SetItems(help_items[:])

        self.compare_menus(menu, Menu("<main>", "<main>",
                                      [Menu("file", "File", file_items[:]),
                                       Menu("help", "Help", help_items[:])]))

        self.compare_menus(helpmenu, Menu("help", "Help", help_items[:]))

        # add new sub-menu after the file menu but before the help menu
        edit_items = ["cut", "copy", "paste"]
        editmenu = menu.InsertMenu("edit", "Edit", after="file")
        editmenu.SetItems(edit_items[:])

        self.compare_menus(editmenu, Menu("edit", "Edit", edit_items[:]))
        self.compare_menus(menu, Menu("<main>", "<main>",
                                      [Menu("file", "File", file_items[:]),
                                       Menu("edit", "Edit", edit_items[:]),
                                       Menu("help", "Help", help_items[:])]))

        # remove an item from the menu
        self.compare_menus(editmenu, Menu("edit", "Edit", edit_items[:]))
        editmenu.RemoveItem("copy")
        self.compare_menus(editmenu, Menu("edit", "Edit", ['cut', 'paste']))
        editmenu.InsertItem("copy", after="cut") # for convenience for
                                                 # the following tests

        # find-or-insert a menu
        self.compare_menus(menu, Menu("<main>", "<main>",
                                      [Menu("file", "File", file_items[:]),
                                       Menu("edit", "Edit", edit_items[:]),
                                       Menu("help", "Help", help_items[:])]))
        menu.FindOrInsertMenu("extensions", "Extensions")
        self.compare_menus(menu, Menu("<main>", "<main>",
                                      [Menu("file", "File", file_items[:]),
                                       Menu("edit", "Edit", edit_items[:]),
                                       Menu("help", "Help", help_items[:]),
                                       Menu("extensions", "Extensions", [])]))
        menu.FindOrInsertMenu("extensions", "Extensions")
        self.compare_menus(menu, Menu("<main>", "<main>",
                                      [Menu("file", "File", file_items[:]),
                                       Menu("edit", "Edit", edit_items[:]),
                                       Menu("help", "Help", help_items[:]),
                                       Menu("extensions", "Extensions", [])]))


if __name__ == "__main__":
    unittest.main()
