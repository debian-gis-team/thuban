# Copyright (c) 2002 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test the functions in Thuban.Lib.fileutil
"""

__version__ = "$Revision: 340 $"
# $Source$
# $Id: test_fileutil.py 340 2002-09-20 17:32:59Z bh $

import unittest

import support
support.initthuban()

from Thuban.Lib.fileutil import relative_filename_posix, relative_filename_nt

class TestRelativeFilename(unittest.TestCase):

    """Test cases for the relative_filename function."""

    def test_posix(self):
        """Test relative_filename posix version"""
        self.assertEquals(relative_filename_posix("/usr/local/lib/",
                                                  "/usr/local/lib/python"),
                          'python')
        self.assertEquals(relative_filename_posix("/usr/local/lib/",
                                                  "/usr/local/bin/python"),
                          '../bin/python')
        self.assertEquals(relative_filename_posix("/usr/local/lib/",
                                                  "/usr/bin/python"),
                          '../../bin/python')
        self.assertEquals(relative_filename_posix("/usr/local/lib/",
                                                  "/var/spool/mail"),
                          '/var/spool/mail')
        self.assertEquals(relative_filename_posix("/home/", "xyzzy"),
                          'xyzzy')
        self.assertRaises(TypeError,
                          relative_filename_posix, "home/", "/xyzzy")

    def test_nt(self):
        """Test relative_filename nt version"""
        self.assertEquals(relative_filename_nt(r"C:\Programme\Python",
                                               r"C:\Programme\Thuban"),
                          '..\\Thuban')
        self.assertEquals(relative_filename_nt(r"C:\Programme\Python",
                                               r"D:\Programme\Thuban"),
                          'D:\\Programme\\Thuban')
        self.assertEquals(relative_filename_nt(r"C:\Programme\Python",
                                               r"C:Programme"),
                          'C:Programme')
        # first argument is not an absolute filename
        self.assertRaises(TypeError, relative_filename_nt,
                          r"C:Programme\Python", r"C:Programme")
        # No drive letters
        self.assertRaises(TypeError, relative_filename_nt,
                          r"\Programme\Python", r"\Programme")


if __name__ == "__main__":
    unittest.main()

