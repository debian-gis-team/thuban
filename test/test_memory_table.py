# Copyright (c) 2002, 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test the MemoryTable class
"""

__version__ = "$Revision: 1662 $"
# $Source$
# $Id: test_memory_table.py 1662 2003-08-27 13:51:01Z bh $

import os
import unittest

import support
support.initthuban()

from Thuban.Model.table import MemoryTable, \
     FIELDTYPE_DOUBLE, FIELDTYPE_INT, FIELDTYPE_STRING
import dbflib



class TestMemoryTable(unittest.TestCase):

    def setUp(self):
        """Create a new dbf file. The name is in self.filename"""
        self.table = MemoryTable([("type", FIELDTYPE_STRING),
                                  ("value", FIELDTYPE_DOUBLE),
                                  ("code", FIELDTYPE_INT)],
                                 [("UNKNOWN", 0.0, 0),
                                  ("Foo", 0.5, -1),
                                  ("Foo", 0.25, 100),
                                  ("bar", 1e10, 17)])

    def test_num_rows(self):
        """Test MemoryTable.NumRows()"""
        self.assertEquals(self.table.NumRows(), 4)

    def test_num_columns(self):
        """Test MemoryTable.NumColumns()"""
        self.assertEquals(self.table.NumColumns(), 3)

    def test_columns(self):
        """Test MemoryTable.Columns()"""
        columns = self.table.Columns()
        self.assertEquals(columns[0].name, "type")
        self.assertEquals(columns[0].type, FIELDTYPE_STRING)
        self.assertEquals(columns[1].name, "value")
        self.assertEquals(columns[1].type, FIELDTYPE_DOUBLE)
        self.assertEquals(columns[2].name, "code")
        self.assertEquals(columns[2].type, FIELDTYPE_INT)

    def test_column(self):
        """Test MemoryTable.Column()"""
        # The Column method can be called with either an index or a name
        col = self.table.Column(2)
        self.assertEquals(col.name, "code")
        self.assertEquals(col.type, FIELDTYPE_INT)
        col = self.table.Column("value")
        self.assertEquals(col.name, "value")
        self.assertEquals(col.type, FIELDTYPE_DOUBLE)

    def test_has_column(self):
        """Test MemoryTable.HasColumn()"""
        # HasColumn
        self.failUnless(self.table.HasColumn("value"))
        self.failUnless(self.table.HasColumn(2))
        # HasColumn for non-exisiting columns
        self.failIf(self.table.HasColumn("non_existing_name"))
        self.failIf(self.table.HasColumn(100))

    def test_read_row_as_dict(self):
        """Test MemoryTable.ReadRowAsDict()"""
        self.assertEquals(self.table.ReadRowAsDict(1),
                          {"type": "Foo", "value": 0.5, "code": -1})

    def test_read_row_as_dict_row_count_mode(self):
        """Test MemoryTable.ReadRowAsDict() row count address mode"""
        self.assertEquals(self.table.ReadRowAsDict(1, row_is_ordinal = 1),
                          {"type": "Foo", "value": 0.5, "code": -1})

    def test_read_value(self):
        """Test MemoryTable.ReadValue()"""
        # The column in ReadValue may be given as either name or index
        self.assertEquals(self.table.ReadValue(2, 0), "Foo")
        self.assertEquals(self.table.ReadValue(3, "code"), 17)

    def test_read_value_row_count_mode(self):
        """Test MemoryTable.ReadValue() row count address mode"""
        # The column in ReadValue may be given as either name or index
        self.assertEquals(self.table.ReadValue(2, 0, row_is_ordinal = 1),
                          "Foo")
        self.assertEquals(self.table.ReadValue(3, "code", row_is_ordinal=1),
                          17)

    def test_row_id_to_ordinal(self):
        """Test MemoryTable.RowIdToOrdinal()"""
        self.assertEquals(self.table.RowIdToOrdinal(5), 5)

    def test_row_oridnal_to_id(self):
        """Test MemoryTable.RowOrdinalToId()"""
        self.assertEquals(self.table.RowOrdinalToId(5), 5)

    def test_value_range(self):
        """Test MemoryTable.ValueRange()"""
        self.assertEquals(self.table.ValueRange("code"), (-1, 100))
        self.assertEquals(self.table.ValueRange(1), (0, 1e10))

    def test_unique_values(self):
        """Test MemoryTable.UniqueValues()"""
        # The column can be specified by name or index
        self.assertEquals(self.table.UniqueValues("type"),
                          ["Foo", "UNKNOWN", "bar"])
        self.assertEquals(self.table.UniqueValues(2), [-1, 0, 17, 100])

    def test_write(self):
        """Test MemoryTable.write_record()"""
        # change only one field
        # TODO: acticate when implemented
        # table.write_record(2, {"type": "FARMS"})

        # check whether the table returns the new value
        # TODO: acticate when implemented
        #eq(table.read_record(2),
        #   {'type': "FARMS", "height": 400.44, "code": 2})

        # Check whether we can specify the record as a tuple
        self.table.write_record(3, ("HUTS", 111.11, 42))
        self.assertEquals(self.table.ReadRowAsDict(3),
                          {"type": "HUTS", "value": 111.11, "code": 42})

    def test_dependencies(self):
        """Test MemoryTable.Dependencies()"""
        # A MemoryTable doesn't have dependencies
        self.assertEquals(len(self.table.Dependencies()), 0)

    def test_title(self):
        """Test MemoryTable.Title()"""
        self.assertEquals(self.table.Title(), "MemoryTable")

if __name__ == "__main__":
    support.run_tests()
