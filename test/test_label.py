# Copyright (c) 2002 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test the Label and LabelLayer classes
"""

__version__ = "$Revision: 330 $"
# $Source$
# $Id: test_label.py 330 2002-09-20 14:30:34Z bh $

import unittest

import support
support.initthuban()

from Thuban.Model.messages import CHANGED
from Thuban.Model.label import Label, LabelLayer, \
     ALIGN_CENTER, ALIGN_BASELINE, ALIGN_LEFT, ALIGN_TOP


class TestLabel(unittest.TestCase):

    """Test cases for the Label class"""

    def test(self):
        """Test Label"""
        # The label objects are very simple. We just have to test
        # whether instantiating one assigns the correct values to the
        # instance variables
        label = Label(10.5, 234567.0, "Label Text",
                      ALIGN_CENTER, ALIGN_BASELINE)
        self.assertEquals(label.x, 10.5)
        self.assertEquals(label.y, 234567.0)
        self.assertEquals(label.text, "Label Text")
        self.assertEquals(label.halign, ALIGN_CENTER)
        self.assertEquals(label.valign, ALIGN_BASELINE)


class TestLabelLayer(unittest.TestCase, support.SubscriberMixin):

    """Test cases for LabelLayer"""

    def setUp(self):
        """Clear the messages list and create a LabelLayer as self.layer
        """
        self.clear_messages()
        self.layer = LabelLayer("A Label Layer")
        self.layer.Subscribe(CHANGED, self.subscribe_with_params, CHANGED)

    def tearDown(self):
        """Clear the messages list and explictly destroy self.layer"""
        self.layer.Destroy()
        self.clear_messages()

    def test_initial_state(self):
        """Test LabelLayer's initial state"""
        self.failIf(self.layer.WasModified())
        self.assertEquals(self.layer.Title(), "A Label Layer")
        self.assertEquals(self.layer.Labels(), [])
        self.check_messages([])

    def test_methods(self):
        """Test LabelLayer methods"""
        # first add a label
        self.layer.AddLabel(10.5, 234567.0, "Label Text")
        self.check_messages([(CHANGED,)])
        self.assertEquals(self.layer.Labels()[0].text, "Label Text")
        self.assert_(self.layer.WasModified())

        # add another one
        self.layer.AddLabel(-1000.125, 987654.0, "Another Label",
                            ALIGN_LEFT, ALIGN_TOP)
        self.check_messages([(CHANGED,),
                             (CHANGED,)])
        self.assertEquals(self.layer.Labels()[0].text, "Label Text")
        self.assertEquals(self.layer.Labels()[1].text, "Another Label")

        # remove one
        self.layer.RemoveLabel(0)
        self.check_messages([(CHANGED,),
                             (CHANGED,),
                             (CHANGED,)])
        self.assertEquals(self.layer.Labels()[0].text, "Another Label")

        

if __name__ == "__main__":
    unittest.main()
