#!/usr/bin/env python
#
# Copyright (C) 2002, 2003 by Intevation GmbH
# Authors:
# Thomas Koester <tkoester@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

"""
Unit test for range.py
"""

__version__ = "$Revision: 1421 $"
# $Source$
# $Id: test_range.py 1421 2003-07-15 14:46:55Z bh $

import unittest

import support
support.initthuban()

from Thuban.Model.range import Range, _inf

class RangeTest(unittest.TestCase, support.FloatComparisonMixin):

    def test_equal(self):
        """test if different ways to create a range yield equal ranges"""
        range = Range()
        self.assertEqual(range, Range(''))
        self.assertEqual(range, Range(']-oo;oo['))
        self.assertEqual(range, Range((']', -_inf, _inf, '[')))
        self.assertEqual(range, Range((']', '-oo', 'oo', '[')))
        range = Range('[0;1]')
        self.assertEqual(range, Range('[000;1.0]'))
        self.assertEqual(range, Range(('[', '0', 1.0, ']')))
        range2 = range
        self.assertEqual(range, range2)
        range3 = Range(range)
        self.assertEqual(range3, range2)
        range = Range(']0;99]')
        self.assertEqual(range, Range(']0;99E+00]'))
        self.assertEqual(range, Range(']0;9.9E+01]'))
        self.assertEqual(range, Range(']0;990E-01]'))
        self.assertEqual(range, Range(']0;.99E+02]'))
        self.assertEqual(range, Range((']', 0, '.99E+02', ']')))

    def test_different(self):
        """test if different ranges are different"""
        range = Range('[-1;1]')
        self.failIfEqual(range, Range(']-1;1]'))
        self.failIfEqual(range, Range('[-1;1['))
        self.failIfEqual(range, Range(']-1;1['))
        self.failIfEqual(range, Range('[0;1]'))
        self.failIfEqual(range, Range('[-1;0]'))

    def test_contains(self):
        """test value in range"""
        range = Range((']', '0', 99, ']'))
        for value in [-0.1, 0, 99.1]:
            self.failIf(value in range)
        for value in [0.1, 9.9, 99]:
            self.assert_(value in range)
        range = Range('[-oo;0]')
        for value in [0.1, float('1e100'), float('1e10000')]:
            self.failIf(value in range)
        for value in [0.0, float('-1e100'), float('-1e10000')]:
            self.assert_(value in range)

    def test_float(self):
        """test string to float conversion"""
        range = Range()
        self.assertFloatEqual(range.float('oo'), _inf)
        self.assertFloatEqual(range.float('-oo'), -_inf)
        for value in [-100000000000000000000l, 12345.6789, 0, 0.0, 5.30e20]:
            self.assertFloatEqual(range.float(str(value)), value)

    def test_short_range(self):
        """test if range parser handles short ranges"""
        range = Range('[0;0]')
        self.failIf(-0.001 in range)
        self.assert_(0 in range)
        self.failIf(0.001 in range)

    def test_bad_ranges(self):
        """test if range parser raises correct exception on bad ranges"""
        for range in [0, 1.1]:
            self.assertRaises(TypeError, Range, range)
        for range in ['x', ']x;9]', ']]0;1]', '[0;1]]', '[0;x]', '0',
                      '[0]', '[;]', '[0;]', '[;0]', '[1;0]',
                      ']0;0]', '[0;0[', ']0;0[',
                      ('[',), ('[', 0), ('[', 0, 1), ('[', 0, ']'),
                      ('', 0, 1, ']'), ('[', 0, 1, '')]:
            self.assertRaises(ValueError, Range, range)


if __name__ == "__main__":
    unittest.main()
