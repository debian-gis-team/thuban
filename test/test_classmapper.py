# Copyright (c) 2004 by Intevation GmbH
# Authors:
# Martin Schulze <joey@infodrom.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Test case for the Thuban ClassMapper.

"""

__version__ = "$Revision: 2298 $"
# $Source$
# $Id: test_classmapper.py 2298 2004-07-26 15:59:46Z joey $


import unittest

import support
support.initthuban()

from Thuban.Lib.classmapper import ClassMapper

class TestMapping(unittest.TestCase):

    def test_mapper(self):
        """
        Test ClassMapper
        """

        class MyClass:
            pass

        class MySecondClass:
            def hello(self):
                return "Hello World!"

        class MyThirdClass:
            def hello(self):
                return "Hello Earth!"

        mapping = ClassMapper()
        instance = MyClass()

        # See if an empty mapping really returns False
        #
        self.assertEqual(mapping.get(instance), None)
        self.assertEqual(mapping.has(instance), False)

        # See if an installed mapping works
        #
        mapping.add(MyClass, MySecondClass)
        self.assertEqual(mapping.get(instance), MySecondClass)
        self.assertEqual(mapping.has(instance), True)

        # Ensure that it's really the class we put in and the method
        # is available as expected.
        #
        myinst = mapping.get(instance)()
        self.assertEqual(myinst.hello(), "Hello World!")

        second = ClassMapper()

        # Test if a second mapper gets mixed ubp with the first one.
        #
        self.assertEqual(second.get(instance), None)

        second.add(MyClass, MyThirdClass)
        self.assertEqual(second.get(instance), MyThirdClass)
        self.assertEqual(second.has(instance), True)

        # Ensure that it's really the class we put in and the method
        # is available as expected.
        #
        myinst = second.get(instance)()
        self.assertEqual(myinst.hello(), "Hello Earth!")


if __name__ == "__main__":
    support.run_tests()
