# Copyright (C) 2003 by Intevation GmbH
# Authors:
# Frank Koormann <frank.koormann@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

"""
Test the CSV table export
"""

__version__ = "$Revision"
# $Source$
# $Id: test_csv_table.py 1398 2003-07-10 14:55:49Z jonathan $

import unittest

import support
support.initthuban()

from Thuban.Model.table import MemoryTable, \
     FIELDTYPE_DOUBLE, FIELDTYPE_INT, FIELDTYPE_STRING, \
     table_to_csv

class TestCSVTable(unittest.TestCase, support.FileTestMixin):

    def setUp(self):
        """Create a simple table and write to file."""
        self.table = MemoryTable([("type", FIELDTYPE_STRING),
                                  ("value", FIELDTYPE_DOUBLE),
                                  ("code", FIELDTYPE_INT)],
                                 [("UNKNOWN", 0.0, 0),
                                  ("Foo", 0.5, -1),
                                  ("Foo", 0.25, 100),
                                  ("bar", 1e10, 17)])

    def test_table_to_cvs(self):
        """Test table_to_csv()"""
        filename = self.temp_file_name("test_export_csv.csv")
        table_to_csv(self.table, filename)
        file = open(filename, "r")

        # Tile line
        line=file.readline()
        self.assertEquals(line,'#type,value,code\n')

        # Data lines
        line=file.readline()
        self.assertEquals(line,'UNKNOWN,0.0,0\n')

        line=file.readline()
        self.assertEquals(line,'Foo,0.5,-1\n')

        line=file.readline()
        self.assertEquals(line,'Foo,0.25,100\n')

        line=file.readline()
        self.assertEquals(line,'bar,10000000000.0,17\n')
        self.assertEquals(file.readline(),'')

        # save selected records
        table_to_csv(self.table, filename, [1, 3])
        file = open(filename, "r")

        # Tile line
        line=file.readline()
        self.assertEquals(line,'#type,value,code\n')

        # Data lines
        line=file.readline()
        self.assertEquals(line,'Foo,0.5,-1\n')

        line=file.readline()
        self.assertEquals(line,'bar,10000000000.0,17\n')
        self.assertEquals(file.readline(),'')
        
if __name__ == "__main__":
    support.run_tests()
