# Copyright (c) 2005 by Intevation GmbH
# Authors:
# Bernhard Reiter <bernhard@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test the functions for Thuban's string representation.
"""

__version__ = "$Revision: 2673 $"
# $Source$
# $Id: test_stringrepresentation.py 2673 2005-10-19 08:08:29Z bernhard $

import unittest

import support
support.initthuban()

import Thuban

class TestInternalEncoding(unittest.TestCase):
    """Test around the thuban default encoding."""

    def setUp(self):
        """Save the old internal encoding, so we can restore it."""
        self.saved_encoding=Thuban.get_internal_encoding()

    def tearDown(self):
        """Restore saved internal encoding."""
        Thuban.set_internal_encoding(self.saved_encoding)

    def test_notice_bad_internalencoding(self):
        bad_encoding="this-never-is-a-valid-encoding"
        self.assertRaises(LookupError, 
            Thuban.set_internal_encoding,bad_encoding)

if __name__ == "__main__":
    support.run_tests()
