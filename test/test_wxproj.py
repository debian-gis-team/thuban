# Copyright (C) 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

"""Test cases for wxproj"""

__version__ = "$Revision: 1864 $"
# $Source$
# $Id: test_wxproj.py 1864 2003-10-24 17:25:53Z bh $

import os
import unittest

import support
support.initthuban()

import shapelib
from wxproj import shape_centroid
from Thuban.Model.proj import Projection


class TestShapeCentroid(unittest.TestCase, support.FloatComparisonMixin):

    """Test cases for wxproj.shape_centroid()"""

    def setUp(self):
        filename = os.path.join("..", "Data", "iceland", "political.shp")
        self.shapefile = shapelib.ShapeFile(filename)
        self.map_proj = Projection(["proj=utm", "zone=26", "ellps=clrk66"])
        self.layer_proj = Projection(["proj=latlong",
                                      "to_meter=0.017453292519943295",
                                      "ellps=clrk66"])

    def test_no_proj(self):
        """Test shape_centroid without any projections"""
        self.assertFloatSeqEqual(shape_centroid(self.shapefile.cobject(), 0,
                                                None, None,
                                                1, 1, 0, 0),
                                 (-22.5514848648, 64.7794567309))

    def test_map_proj(self):
        """Test shape_centroid with map projection"""
        self.assertFloatSeqEqual(shape_centroid(self.shapefile.cobject(), 0,
                                                self.map_proj, None,
                                                1, 1, 0, 0),
                                 (711378, 7191110),
                                 epsilon = 1)

    def test_layer_proj(self):
        """Test shape_centroid with layer projection"""
        self.assertFloatSeqEqual(shape_centroid(self.shapefile.cobject(), 0,
                                                None, self.layer_proj,
                                                1, 1, 0, 0),
                                 (-22.5514848648, 64.7794567309))

    def test_both_proj(self):
        """Test shape_centroid with map and layer projection"""
        self.assertFloatSeqEqual(shape_centroid(self.shapefile.cobject(), 0,
                                                self.map_proj, self.layer_proj,
                                                1, 1, 0, 0),
                                 (711378, 7191110),
                                 epsilon=1)

    def test_invalid_shape_id(self):
        """Test shape_centroid without an invalid shape id"""
        self.assertRaises(ValueError, shape_centroid,
                          self.shapefile.cobject(), -1, None, None,
                          1, 1, 0, 0)
        self.assertRaises(ValueError, shape_centroid,
                          self.shapefile.cobject(), 1000000, None, None,
                          1, 1, 0, 0)

if __name__ == "__main__":
    support.run_tests()
