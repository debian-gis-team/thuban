# Copyright (c) 2002, 2003 by Intevation GmbH
# Authors:
# Frank Koormann <frank.koormann@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test the Thuban export calculations
"""

__version__ = "$Revision: 1454 $"
# $Source$
# $Id: test_export.py 1454 2003-07-18 14:41:04Z bh $

import unittest

import support
support.initthuban()

from Thuban.UI.viewport import output_transform

class TestScalebar(unittest.TestCase, support.FloatComparisonMixin):

    """Test cases for the Thuban export calculations
    """

    def test_output_transform(self):
        """Test output_transform()."""

        scale, offset, mapregion = output_transform(1.0, (0,0),
                                                   (200, 100), (200, 100))
        self.assertFloatEqual(0.3, scale)
        self.assertFloatSeqEqual((0.0, 0.0), offset)
        self.assertFloatSeqEqual((20.0, 20.0, 80.0, 80.0), mapregion)

        scale, offset, mapregion = output_transform(1.0, (0,0),
                                                   (200, 100), (100, 200))
        self.assertFloatEqual(0.16, scale)
        self.assertFloatSeqEqual((0.0, 0.0), offset)
        self.assertFloatSeqEqual((20.0, 20.0, 52.0, 52.0), mapregion)

        scale, offset, mapregion = output_transform(1.0, (5,5),
                                                   (200, 100), (100, 100))
        self.assertFloatEqual(0.16, scale)
        self.assertFloatSeqEqual((0.8, 0.8), offset)
        self.assertFloatSeqEqual((20.0, 20.0, 52.0, 52.0), mapregion)

        scale, offset, mapregion = output_transform(1.0, (0,0),
                                                   (200, 100), (200, 200))
        self.assertFloatEqual(0.52, scale)
        self.assertFloatSeqEqual((0.0, 0.0), offset)
        self.assertFloatSeqEqual((20, 20, 124.0, 124.0), mapregion)


if __name__ == "__main__":
    unittest.main()
