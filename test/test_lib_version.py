# Copyright (C) 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

""""""

__version__ = "$Revision: 2011 $"
# $Source$
# $Id: test_lib_version.py 2011 2003-12-03 09:46:27Z bh $

import unittest

import support
support.initthuban()

from Thuban.Lib.version import make_tuple


class TestMakeTuple(unittest.TestCase):

    def test(self):
        """Test Thuban.version.make_tuple()"""
        self.assertEquals(make_tuple("1.2"), (1, 2))
        self.assertEquals(make_tuple("1.2.3"), (1, 2, 3))
        self.assertEquals(make_tuple("2.4.0.7"), (2, 4, 0))
        self.assertEquals(make_tuple("1.2+cvs.20031111"), (1, 2))


if __name__ == "__main__":
    support.run_tests()
