# Copyright (C) 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

"""Test Thuban.Model.wellknowntext"""

__version__ = "$Revision: 1605 $"
# $Source$
# $Id: test_wellknowntext.py 1605 2003-08-19 11:00:40Z bh $

import unittest

import support
support.initthuban()

from Thuban.Model.wellknowntext import parse_wkt_thuban


class TestWKT(unittest.TestCase, support.FloatComparisonMixin):

    def test_point(self):
        """Test WKT parsing of POINT geometries"""
        wkt = "SRID=1;POINT(6554860 967048)"
        self.assertPointListEquals(parse_wkt_thuban(wkt),
                                   [[(6554860, 967048)]])

    def test_multilinestring(self):
        """Test WKT parsing of MULTILINESTRING geometries"""
        wkt = ("MULTILINESTRING(("
               "(6489598.51 867720.64,"
               "6489593.90 867837.42),"
               "(6489624.12 867714.83,"
               "6489629.83 867621.31,"
               "6489616.35 867552.57,"
               "6489608.31 867507.10),"
               "(6489902.89 868153.54,"
               "6489880.85 868167.12,"
               "6489837.96 868136.41,"
               "6489788.94 868071.37)))")
        self.assertPointListEquals(parse_wkt_thuban(wkt),
                                  [[(6489598.51, 867720.64),
                                    (6489593.90, 867837.42)],
                                   [(6489624.12, 867714.83),
                                    (6489629.83, 867621.31),
                                    (6489616.35, 867552.57),
                                    (6489608.31, 867507.10)],
                                   [(6489902.89, 868153.54),
                                    (6489880.85, 868167.12),
                                    (6489837.96, 868136.41),
                                    (6489788.94, 868071.37)]])

    def test_polygon(self):
        """Test WKT parsing of POLYGON geometries"""
        wkt = ("SRID=1;POLYGON((6545639.323878 961209.599602,"
               "6545630.08526 961213.426864,6545617.99819 961184.249912,"
               "6545627.236809 961180.422651,6545639.323878 961209.599602))")
        self.assertPointListEquals(parse_wkt_thuban(wkt),
                                   [[(6545639.323878, 961209.599602),
                                     (6545630.08526, 961213.426864),
                                     (6545617.99819, 961184.249912),
                                     (6545627.236809, 961180.422651),
                                     (6545639.323878, 961209.599602)]])

    def test_errors(self):
        """Test WKT parsing error detection"""
        # empty strings cause value errors
        self.assertRaises(ValueError, parse_wkt_thuban, "")
        # strings with only an SRID cause value errors
        self.assertRaises(ValueError, parse_wkt_thuban, "SRID=1")

        # Missing parentheses cause value errors
        self.assertRaises(ValueError, parse_wkt_thuban, "POINT(10 20")


if __name__ == "__main__":
    support.run_tests()
