# Copyright (c) 2002 by Intevation GmbH
# Authors:
# Frank Koormann <frank.koormann@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test the Thuban scalebar calculations
"""

__version__ = "$Revision: 1674 $"
# $Source$
# $Id: test_scalebar.py 1674 2003-08-28 13:07:58Z bh $

import unittest
import os

import support
support.initthuban()

from Thuban.Model.scalebar import deriveInterval, roundInterval

class TestScalebar(unittest.TestCase, support.FloatComparisonMixin):

    """Test cases for the Thuban scalebar calculations
    """

    def test_deriveInterval(self):
        """Test scalebar.deriveInterval()"""

        # Zero savety
        interval, unit = deriveInterval(100, 0.0)
        self.assertEquals(interval, -1)
        self.assertEquals(unit, '')

        # meters
        interval, unit = deriveInterval(100, 0.5)
        self.assertEquals(interval, 200)
        self.assertEquals(unit, 'm')
    
        # kilometer conversion
        interval, unit = deriveInterval(100, 0.05)
        self.assertEquals(interval, 2)
        self.assertEquals(unit, 'km')
    
    def test_roundInterval(self):
        """Test scalebar.roundInterval()"""
        
        # 0.0005
        interval, label = roundInterval(0.000545943795)
        self.assertFloatEqual(interval, 0.0005)
        self.assertEquals(label, '0.0005')
        
        # 0.005
        interval, label = roundInterval(0.00545943795)
        self.assertFloatEqual(interval, 0.005)
        self.assertEquals(label, '0.005')
            
        # 0.05
        interval, label = roundInterval(0.0545943795)
        self.assertFloatEqual(interval, 0.05)
        self.assertEquals(label, '0.05')
            
        # 0.5
        interval, label = roundInterval(0.545943795)
        self.assertFloatEqual(interval, 0.5)
        self.assertEquals(label, '0.5')
            
        # 5
        interval, label = roundInterval(5.45943795)
        self.assertFloatEqual(interval, 5)
        self.assertEquals(label, '5')
    
        # 50
        interval, label = roundInterval(54.5943795)
        self.assertFloatEqual(interval, 50)
        self.assertEquals(label, '50')
    
        # 500
        interval, label = roundInterval(545.943795)
        self.assertFloatEqual(interval, 500)
        self.assertEquals(label, '500')
    
        # 5000
        interval, label = roundInterval(5459.43795)
        self.assertFloatEqual(interval, 5000)
        self.assertEquals(label, '5000')
    
        # 50000
        interval, label = roundInterval(54594.3795)
        self.assertFloatEqual(interval, 50000)
        self.assertEquals(label, '50000')
    
        # 500000
        interval, label = roundInterval(545943.795)
        self.assertFloatEqual(interval, -1)
        self.assertEquals(label, '')

if __name__ == "__main__":
    unittest.main()

