import unittest
import os, sys
import support
from Thuban.Model.layer import Layer
from Thuban.Model.map import Map
from Thuban.Model.session import Session
from Thuban.UI.context import Context
from Thuban.Lib.connector import ConnectorError

from test_postgis_db import PostGISStaticTests
from Thuban.Model.postgisdb import PostGISShapeStore

mapscriptAvailable=True
try:
    import mapscript
    from Extensions.umn_mapserver.mf_export import thuban_to_map
    from Extensions.umn_mapserver.mapfile import MF_Map
except ImportError:
    mapscriptAvailable=False
    

class DummyMainWindow(object):
    def __init__(self, canvas):
        self.canvas = canvas

class DummyCanvas(object):
    def __init__(self, map):
        self.map = map
    def Map(self):
        return self.map
    def VisibleExtent (self):
        return self.map.BoundingBox()

class TestPostGISMFExport(PostGISStaticTests):
    def setUp(self):
        PostGISStaticTests.setUp(self)
        self.store = PostGISShapeStore(self.db, "political_multi")

    def testExport(self):
        if not mapscriptAvailable:
            raise support.SkipTest("Couldn't import mapscript module")
        session = Session("A Session")
        map = Map("A Map")
        session.AddMap(map)
        layer = Layer("PostGIS Layer", self.store)
        map.AddLayer(layer)
        mainwindow = DummyMainWindow(DummyCanvas(map))
        context = Context(None, session, mainwindow)
        mf = MF_Map(mapscript.mapObj(""))
        mf.set_size (600, 500)
        thuban_to_map (context, mf)
        try:
            map.Destroy()
        except ConnectorError:
            pass
        session.Destroy()

if __name__ == "__main__":
    support.run_tests()

