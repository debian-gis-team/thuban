# Copyright (c) 2002, 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test the Thuban.Model.color module
"""

__version__ = "$Revision: 1547 $"
# $Source$
# $Id: test_color.py 1547 2003-08-05 12:38:58Z bh $

import unittest

import support
support.initthuban()

from Thuban.Model.color import Color, Transparent


class TestColor(unittest.TestCase):

    def test(self):
        """Test Color"""
        # The color objects are very simple. We just have to test
        # whether instantiating one assigns the colors to the right
        # instance variables and whether the correct hex string is
        # produced
        color = Color(0, 0.5, 1.0)
        self.assertEquals(color.red, 0.0)
        self.assertEquals(color.green, 0.5)
        self.assertEquals(color.blue, 1.0)
        self.assertEquals(color.hex().lower(), "#007fff")

    def test_repr(self):
        """Test Color repr"""
        self.assertEquals(repr(Color(0, 0.5, 0.75)), "Color(0, 0.5, 0.75)")

    def test_equality(self):
        """Test Color equality testing"""
        self.failUnless(Color(0, 0, 0) == Color(0.0, 0.0, 0.0))
        self.failUnless(Color(0, 0.5, 1.0) == Color(0.0, 0.5, 1.0))
        self.failIf(Color(0, 0.5, 1.0) == Color(0.0, 0.5, 0.75))
        self.failIf(Color(0, 0.5, 1.0) == (0.0, 0.5, 1.0))
        self.failIf((0, 0.5, 1.0) == Color(0.0, 0.5, 1.0))

    def test_inequality(self):
        """Test Color inequality testing"""
        self.failIf(Color(0, 0, 0) != Color(0.0, 0.0, 0.0))
        self.failIf(Color(0, 0.5, 1.0) != Color(0.0, 0.5, 1.0))
        self.failUnless(Color(0, 0.5, 1.0) != Color(0.0, 0.5, 0.75))
        self.failUnless(Color(0, 0.5, 1.0) != (0.0, 0.5, 1.0))
        self.failUnless((0, 0.5, 1.0) != Color(0.0, 0.5, 1.0))

class TestTransparent(unittest.TestCase):

    def test_repr(self):
        """Test Transparent repr"""
        self.assertEquals(repr(Transparent), "Transparent")

    def test_hex(self):
        """Test Transparent.hex()"""
        self.assertEquals(Transparent.hex(), "None")

    def test_equality(self):
        """Test Transparent equality testing"""
        self.failUnless(Transparent == Transparent)
        self.failIf(Transparent == Color(0.0, 0.5, 1.0))
        self.failIf(Color(0.0, 0.5, 1.0) == Transparent)
        self.failIf(None == Transparent)
        self.failIf(Transparent == None)

    def test_inequality(self):
        """Test Transparent inequality testing"""
        self.failIf(Transparent != Transparent)
        self.failUnless(Transparent != Color(0.0, 0.5, 1.0))
        self.failUnless(Color(0.0, 0.5, 1.0) != Transparent)
        self.failUnless(None != Transparent)
        self.failUnless(Transparent != None)


if __name__ == "__main__":
    unittest.main()
