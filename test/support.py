# Copyright (c) 2002, 2003, 2004, 2005 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Support classes and function for the test suite
"""

__version__ = "$Revision: 2642 $"
# $Source$
# $Id: support.py 2642 2005-07-01 20:49:04Z bh $

import os, sys
import unittest
import traceback

import postgissupport


def thuban_dir():
    """Return the directory containing the Thuban package"""
    thisdir = os.path.dirname(__file__)
    return os.path.join(thisdir, os.pardir)

def resource_dir():
    return os.path.join(thuban_dir(), "Resources")

def add_thuban_dir_to_path():
    """Insert the Thuban directory at the beginning of the python path.

    If it's already part of the path, remove later occurrences.
    """
    dir = thuban_dir()
    while 1:
        try:
            sys.path.remove(dir)
        except ValueError:
            break
    sys.path.insert(0, dir)


_initthuban_done = 0
def initthuban():
    """Initialize the interpreter for using Thuban modules
    """
    global _initthuban_done
    if not _initthuban_done:
        # Thuban uses gettext to translate some strings. Some of these
        # strings are tested for equality in some test cases. So we
        # unset any LANG environment setting to make sure only the
        # untranslated messages are used.
        try:
            del os.environ["LANG"]
        except KeyError:
            pass
        add_thuban_dir_to_path()
        import thubaninit

        # Install a dummy translation function so that importing
        # Thuban.UI doesn't install a wx specific one for which would
        # need to import wxPython
        import Thuban
        Thuban.install_translation_function(lambda s: s)

        # For the time being the default encoding in the test suite is
        # latin 1.  This is mostly for historical reasons.  Other
        # encodings can be specified as an argument for runtests.py.
        Thuban.set_internal_encoding("latin-1")

        _initthuban_done = 1


#
# Special test runner and result that support skipping tests
#

class SkipTest(Exception):
    """Exception to raise in tests that are skipped for some reason

    For instance, since gdal support is optional, test cases that
    require gdal raise this exception to indicate that they are skipped.
    Skipped is different from failure or error in that it is expected
    under certain circumstances.
    """

class ThubanTestResult(unittest._TextTestResult):

    def __init__(self, stream, descriptions, verbosity):
        unittest._TextTestResult.__init__(self, stream, descriptions,
                                          verbosity)
        self.skipped_tests = {}

    def add_skipped_test(self, test, exc):
        reason = str(exc)
        self.skipped_tests.setdefault(reason, []).append(test)

    def count_skipped(self):
        sum = 0
        for tests in self.skipped_tests.values():
            sum += len(tests)
        return sum

    def addError(self, test, err):
        """Extend inherited method to handle SkipTest exceptions specially
        """
        #print "addError", test, err
        if isinstance(err[1], SkipTest):
            self.add_skipped_test(test, err[1])
            if self.showAll:
                self.stream.writeln("skipped")
            elif self.dots:
                self.stream.write('S')
        else:
            unittest._TextTestResult.addError(self, test, err)

    def printErrors(self):
        if self.skipped_tests:
            if self.dots or self.showAll:
                self.stream.writeln()
            self.stream.writeln("Skipped tests:")
            for reason, tests in self.skipped_tests.items():
                self.stream.writeln("  %s:" % reason)
                for test in tests:
                    self.stream.writeln("    " + test.id())
        unittest._TextTestResult.printErrors(self)

    def getDescription(self, test):
        return test.id()


class ThubanTestRunner(unittest.TextTestRunner):

    def _makeResult(self):
        return ThubanTestResult(self.stream, self.descriptions, self.verbosity)

    def run(self, test):
        result = unittest.TextTestRunner.run(self, test)
        self.stream.writeln("skipped = %d" % result.count_skipped())
        return result


class ThubanTestProgram(unittest.TestProgram):

    def runTests(self):
        """Extend inherited method so that we use a ThubanTestRunner"""
        self.testRunner = ThubanTestRunner(verbosity = self.verbosity)
        unittest.TestProgram.runTests(self)


def execute_as_testsuite(callable, *args, **kw):
    """Call callable  with args as if it were the entry point to the test suite

    Return watever callable returns.

    This is a helper function for run_tests and runtests.py. Call
    callable in a try-finally block and run some cleanup and print some
    additional information in the finally block.

    The additionaly information include:

     - A list of uncollected objects (after an explicit garbage
       collector call)

     - any unsubscribed messages
    """
    try:
        return callable(*args, **kw)
    finally:
        # This has to be in a finally clause because unittest.main()
        # ends with a sys.exit to make sure that the process exits with
        # an appropriate exit code

        # Shutdown the postgis server if it's running
        try:
            postgissupport.shutdown_test_server()
        except:
            traceback.print_exc()

        # Print additional information
        print_additional_summary()

def run_tests():
    """Frontend for unittest.main that prints some additional debug information

    After calling unittest.main, run the garbage collector and print
    uncollected objects. Also print any un-unsubscribed messages.
    """
    execute_as_testsuite(ThubanTestProgram)


def print_additional_summary():
    """Print some additional summary information after tests have been run"""
    print_garbage_information()
    import xmlsupport
    xmlsupport.print_summary_message()

def print_garbage_information():
    """Print information about things that haven't been cleaned up.

    Run the garbage collector and print uncollected objects. Also print
    any un-unsubscribed messages.
    """
    # this function may be called indirectly from test cases that test
    # test support modules which do not use anything from thuban itself,
    # so we call initthuban so that we can import the connector module
    initthuban()
    import gc, Thuban.Lib.connector
    gc.collect()
    if gc.garbage:
        print
        print "There are %d uncollected objects:" % len(gc.garbage)
        print gc.garbage
    Thuban.Lib.connector._the_connector.print_connections()

#

def create_temp_dir():
    """Create a temporary directory and return its name.

    The temporary directory is always called temp and is created in the
    directory where support module is located.

    If the temp directory already exists, just return the name.
    """
    name = os.path.abspath(os.path.join(os.path.dirname(__file__), "temp"))

    # if the directory already exists, we're done
    if os.path.isdir(name):
        return name

    # create the directory
    os.mkdir(name)
    return name


class FileTestMixin:

    """Mixin class for tests that use files in the temporary directory
    """

    def temp_file_name(self, basename):
        """Return the full name of the file named basename in the temp. dir"""
        return os.path.join(create_temp_dir(), basename)

    def temp_dir(self):
        """Return the name of the directory for the temporary files"""
        return create_temp_dir()



class FileLoadTestCase(unittest.TestCase, FileTestMixin):

    """Base class for test case that test loading files.

    This base class provides some common infrastructure for testing the
    reading of files.

    Each test case should be its own class derived from this one. There
    is one file associated with each class. The contents are defined by
    the file_contents class attribute and its name by the filename
    method.

    Derived classes usually only have to provide appropriate values for
    the file_contents and file_extension class attributes.
    """

    file_contents = None
    file_extension = ""

    def filename(self):
        """Return the name of the test file to use.

        The default implementation simply calls self.temp_file_name with
        a basename derived from the class name by stripping off a
        leading 'test_' and appending self.file_extension.
        """
        name = self.__class__.__name__
        if name.startswith("test_"):
            name = name[5:]
        return self.temp_file_name(name + self.file_extension)

    def setUp(self):
        """Create the volatile file for the test.

        Write self.contents (which should be a string) to the file named
        by self.filename().
        """
        filename = self.filename()
        file = open(filename, "w")
        file.write(self.file_contents)
        file.close()


class FloatComparisonMixin:

    """
    Mixin class for tests comparing floating point numbers.

    This class provides a few methods for testing floating point
    operations.
    """

    fp_epsilon = 1e-6
    fp_inf = float('1e1000')   # FIXME: hack for infinite

    def assertFloatEqual(self, test, value, epsilon = None):
        """Assert equality of test and value with some tolerance.

        Assert that the absolute difference between test and value is
        less than self.fp_epsilon.
        """
        if epsilon is None:
            epsilon = self.fp_epsilon
        if abs(test) == self.fp_inf:
            self.assertEqual(test, value)
        else:
            self.assert_(epsilon > abs(test - value),
                     "abs(%g - %g) >= %g" % (test, value, epsilon))

    def assertFloatSeqEqual(self, test, value, epsilon = None):
        """Assert equality of the sequences test and value with some tolerance.

        Assert that the absolute difference between each corresponding
        value in test and value is less than the optional parameter
        epsilon. If epsilon is not given use self.fp_epsilon.
        """
        self.assertEquals(len(test), len(value))
        for i in range(len(test)):
            self.assertFloatEqual(test[i], value[i], epsilon)

    def assertPointListEquals(self, test, value):
        """Assert equality of two lists of lists of tuples of float

        This assertion is usually used to compare the geometry of shapes
        as returned by a Shape object's Points() method, hence the name.
        """
        for i in range(len(test)):
            self.assertEquals(len(test[i]), len(value[i]))
            for j in range(len(test[i])):
                self.assertFloatSeqEqual(test[i][j], value[i][j])


class SubscriberMixin:

    """Mixin class for tests for messages sent through the Connector

    The SubscriberMixin has some methods that can be used as subscribers
    of events that when called append information about the message into
    a list of messages received.

    A derived class should call the clear_messages() method in both its
    setUp and tearDown methods to clear the list of messages received.
    """

    def clear_messages(self):
        """Clear the list of received messages.

        Call this at least in the tests setUp and tearDown methods. It's
        important to do it in tearDown too because otherwise there may
        be cyclic references.
        """
        self.received_messages = []

    def subscribe_no_params(self):
        """Method for subscriptions without parameters.

        Add an empty tuple to the list of received messages.
        """
        self.received_messages.append(())

    def subscribe_with_params(self, *args):
        """Method for subscriptions with parameters.

        Append the tuple will all arguments to this function (except for
        the self argument) to the list of received messages.
        """
        self.received_messages.append(args)

    def check_messages(self, messages):
        """Check whether the messages received match the list messages"""
        self.assertEquals(messages, self.received_messages)

