# Copyright (C) 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

"""Test cases for the mockgeo helper objects"""

__version__ = "$Revision: 1585 $"
# $Source$
# $Id: test_mockgeo.py 1585 2003-08-15 10:26:40Z bh $

import unittest

import mockgeo

import support

class TestAffineProjection(unittest.TestCase, support.FloatComparisonMixin):

    def test_forward(self):
        """Test AffineProjection.Forward()"""
        proj = mockgeo.AffineProjection((0.25, 0.1, 2, 0.125, 100.75, 123.25))
        self.assertEquals(proj.Forward(0, 0), (100.75, 123.25))
        self.assertEquals(proj.Forward(10.0, 20.0), (143.25, 126.75))
        self.assertEquals(proj.Forward(30, 0.125), (108.5, 126.265625))

    def test_inverse(self):
        """Test AffineProjection.Inverse()"""
        proj = mockgeo.AffineProjection((0.25, 0.1, 2, 0.125, 100.75, 123.25))
        self.assertEquals(proj.Inverse(100.75, 123.25), (0, 0))
        self.assertEquals(proj.Inverse(143.25, 126.75), (10.0, 20.0))
        self.assertFloatSeqEqual(proj.Inverse(108.5, 126.265625), (30, 0.125))

    def test_int_coeff(self):
        """Test AffineProjection with integer coefficients"""
        proj = mockgeo.AffineProjection((1, 1, -1, 1, 0, 0))
        # The determinant is an int too but must be treated as a float
        # when dividing by it otherwise some coefficients in the inverse
        # become wrong.
        self.assertEquals(proj.Inverse(10, 10), (10, 0))



if __name__ == "__main__":
    support.run_tests()
