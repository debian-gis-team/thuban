# Copyright (c) 2002, 2003, 2005 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test loading a thuban session from a file generated by Thuban 0.8.0 or 0.8.1
"""

__version__ = "$Revision: 2642 $"
# $Source$
# $Id: test_load_0_8.py 2642 2005-07-01 20:49:04Z bh $

import os
import unittest

import support
support.initthuban()

import dbflib

from Thuban import internal_from_unicode
from Thuban.Model.load import load_session, parse_color, LoadError
from Thuban.Model.color import Transparent
from Thuban.Model.classification import ClassGroupProperties, ClassGroupRange,\
    ClassGroupSingleton, ClassGroupDefault


def filenames_equal(name1, name2):
    """Return true if the filenames name1 and name2 are equal.

    On systems where it is available, simply use os.path.samefile,
    otherwise return whether the normalized versions of the filenames
    according to os.path.normpath are equal.
    """
    if hasattr(os.path, "samefile"):
        return os.path.samefile(name1, name2)
    return os.path.normpath(name1) == os.path.normpath(name2)



class LoadSessionTest(support.FileLoadTestCase):

    """Base class for .thuban file loading tests

    Basically the same as the FileLoadTestCase, except that all tests
    use the '.thuban' extension by default and that setUp and tearDown
    handle sessions.
    """

    file_extension = ".thuban"

    def setUp(self):
        """Create the test files"""
        support.FileLoadTestCase.setUp(self)
        self.session = None

    def tearDown(self):
        if self.session is not None:
            self.session.Destroy()
        self.session = None


    dtd = "http://thuban.intevation.org/dtds/thuban-0.8.dtd"
    thubanids = [((dtd, n), (None, "id")) for n in
                 ["fileshapesource", "filetable", "jointable",
                  "derivedshapesource"]]
    thubanidrefs = [((dtd, n), (None, m)) for n, m in
                    [("layer", "shapestore"),
                     ("jointable", "left"),
                     ("jointable", "right"),
                     ("derivedshapesource", "table"),
                     ("derivedshapesource", "shapesource")]]
    del n, m, dtd


class ClassificationTest(LoadSessionTest):

    """
    Base class for tests that do some detailed checking of classifications
    """

    def TestLayers(self, layers, expected):
        TITLE = 0
        NUM_GROUPS = 1
        CLASSES = 2
        GROUP_TYPE = 0
        GROUP_DATA = 1
        GROUP_LABEL = 2
        GROUP_PROPS = 3

        eq = self.assertEquals

        eq(len(layers), len(expected))

        for layer, data in zip(layers, expected):
            eq(layer.Title(), data[TITLE])

            clazz = layer.GetClassification()
            eq(clazz.GetNumGroups(), data[NUM_GROUPS])
            eq(clazz.GetNumGroups() + 1, len(data[CLASSES]))

            i = 0
            for group in clazz:
                props = ClassGroupProperties()
                props.SetLineColor(
                    parse_color(data[CLASSES][i][GROUP_PROPS][0]))
                props.SetLineWidth(data[CLASSES][i][GROUP_PROPS][1])
                props.SetFill(
                    parse_color(data[CLASSES][i][GROUP_PROPS][2]))

                if data[CLASSES][i][GROUP_TYPE] == "default":
                    g = ClassGroupDefault(props, data[CLASSES][i][GROUP_LABEL])
                elif data[CLASSES][i][GROUP_TYPE] == "range":
                    g = ClassGroupRange((data[CLASSES][i][GROUP_DATA][0],
                                         data[CLASSES][i][GROUP_DATA][1]),
                                        props, data[CLASSES][i][GROUP_LABEL])
                elif data[CLASSES][i][GROUP_TYPE] == "single":
                    g = ClassGroupSingleton(data[CLASSES][i][GROUP_DATA],
                                          props, data[CLASSES][i][GROUP_LABEL])

                eq(group, g)

                i += 1



class TestSingleLayer(LoadSessionTest):

    file_contents = '''\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE session SYSTEM "thuban-0.8.dtd">
<session xmlns="http://thuban.intevation.org/dtds/thuban-0.8.dtd"
        title="single map&amp;layer">
    <fileshapesource filetype="shapefile" id="D1"
        filename="../../Data/iceland/political.shp"/>
    <map title="Test Map">
        <projection name="Unknown">
            <parameter value="zone=26"/>
            <parameter value="proj=utm"/>
            <parameter value="ellps=clrk66"/>
        </projection>
        <layer shapestore="D1" visible="true"
                stroke="#000000" title="My Layer" stroke_width="1"
                fill="None"/>
    </map>
</session>
'''

    def test(self):
        """Load a session with a single map with a single layer"""
        eq = self.assertEquals
        session = load_session(self.filename())
        self.session = session

        # Check the title
        eq(session.Title(), "single map&layer")

        # the session has one map.
        maps = session.Maps()
        eq(len(maps), 1)

        # Check the map's attributes
        map = maps[0]
        eq(map.Title(), "Test Map")

        # the map has a single layer
        layers = map.Layers()
        eq(len(layers), 1)

        # Check the layer attributes
        layer = layers[0]
        eq(layer.Title(), "My Layer")
        self.failUnless(filenames_equal(layer.ShapeStore().FileName(),
                                        os.path.join(self.temp_dir(),
                                                     os.pardir, os.pardir,
                                                     "Data", "iceland",
                                                     "political.shp")))
        eq(layer.GetClassification().GetDefaultFill(), Transparent)
        eq(layer.GetClassification().GetDefaultLineColor().hex(), "#000000")
        eq(layer.Visible(), True)

        self.session.Destroy()
        self.session = None


class TestLayerVisibility(LoadSessionTest):

    file_contents = '''\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE session SYSTEM "thuban-0.8.dtd">
<session xmlns="http://thuban.intevation.org/dtds/thuban-0.8.dtd"
        title="single map&amp;layer">
    <fileshapesource filetype="shapefile" id="D1"
        filename="../../Data/iceland/political.shp"/>
    <map title="Test Map">
        <projection name="Unknown">
            <parameter value="zone=26"/>
            <parameter value="proj=utm"/>
            <parameter value="ellps=clrk66"/>
        </projection>
        <layer shapestore="D1" visible="false" stroke="#000000"
                title="My Layer" stroke_width="1" fill="None"/>
    </map>
</session>
'''

    def test(self):
        """Test that the visible flag is correctly loaded for a layer."""
        eq = self.assertEquals
        session = load_session(self.filename())
        self.session = session
        maps = session.Maps()
        eq(len(maps), 1)
        map = maps[0]
        layers = map.Layers()
        eq(len(layers), 1)
        layer = layers[0]

        eq(layer.Visible(), False)


class TestClassification(ClassificationTest):

    file_contents = '''\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE session SYSTEM "thuban.dtd">
<session title="single map&amp;layer">
	<map title="Test Map">
		<projection>
			<parameter value="zone=26"/>
			<parameter value="proj=utm"/>
			<parameter value="ellps=clrk66"/>
		</projection>
		<layer title="My Layer" stroke_width="1" fill="None"
                    filename="../../Data/iceland/political.shp"
                    stroke="#000000">
            <classification field="POPYREG" field_type="string">
                <clnull>
                    <cldata stroke="#000000" stroke_width="1" fill="None"/>
                </clnull>
                <clpoint value="1">
                    <cldata stroke="#000000" stroke_width="2" fill="None"/>
                </clpoint>
                <clpoint value="1">
                    <cldata stroke="#000000" stroke_width="10" fill="None"/>
                </clpoint>
            </classification>
        </layer>
		<layer title="My Layer 2" stroke_width="1" fill="None"
                    filename="../../Data/iceland/political.shp"
                    stroke="#000000">
            <classification field="AREA" field_type="double">
                <clnull>
                    <cldata stroke="#000000" stroke_width="2" fill="None"/>
                </clnull>
                <clrange min="0" max="1">
                    <cldata stroke="#111111" stroke_width="1" fill="None"/>
                </clrange>
                <clpoint value=".5">
                    <cldata stroke="#000000" stroke_width="1" fill="#111111"/>
                </clpoint>
                <clrange min="-1" max="0">
                    <cldata stroke="#000000" stroke_width="1" fill="None"/>
                </clrange>
                <clpoint value="-.5">
                    <cldata stroke="#000000" stroke_width="1" fill="None"/>
                </clpoint>
            </classification>
        </layer>
	</map>
</session>
'''

    def test(self):
        """Load a Thuban session with a map and classified layers."""
        session = load_session(self.filename())
        self.session = session

        map = self.session.Maps()[0] # only one map in the sample

        expected = [("My Layer", 2,
                        [("default", (), "",
                            ("#000000", 1, "None")),
                         ("single", "1", "",
                            ("#000000", 2, "None")),
                         ("single", "1", "",
                            ("#000000", 10, "None"))]),
                     ("My Layer 2", 4,
                         [("default", (), "",
                            ("#000000", 2, "None")),
                          ("range", (0, 1), "",
                            ("#111111", 1, "None")),
                          ("single", .5, "",
                            ("#000000", 1, "#111111")),
                          ("range", (-1, 0), "",
                            ("#000000", 1, "None")),
                          ("single", -.5, "",
                            ("#000000", 1, "None"))])]

        self.TestLayers(map.Layers(), expected)


class TestLabels(ClassificationTest):

    file_contents = '''\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE session SYSTEM "thuban-0.8.dtd">
<session xmlns="http://thuban.intevation.org/dtds/thuban-0.8.dtd"
        title="single map&amp;layer">
    <fileshapesource filetype="shapefile" id="D1"
        filename="../../Data/iceland/political.shp"/>
    <map title="Test Map">
        <projection name="Unknown">
            <parameter value="zone=26"/>
            <parameter value="proj=utm"/>
            <parameter value="ellps=clrk66"/>
        </projection>
        <layer shapestore="D1" visible="true" stroke="#000000"
                title="My Layer" stroke_width="1" fill="None">
            <classification field="POPYREG" field_type="string">
                <clnull label="hallo">
                    <cldata stroke="#000000" stroke_width="1" fill="None"/>
                </clnull>
                <clpoint label="welt" value="1">
                    <cldata stroke="#000000" stroke_width="2" fill="None"/>
                </clpoint>
            </classification>
        </layer>
    </map>
</session>
'''

    def test(self):
        """Load a session and test for reading the group labels."""
        eq = self.assertEquals
        session = load_session(self.filename())
        self.session = session

        map = self.session.Maps()[0] # only one map in the sample

        expected = [("My Layer", 1,
                        [("default", (), "hallo",
                            ("#000000", 1, "None")),
                         ("single", "1", "welt",
                            ("#000000", 2, "None"))])]

        self.TestLayers(map.Layers(), expected)


class TestLayerProjection(LoadSessionTest):

    file_contents = '''\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE session SYSTEM "thuban-0.8.dtd">
<session xmlns="http://thuban.intevation.org/dtds/thuban-0.8.dtd"
        title="single map&amp;layer">
    <fileshapesource filetype="shapefile" id="D2"
        filename="../../Data/iceland/roads-line.shp"/>
    <fileshapesource filetype="shapefile" id="D4"
        filename="../../Data/iceland/political.shp"/>
    <map title="Test Map">
        <projection name="Unknown">
            <parameter value="zone=26"/>
            <parameter value="proj=utm"/>
            <parameter value="ellps=clrk66"/>
        </projection>
        <layer shapestore="D4" visible="true" stroke="#000000"
                title="My Layer" stroke_width="1" fill="None">
            <projection name="hello">
                <parameter value="zone=13"/>
                <parameter value="proj=tmerc"/>
                <parameter value="ellps=clrk66"/>
            </projection>
            <classification field="POPYREG" field_type="string">
                <clnull label="hallo">
                    <cldata stroke="#000000" stroke_width="1" fill="None"/>
                </clnull>
                <clpoint label="welt" value="1">
                    <cldata stroke="#000000" stroke_width="2" fill="None"/>
                </clpoint>
            </classification>
        </layer>
        <layer shapestore="D2" visible="true" stroke="#000000"
                title="My Layer" stroke_width="1" fill="None">
            <projection name="Unknown">
                <parameter value="proj=lcc"/>
                <parameter value="lat_1=10"/>
                <parameter value="lat_2=20"/>
                <parameter value="ellps=clrk66"/>
            </projection>
        </layer>
    </map>
</session>
'''

    def test(self):
        """Test loading layers with projections"""
        eq = self.assertEquals
        neq = self.assertNotEqual

        session = load_session(self.filename())
        self.session = session

        map = self.session.Maps()[0] # only one map in the sample

        layers = map.Layers() # two layers in the sample

        # test layer with a named projection
        proj = layers[0].GetProjection()
        neq(proj, None)
        eq(proj.GetName(), "hello")
        eq(proj.GetParameter("proj"), "tmerc")
        eq(proj.GetParameter("zone"), "13")
        eq(proj.GetParameter("ellps"), "clrk66")

        # test layer with an unnamed projection
        proj = layers[1].GetProjection()
        neq(proj, None)
        eq(proj.GetName(), "Unknown")
        eq(proj.GetParameter("proj"), "lcc")
        eq(proj.GetParameter("lat_1"), "10")
        eq(proj.GetParameter("lat_2"), "20")
        eq(proj.GetParameter("ellps"), "clrk66")


class TestRasterLayer(LoadSessionTest):

    file_contents = '''\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE session SYSTEM "thuban-0.8.dtd">
<session xmlns="http://thuban.intevation.org/dtds/thuban-0.8.dtd"
        title="single map&amp;layer">
    <map title="Test Map">
        <rasterlayer visible="false" filename="../../Data/iceland/island.tif"
                title="My RasterLayer"/>
    </map>
</session>
'''

    def test(self):
        eq = self.assertEquals
        neq = self.assertNotEqual

        session = load_session(self.filename())
        self.session = session

        map = self.session.Maps()[0] # only one map in the sample

        layer = map.Layers()[0] # one layer in the sample

        eq(layer.Title(), "My RasterLayer")
        self.failIf(layer.Visible())
        self.failUnless(filenames_equal(layer.GetImageFilename(),
                                        os.path.join(self.temp_dir(),
                                                     os.pardir, os.pardir,
                                                     "Data", "iceland",
                                                     "island.tif")))

class TestJoinedTable(LoadSessionTest):

    file_contents = '''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE session SYSTEM "thuban-0.8.dtd">
<session xmlns="http://thuban.intevation.org/dtds/thuban-0.8.dtd" title="A Joined Table session">
    <fileshapesource filetype="shapefile" id="D137227612"
        filename="../../Data/iceland/roads-line.shp"/>
    <filetable filetype="DBF" filename="load_joinedtable.dbf" id="D136171140"
        title="Some Title"/>
    <jointable leftcolumn="RDLNTYPE" right="D136171140"
        title="Joined" rightcolumn="RDTYPE" id="D136169900" left="D137227612"/>
    <derivedshapesource table="D136169900" shapesource="D137227612"
        id="D136170932"/>
    <map title="Test Map">
        <layer shapestore="D136170932" visible="true" stroke="#000000"
                title="My Layer" stroke_width="1" fill="None"/>
    </map>
</session>
'''

    def setUp(self):
        """Extend inherited method to create the dbffile for the join"""
        LoadSessionTest.setUp(self)
        dbffile = self.temp_file_name("load_joinedtable.dbf")
        dbf = dbflib.create(dbffile)
        dbf.add_field("RDTYPE", dbflib.FTInteger, 10, 0)
        dbf.add_field("TEXT", dbflib.FTString, 10, 0)
        dbf.write_record(0, {'RDTYPE': 8, "TEXT": "foo"})
        dbf.write_record(1, {'RDTYPE': 2, "TEXT": "bar"})
        dbf.write_record(2, {'RDTYPE': 3, "TEXT": "baz"})
        dbf.close()

    def test(self):
        """Test loading a session containing a joined table"""
        session = load_session(self.filename())
        self.session = session

        tables = session.Tables()
        self.assertEquals(len(tables), 3)
        # FIXME: The tests shouldn't assume a certain order of the tables
        self.assertEquals(tables[0].Title(), "Some Title")
        self.assertEquals(tables[1].Title(), "Joined")
        self.assertEquals(tables[1].JoinType(), "INNER")


class TestLoadError(LoadSessionTest):

    file_contents = '''\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE session SYSTEM "thuban-0.8.dtd">
<session xmlns="http://thuban.intevation.org/dtds/thuban-0.8.dtd"
        title="single map&amp;layer">
    <fileshapesource id="D1" filename="../../Data/iceland/political.shp"/>
    <map title="Test Map">
        <projection name="Unknown">
            <parameter value="zone=26"/>
            <parameter value="proj=utm"/>
            <parameter value="ellps=clrk66"/>
        </projection>
        <layer shapestore="D1" visible="true"
                stroke="#000000" title="My Layer" stroke_width="1"
                fill="None"/>
    </map>
</session>
'''

    def test(self):
        """Test loading a session missing a required attribute"""
        # Don't use assertRaises to make sure that if a session is
        # actually returned it gets destroyed properly.
        try:
            self.session = load_session(self.filename())
        except LoadError, value:
            pass
        else:
            self.fail("Missing filetype attribute doesn't raise LoadError")


class TestUnicodeStrings(LoadSessionTest):

    file_contents = '''\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE session SYSTEM "thuban-0.8.dtd">
<session xmlns="http://thuban.intevation.org/dtds/thuban-0.8.dtd"
        title="Frida: Free Vector Geodata Osnabr\xc3\xbcck">
    <fileshapesource filetype="shapefile" id="D1"
        filename="../../Data/iceland/political.shp"/>
    <map title="Osnabr\xc3\xbcck">
        <projection name="Osnabr\xc3\xbcck Projection">
            <parameter value="zone=26"/>
            <parameter value="proj=utm"/>
            <parameter value="ellps=clrk66"/>
        </projection>
        <layer shapestore="D1" visible="true"
                stroke="#000000" title="Osnabr\xc3\xbcck Layer"
                stroke_width="1" fill="None"/>
    </map>
</session>
'''

    def test(self):
        """Load a session with unicode strings"""
        eq = self.assertEquals
        session = load_session(self.filename())
        self.session = session

        # Check the title
        eq(session.Title(),
           internal_from_unicode(u"Frida: Free Vector Geodata Osnabr\xfcck"))

        # Check the map's title
        maps = session.Maps()
        map = maps[0]
        eq(map.Title(), internal_from_unicode(u"Osnabr\xfcck"))

        # Check the layer's title
        layers = map.Layers()
        layer = layers[0]
        eq(layer.Title(), internal_from_unicode(u"Osnabr\xfcck Layer"))

        # Check the projection's title
        projection = map.GetProjection()
        eq(projection.GetName(),
           internal_from_unicode(u"Osnabr\xfcck Projection"))

        self.session.Destroy()
        self.session = None


if __name__ == "__main__":
    unittest.main()
