# Copyright (c) 2002, 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test loading a thuban session from a files generated by Thuban 0.2 and earlier
"""

__version__ = "$Revision: 1357 $"
# $Source$
# $Id: test_load_0_2.py 1357 2003-07-02 09:38:27Z jonathan $

import os
import unittest

import support
support.initthuban()

from Thuban.Model.load import load_session, parse_color
from Thuban.Model.color import Transparent
from Thuban.Model.classification import ClassGroupProperties, ClassGroupRange,\
    ClassGroupSingleton, ClassGroupDefault


def filenames_equal(name1, name2):
    """Return true if the filenames name1 and name2 are equal.

    On systems where it is available, simply use os.path.samefile,
    otherwise return whether the normalized versions of the filenames
    according to os.path.normpath are equal.
    """
    if hasattr(os.path, "samefile"):
        return os.path.samefile(name1, name2)
    return os.path.normpath(name1) == os.path.normpath(name2)



class LoadSessionTest(support.FileLoadTestCase):

    """Base class for .thuban file loading tests

    Basically the same as the FileLoadTestCase, except that all tests
    use the '.thuban' extension by default and that setUp and tearDown
    handle sessions.
    """

    file_extension = ".thuban"

    def setUp(self):
        """Create the test files"""
        support.FileLoadTestCase.setUp(self)
        self.session = None

    def tearDown(self):
        if self.session is not None:
            self.session.Destroy()
        self.session = None


class ClassificationTest(LoadSessionTest):

    """
    Base class for tests that do some detailed checking of classifications
    """

    def TestLayers(self, layers, expected):
        TITLE = 0
        NUM_GROUPS = 1
        CLASSES = 2
        GROUP_TYPE = 0
        GROUP_DATA = 1
        GROUP_LABEL = 2
        GROUP_PROPS = 3

        eq = self.assertEquals

        eq(len(layers), len(expected))

        for layer, data in zip(layers, expected):
            eq(layer.Title(), data[TITLE])

            clazz = layer.GetClassification()
            eq(clazz.GetNumGroups(), data[NUM_GROUPS])
            eq(clazz.GetNumGroups() + 1, len(data[CLASSES]))

            i = 0
            for group in clazz:
                props = ClassGroupProperties()
                props.SetLineColor(
                    parse_color(data[CLASSES][i][GROUP_PROPS][0]))
                props.SetLineWidth(data[CLASSES][i][GROUP_PROPS][1])
                props.SetFill(
                    parse_color(data[CLASSES][i][GROUP_PROPS][2]))

                if data[CLASSES][i][GROUP_TYPE] == "default":
                    g = ClassGroupDefault(props, data[CLASSES][i][GROUP_LABEL])
                elif data[CLASSES][i][GROUP_TYPE] == "range":
                    g = ClassGroupRange((data[CLASSES][i][GROUP_DATA][0],
                                         data[CLASSES][i][GROUP_DATA][1]),
                                        props, data[CLASSES][i][GROUP_LABEL])
                elif data[CLASSES][i][GROUP_TYPE] == "single":
                    g = ClassGroupSingleton(data[CLASSES][i][GROUP_DATA],
                                          props, data[CLASSES][i][GROUP_LABEL])

                eq(group, g)

                i += 1



class TestSingleLayer(LoadSessionTest):

    file_contents = '''\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE session SYSTEM "thuban.dtd">
<session title="single map&amp;layer">
	<map title="Test Map">
		<projection>
			<parameter value="zone=26"/>
			<parameter value="proj=utm"/>
			<parameter value="ellps=clrk66"/>
		</projection>
		<layer title="My Layer" stroke_width="1" fill="None"
                    filename="../../Data/iceland/political.shp"
                    stroke="#000000"/>
	</map>
</session>
'''

    def test(self):
        """Load a Thuban 0.2 session with a single map with a single layer"""
        eq = self.assertEquals
        session = load_session(self.filename())
        self.session = session

        # Check the title
        eq(session.Title(), "single map&layer")

        # the session has one map.
        maps = session.Maps()
        eq(len(maps), 1)

        # Check the map's attributes
        map = maps[0]
        eq(map.Title(), "Test Map")

        # the map has a single layer
        layers = map.Layers()
        eq(len(layers), 1)

        # Check the layer attributes
        layer = layers[0]
        eq(layer.Title(), "My Layer")
        self.failUnless(filenames_equal(layer.ShapeStore().FileName(),
                                        os.path.join(self.temp_dir(),
                                                     os.pardir, os.pardir,
                                                     "Data", "iceland",
                                                     "political.shp")))
        eq(layer.GetClassification().GetDefaultFill(), Transparent)
        eq(layer.GetClassification().GetDefaultLineColor().hex(), "#000000")
        eq(layer.Visible(), True)

        self.session.Destroy()
        self.session = None


class TestClassification(ClassificationTest):

    file_contents = '''\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE session SYSTEM "thuban.dtd">
<session title="single map&amp;layer">
	<map title="Test Map">
		<projection>
			<parameter value="zone=26"/>
			<parameter value="proj=utm"/>
			<parameter value="ellps=clrk66"/>
		</projection>
		<layer title="My Layer" stroke_width="1" fill="None"
                    filename="../../Data/iceland/political.shp"
                    stroke="#000000">
            <classification field="POPYREG" field_type="string">
                <clnull>
                    <cldata stroke="#000000" stroke_width="1" fill="None"/>
                </clnull>
                <clpoint value="1">
                    <cldata stroke="#000000" stroke_width="2" fill="None"/>
                </clpoint>
                <clpoint value="1">
                    <cldata stroke="#000000" stroke_width="10" fill="None"/>
                </clpoint>
            </classification>
        </layer>
		<layer title="My Layer 2" stroke_width="1" fill="None"
                    filename="../../Data/iceland/political.shp"
                    stroke="#000000">
            <classification field="AREA" field_type="double">
                <clnull>
                    <cldata stroke="#000000" stroke_width="2" fill="None"/>
                </clnull>
                <clrange min="0" max="1">
                    <cldata stroke="#111111" stroke_width="1" fill="None"/>
                </clrange>
                <clpoint value=".5">
                    <cldata stroke="#000000" stroke_width="1" fill="#111111"/>
                </clpoint>
                <clrange min="-1" max="0">
                    <cldata stroke="#000000" stroke_width="1" fill="None"/>
                </clrange>
                <clpoint value="-.5">
                    <cldata stroke="#000000" stroke_width="1" fill="None"/>
                </clpoint>
            </classification>
        </layer>
	</map>
</session>
'''

    def test(self):
        """Load a Thuban 0.2 session with a map and classified layers."""
        session = load_session(self.filename())
        self.session = session

        map = self.session.Maps()[0] # only one map in the sample

        expected = [("My Layer", 2,
                        [("default", (), "",
                            ("#000000", 1, "None")),
                         ("single", "1", "",
                            ("#000000", 2, "None")),
                         ("single", "1", "",
                            ("#000000", 10, "None"))]),
                     ("My Layer 2", 4,
                         [("default", (), "",
                            ("#000000", 2, "None")),
                          ("range", (0, 1), "",
                            ("#111111", 1, "None")),
                          ("single", .5, "",
                            ("#000000", 1, "#111111")),
                          ("range", (-1, 0), "",
                            ("#000000", 1, "None")),
                          ("single", -.5, "",
                            ("#000000", 1, "None"))])]

        self.TestLayers(map.Layers(), expected)


if __name__ == "__main__":
    unittest.main()
