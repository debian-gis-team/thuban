# Copyright (C) 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

"""Tests for DerivedShapeStore"""

__version__ = "$Revision: 1784 $"
# $Source$
# $Id: test_derivedshapestore.py 1784 2003-10-07 17:17:22Z bh $

import os

import unittest

import support
support.initthuban()

from Thuban.Model.data import DerivedShapeStore, ShapefileStore, \
     SHAPETYPE_ARC, RAW_SHAPEFILE

from Thuban.Model.session import Session
from Thuban.Model.table import MemoryTable, \
     FIELDTYPE_DOUBLE, FIELDTYPE_INT, FIELDTYPE_STRING


class TestDerivedShapeStore(unittest.TestCase, support.FloatComparisonMixin):

    def setUp(self):
        """Initialize self.session"""
        self.session = Session("Test Session")
        self.filename = os.path.join("..", "Data", "iceland",
                                "roads-line.shp")
        self.store = ShapefileStore(self.session, self.filename)

        self.table = MemoryTable([("type", FIELDTYPE_STRING),
                                  ("value", FIELDTYPE_DOUBLE),
                                  ("code", FIELDTYPE_INT)],
                                 [("UNKNOWN", 0.0, 0)] * 839)
        self.derived = DerivedShapeStore(self.store, self.table)

    def tearDown(self):
        """Call self.session.Destroy() and reset self.session to None"""
        self.session.Destroy()
        self.session = None

    def test_dependencies(self):
        """Test DerivedShapeStore dependencies"""
        # The shapestore itself depends on nothing else
        self.assertEquals(self.derived.Dependencies(),
                          (self.store, self.table))

    def test_orig_shapestore(self):
        """Test DerivedShapeStore.OrigShapeStore()"""
        self.assertEquals(self.derived.OrigShapeStore(), self.store)

    def test_shape_type(self):
        """Test DerivedShapeStore.ShapeType() with arc shapes"""
        self.assertEquals(self.derived.ShapeType(), SHAPETYPE_ARC)

    def test_raw_format(self):
        """Test DerivedShapeStore.RawShapeFormat() with shapefiles"""
        self.assertEquals(self.derived.RawShapeFormat(), RAW_SHAPEFILE)

    def test_boundingbox(self):
        """Test DerivedShapeStore.BoundingBox() with arc shapes"""
        self.assertFloatSeqEqual(self.derived.BoundingBox(),
                                 [-24.450359344482422, 63.426830291748047,
                                  -13.55668830871582, 66.520111083984375])

    def test_num_shapes(self):
        """Test DerivedShapeStore.NumShapes() with arc shapes"""
        self.assertEquals(self.derived.NumShapes(), 839)

    def test_shapes_in_region(self):
        """Test DerivedShapeStore.ShapesInRegion() with arc shapes"""
        shapes = self.derived.ShapesInRegion((-24.0, 64.0, -23.75, 64.25))
        self.assertEquals([s.ShapeID() for s in shapes],
                          [613, 726, 838])

    def test_all_shapes(self):
        """Test DerivedShapeStore.AllShapes()"""
        self.assertEquals([s.ShapeID() for s in self.store.AllShapes()],
                          range(self.store.NumShapes()))

    def test_shape(self):
        """Test DerivedShapeStore.Shape() with arc shapes"""
        self.assertPointListEquals(self.derived.Shape(32).Points(),
                                   [[(-15.08217430114746, 66.2773818969726),
                                     (-15.02635002136230, 66.2733917236328)]])
    def test_shape_shapeid(self):
        """Test DerivedShapeStore.Shape(i).ShapeID()"""
        self.assertEquals(self.store.Shape(5).ShapeID(), 5)



class TestDerivedShapeStoreExceptions(unittest.TestCase):

    """Test DerivedShapeStore exceptions"""

    def tearDown(self):
        if hasattr(self, "session"):
            self.session.Destroy()
            self.session = None

    def test_table_with_wrong_size(self):
        """Test DerivedShapeStore() with a table with the wrong number of lines
        """
        filename = os.path.join("..", "Data", "iceland", "roads-line.shp")
        session = self.session = Session("TestDerivedShapeStore Session")
        store =  session.OpenShapefile(filename)

        table = MemoryTable([("type", FIELDTYPE_STRING),
                             ("value", FIELDTYPE_DOUBLE),
                             ("code", FIELDTYPE_INT)],
                            [("UNKNOWN", 0.0, 0),
                             ("Foo", 0.5, -1),
                             ("Foo", 0.25, 100),
                             ("bar", 1e10, 17)])

        # Trying to create a DerivedShapeStore where the number of lines
        # in the table is not the same as the number of shapes in the
        # shapefile raises a ValueError
        self.assertRaises(ValueError, DerivedShapeStore, store, table)


if __name__ == "__main__":
    support.run_tests()
