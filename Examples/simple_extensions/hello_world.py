# Copyright (C) 2003, 2004 by Intevation GmbH
# Authors:
# Jan-Oliver Wagner <jan@intevation.de> (2003, 2004)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Extend Thuban with a sample Hello World to demonstrate simple
extensions.
"""

__version__ = '$Revision: 2409 $'
# $Source$
# $Id: hello_world.py 2409 2004-11-20 21:57:14Z jan $

# use _() already now for all strings that may later be translated
from Thuban import _

# Thuban has named commands which can be registered in the central
# instance registry.
from Thuban.UI.command import registry, Command

# The instance of the main menu of the Thuban application
# See Thuban/UI/menu.py for the API of the Menu class
from Thuban.UI.mainwindow import main_menu

def hello_world_dialog(context):
    """Just raise a simple dialog to greet the world.

    context -- The Thuban context.
    """
    context.mainwindow.RunMessageBox(_('Hello World'), _('Hello World!'))


# create a new command and register it
registry.Add(Command('hello_world', _('Hello World'), hello_world_dialog,
                     helptext = _('Welcome everyone on this planet')))

# find the extensions menu (create it anew if not found)
examples_menu = main_menu.FindOrInsertMenu('examples', _('&Examples'))

# finally bind the new command with an entry in the extensions menu
examples_menu.InsertItem('hello_world')
