# Copyright (C) 2002, 2004 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de> (2002)
# Jan-Oliver Wagner <jan@intevation.de> (2004)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Extend thuban with a simple command.
"""

__version__ = "$Revision: 2410 $"
# $Source$
# $Id: simple_command.py 2410 2004-11-20 21:57:43Z jan $

# First import some things we need later.
import os
from Thuban.UI.command import registry, Command
from Thuban.UI.mainwindow import main_menu

# a function implementing a command. Such a function is called with one
# argument describing the context in which it is invoked. The context is
# an object with a few public attributes for the application object, the
# session and the main window.
def simple_command(context):
    print "simple_command: Application", context.application
    print "simple_command: Session", context.session
    print "simple_command: Main window", context.mainwindow
    print "simple_command: Map", context.mainwindow.canvas.Map()

# Add the command to the registry. A command is represented by a Command
# instance which is instantiated with four parameters here, the name of
# the command, it's title, the function to call when the command is
# invoked by the user and a help text to show in the statusbar.
#
# The name is used internally to identify commands. The title is
# displayed in the menus.
registry.Add(Command("simple_command", "Simple Command", simple_command,
                     helptext = "Simple Command"))

# The main menu is defined by the main_menu object in
# Thuban.UI.mainwindow. This object has a few methods to add new menus
# and menu items.
#
# FindOrInsertMenu creates a new submenu in the menu, parameters are its name
# and title which have the same meanings as for a command. The return
# value is a menu object for the new submenu which has the same methods
# as main_menu.
# If the menu already exist, this previous one will be returned an
# no new one be created.
menu = main_menu.FindOrInsertMenu("examples", "&Examples")

# In the new menu we can add new items with InsertItem which takes the
# name of a command as parameter or with InsertSeparator to add a
# separator. We can also use InsertMenu to add submenus, of course. We
# add the command twice here to demonstrate separators.
menu.InsertItem("simple_command")
menu.InsertSeparator()
menu.InsertItem("simple_command")
