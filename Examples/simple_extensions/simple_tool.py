# Copyright (C) 2002, 2004 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de> (2002)
# Jan-Oliver Wagner <jan@intevation.de> (2004)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Extend thuban with a simple tool.
"""

__version__ = "$Revision: 2411 $"
# $Source$
# $Id: simple_tool.py 2411 2004-11-20 21:58:18Z jan $

# First import some things we need later.
import os
from math import hypot
from Thuban.UI.command import registry, ToolCommand
from  Thuban.UI.mainwindow import main_toolbar
from Thuban.UI.viewport import Tool


# A tool is a class usually derived from the Tool class. The tool class
# provides some standard methods to handle mouse events. It maintains a
# few instance variables that can be used when processing mouse events:
# self.start is the point where the user started to drag, self.current
# is the current position, self.dragging is true while the user is
# moving the mouse with the (left) button pressed.

class SimpleTool(Tool):

    """A Simple Tool"""

    def Name(self):
        """Return the string 'SimpleTool'."""
        # The return value is used to identify tools easily
        return "SimpleTool"

    def map_distance(self):
        """Return the distance on the map between the start and current point
        """
        # self.view is the canvas window the tool instance is working
        # on. Its win_to_proj method computes the coordinates in the
        # projected map coordinates for a given point in window
        # coordinates
        sx, sy = apply(self.view.win_to_proj, self.start)
        x, y = apply(self.view.win_to_proj, self.current)
        return hypot(x - sx, y - sy)

    def MouseMove(self, event):
        """Called by the canvas window when the mouse moves"""
        # The self.dragging flag is true, if the user is currently
        # dragging the mouse. Code in the Tool class has already handled
        # the button press event to set this flag.
        if self.dragging:
            # Call the inherited method to update some internal data
            # (self.start, etc.)
            Tool.MouseMove(self, event)
            print "SimpleTool: current distance", self.map_distance()

    def MouseUp(self, event):
        if self.dragging:
            Tool.MouseUp(self, event)
            print "SimpleTool: final distance", self.map_distance()


# the function implementing the "SimpleTool" command. Set the tool of
# the canvas to SimpleTool
def simple_tool(context):
    canvas = context.mainwindow.canvas
    canvas.SelectTool(SimpleTool(canvas))


# Add the command to the registry. A command is represented by a Command
# instance. Here it's instantiated with the the name of the command,
# it's title and the function to call when the command is invoked by the
# user as positional arguments. The name is used internally to identify
# commands. The title is displayed in the menus.
#
# The icon keyword argument is optional and only useful if the command
# is used in a toolbar. It should be the name of an XPM file without the
# .xpm extension which will be automatically appended. We assume here
# that the icon's XPM file is located in the same directory as this
# module.
#
# The helptext keyword argument is an optional helptext.
#
# The checked keyword argument is an optional function to determine
# whether the button or menu item should be checked. It's called with
# the context. If the checked argument is not given the button or menu
# item will be a normal command button/item.

def check_simple_tool(context):
    """Return if the current tool of the context is the simple tool"""
    # the CurrentTool() method of the canvas returns the result of the
    # tool's Name method so we just have to compare it to "SimpleTool"
    return context.mainwindow.canvas.CurrentTool() == "SimpleTool"

iconfile = os.path.abspath(os.path.join(os.path.split(__file__)[0],
                                        "simple_tool"))
registry.Add(ToolCommand("simple_tool", "Simple Tool", simple_tool,
                         icon = iconfile, helptext = "Simple Tool",
                         checked = check_simple_tool))

# Add the command to the toolbar
main_toolbar.InsertSeparator()
main_toolbar.InsertItem("simple_tool")
