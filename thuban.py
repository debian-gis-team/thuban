#! /usr/bin/python

# Copyright (c) 2001 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

import sys, os
import Thuban

import wxversion
wxversion.select("2.8")

# Put the Lib dir into the path. The Lib dir contains some extra non
# really Thuban specific Python modules
thubandir = os.path.join(Thuban.__path__[0], '..')
dir = os.path.join(thubandir, "Lib")
if os.path.isdir(dir):
    sys.path.insert(0, dir)

# win32 gdal support
if sys.platform == 'win32':
    # PYTHONPATH update
    dir = os.path.join(thubandir, "gdal", "pymod")
    if os.path.isdir(dir):
        sys.path.append(dir)

    try:
        import gdal
    except ImportError:
        print "Please update your PATH environment variable to include %s\\gdal\\bin" % thubandir

import Thuban.UI.main

# Start Thuban
if __name__ == "__main__":
    Thuban.UI.main.main()
