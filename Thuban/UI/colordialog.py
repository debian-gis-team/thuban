# Copyright (c) 2004 by Intevation GmbH
# Authors:
# Jan-Oliver Wagnber <jan@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

__version__ = "$Revision: 2877 $"
# $Source$
# $Id: colordialog.py 2877 2009-05-18 15:14:01Z dpinte $

import wx

from Thuban import _

from Thuban.UI.common import Color2wxColour, wxColour2Color

from Thuban.Model.color import Transparent


# determine whether the pyColourChooserDialog is available
# It was not available with the very first versions of wxWindows 2.4
# (I don't know the exact version though)
try:
    from wx.lib.colourchooser import wxPyColourChooser
    _wxPyColourChooser = True
    wx.InitAllImageHandlers() # should be somewhere at Thuban startup?
except:
    _wxPyColourChooser = False


class PyColorChooserDialog(wx.Dialog):
    """
    A Dialog that uses the wxPyColourChooser Frame and simply
    adds OK and Cancel button to form a modal color selection dialog.
    """

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, -1, _("Select Color"))

        self.parent = parent
        self.dialog_layout()

    def dialog_layout(self):
        top_box = wx.BoxSizer(wx.VERTICAL)

        self.chooser = wxPyColourChooser(self, -1)

        top_box.Add(self.chooser, 1, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(wx.Button(self, wx.ID_OK, _("OK")), 0, wx.ALL, 4)
        box.Add(wx.Button(self, wx.ID_CANCEL, _("Cancel")), 0, wx.ALL, 4)
        top_box.Add(box, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 10)

        self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)
        self.Bind(wx.EVT_BUTTON, self.OnCancel, id=wx.ID_CANCEL)

        self.SetAutoLayout(True)
        self.SetSizer(top_box)
        top_box.Fit(self)
        top_box.SetSizeHints(self)

    def GetValue(self):
        return self.chooser.GetValue()

    def SetValue(self, color):
        return self.chooser.SetValue(color)

    def OnOK(self, event):
        self.EndModal(wx.ID_OK)

    def OnCancel(self, event):
        self.EndModal(wx.ID_CANCEL)


class ColorDialog:
    """
    The color dialog raises one of the available color selection
    dialogs. ColorDialog is no derived from any GUI class though.

    Furthermore, wxColour class is mapped to the Thuban
    Color class already through this class.

    Eventually it should be configurable globally for Thuban
    which color dialog to use since they might differ in
    functionality, translation or stability.
    """

    def __init__(self, parent):
        if _wxPyColourChooser:
            self.dlg = PyColorChooserDialog(parent)
        else:
            self.dlg = wx.ColourDialog(parent)

    def GetColor(self):
        if _wxPyColourChooser:
            return wxColour2Color(self.dlg.GetValue())
        else:
            return wxColour2Color(self.dlg.GetColourData().GetColour())

    def SetColor(self, color):
        if color is not Transparent:
            if _wxPyColourChooser:
                self.dlg.SetValue(Color2wxColour(color))
            else:
                self.dlg.GetColourData().SetColour(Color2wxColour(color))

    def ShowModal(self):
        return self.dlg.ShowModal()

    def Destroy(self):
        return self.dlg.Destroy()


if __name__ == "__main__":
    # Test routine to run the dialog without Thuban

    from wxPython.wx import wxApp, NULL

    wx.InitAllImageHandlers()

    class _TestApp(wx.App):
        def OnInit(self):
            dialog = ColorDialog(NULL)

            if dialog.ShowModal() == wx.ID_OK:
                print "Selected color:", dialog.GetColor()
            else:
                print "No color selected"

            dialog.Destroy()
            return True

    app = _TestApp()
    app.MainLoop()
