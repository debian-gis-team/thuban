# Copyright (c) 2001, 2002, 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.


"""
Define the message types used in Thuban's GUI. The messages types are
simply strings. The message system itself is implemented in
Thuban.Lib.connector.
"""

__version__ = "$Revision: 1464 $"

# Application object
SESSION_REPLACED = "SESSION_REPLACED"

# events for the selection
LAYER_SELECTED = "LAYER_SELECTED"
SHAPES_SELECTED = "SHAPES_SELECTED"
# obsolete selection messages.
SELECTED_SHAPE = "SELECTED_SHAPE"
SELECTED_LAYER = "SELECTED_LAYER"

# events for the view
VIEW_POSITION = "VIEW_POSITION"
SCALE_CHANGED = "SCALE_CHANGED"
MAP_REPLACED = "MAP_REPLACED"

# event for a dockable window
DOCKABLE_DOCKED   = "DOCKABLE_DOCKED"
DOCKABLE_UNDOCKED = "DOCKABLE_UNDOCKED"
DOCKABLE_CLOSED   = "DOCKABLE_CLOSED"

