# Copyright (c) 2003, 2004 by Intevation GmbH
# Authors:
# Jan-Oliver Wagner <jan@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""A dialog for entering multiple choice from a list of strings.

This dialog is actually a class contained by the wxPython Library.
However, the wxMultipleChoiceDialog did not pass the style in 2.4.0.

As soon as Thuban does not support wxPython 2.4.0 any more,
this module can be removed and the official wxMultipleChoiceDialog
of the wxPython Library be used directly."""

__version__ = "$Revision: 2700 $"

import wx

class wxMultipleChoiceDialog(wx.Dialog):
    """This is a copy of the class wxPython.lib.dialogs.wxMultipleChoiceDialog
    and fixes the bug that the style now is passed on (this is fixed
    in version wxPython 2.4.1).
    """
    def __init__(self, parent, msg, title, lst, pos = wx.DefaultPosition,
                 size = (200,200), style = wx.DEFAULT_DIALOG_STYLE):
        wx.Dialog.__init__(self, parent, -1, title, pos, size, style)
        x, y = pos
        if x == -1 and y == -1:
            self.CenterOnScreen(wx.wxBOTH)
        dc = wx.wxClientDC(self)
        height = 0
        for line in msg.splitlines():
            height = height + dc.GetTextExtent(msg)[1] + 4
        stat = wx.wxStaticText(self, -1, msg)
        self.lbox = wx.wxListBox(self, 100, wx.wxDefaultPosition,
                                 wx.wxDefaultSize, lst, wx.wxLB_MULTIPLE)
        ok = wx.wxButton(self, wx.wxID_OK, "OK")
        cancel = wx.wxButton(self, wx.wxID_CANCEL, "Cancel")
        stat.SetConstraints(Layoutf('t=t10#1;l=l5#1;r=r5#1;h!%d' % (height,),
                                   (self,)))
        self.lbox.SetConstraints(Layoutf('t=b10#2;l=l5#1;r=r5#1;b=t5#3',
                                 (self, stat, ok)))
        ok.SetConstraints(Layoutf('b=b5#1;x%w25#1;w!80;h!25', (self,)))
        cancel.SetConstraints(Layoutf('b=b5#1;x%w75#1;w!80;h!25', (self,)))
        self.SetAutoLayout(1)
        self.lst = lst
        self.Layout()

    def GetValue(self):
        return self.lbox.GetSelections()

    def GetValueString(self):
        sel = self.lbox.GetSelections()
        val = []
        for i in sel:
            val.append(self.lst[i])
        return tuple(val)
