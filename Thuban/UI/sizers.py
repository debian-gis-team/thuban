# Copyright (C) 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

"""Custom Sizers"""

__version__ = "$Revision: 2700 $"
# $Source$
# $Id: sizers.py 2700 2006-09-18 14:27:02Z dpinte $

import wx

class NotebookLikeSizer(wx.PySizer):

    """Similar to a wxNotebookSizer but doesn't need a real notebook
    """

    def __init__(self):
        wx.PySizer.__init__(self)
        self.item_dict = {}

    def Add(self, item):
        """Add the item to the sizer.

        The children added with Add must must be an object that has
        Show() and Hide() methods
        """
        wx.PySizer.Add(self, item)

    def Activate(self, item):
        """Activate the item.

        Call this item's Show method and the Hide method of all other
        items. The item must have been added to the sizer with Add.
        """
        for child in self.GetChildren():
            window = child.GetWindow()
            if window is item:
                window.Show()
            else:
                window.Hide()

    def RecalcSizes(self):
        """Set all children to the current size of the sizer

        This method is automatically called by the wxWindows sizer
        mechanism.
        """
        pos = self.GetPosition()
        size  = self.GetSize()

        for item in self.GetChildren():
            item.SetDimension(pos, size)

    def CalcMin(self):
        """Return the maximum of the minimum sizes of the children.

        This method is automatically called by the wxWindows sizer
        mechanism.
        """
        widths = []
        heights = []
        for item in self.GetChildren():
            size = item.CalcMin()
            widths.append(size.width)
            heights.append(size.height)
        if widths:
            return wx.Size(max(widths), max(heights))
        return wx.Size(0, 0)
