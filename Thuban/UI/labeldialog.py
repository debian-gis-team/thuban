# Copyright (c) 2001 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

__version__ = "$Revision: 2700 $"

import wx

from Thuban import _

from controls import SelectableRecordListCtrl

class LabelListCtrl(SelectableRecordListCtrl):

    def __init__(self, parent, id, table, shape):
        SelectableRecordListCtrl.__init__(self, parent, id)
        self.fill_list(table, shape)


class LabelDialog(wx.Dialog):

    def __init__(self, parent, table, shape_index):
        wx.Dialog.__init__(self, parent, -1, _("Label Values"),
                          wx.DefaultPosition,
                          style = wx.RESIZE_BORDER|wx.CAPTION|wx.DIALOG_MODAL)

        self.parent = parent
        self.dialog_layout(table, shape_index)

    def dialog_layout(self, table, shape_index):
        top_box = wx.BoxSizer(wx.VERTICAL)

        self.list = LabelListCtrl(self, -1, table, shape_index)
        self.list.SetSize(wx.Size(305,200))
        top_box.Add(self.list, 1, wx.EXPAND|wx.ALL, 4)

        box = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(wx.Button(self, wx.ID_OK, _("OK")), 0, wx.ALL, 4)
        box.Add(wx.Button(self, wx.ID_CANCEL, _("Cancel")), 0, wx.ALL, 4)
        top_box.Add(box, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 10)

        self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)
        self.Bind(wx.EVT_BUTTON, self.OnCancel, id=wx.ID_CANCEL)

        self.SetAutoLayout(True)
        self.SetSizer(top_box)
        top_box.Fit(self)
        top_box.SetSizeHints(self)

    def OnOK(self, event):
        result = self.list.GetValue()
        if result is not None:
            self.end_dialog(wx.ID_OK, str(result))
        else:
            self.end_dialog(wx.ID_CANCEL, None)

    def OnCancel(self, event):
        self.end_dialog(wx.ID_CANCEL, None)

    def end_dialog(self, wx_result, result):
        self.result = result
        self.EndModal(wx_result)

def run_label_dialog(parent, table, shape_index):
    dialog = LabelDialog(parent, table, shape_index)
    if dialog.ShowModal() == wx.ID_OK:
        return dialog.result
    else:
        return None

