# Copyright (c) 2003 by Intevation GmbH
# Authors:
# Jonathan Coles <jonathan@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""Acess to UI-related resources"""

__version__ = "$Revision: 2880 $"
# $Source$
# $Id: resource.py 2880 2009-06-29 14:06:24Z dpinte $


import os
import Thuban
from Thuban.Lib.fileutil import get_thuban_dir

import wx


bitmapdir = os.path.join(get_thuban_dir(), "Resources", "Bitmaps")
bitmap_extensions = {wx.BITMAP_TYPE_XPM: ".xpm",
                     wx.BITMAP_TYPE_ANY: ""}

def GetBitmapResource(file, type):
    filename = os.path.join(bitmapdir, file) + bitmap_extensions[type]
    return wx.Bitmap(filename, type)

def GetImageResource(file, type):
    filename = os.path.join(bitmapdir, file) + bitmap_extensions[type]
    return wx.Image(filename, type)

