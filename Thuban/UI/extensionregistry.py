# Copyright (C) 2004, 2005 by Intevation GmbH
# Authors:
# Jan-Oliver Wagner <jan@intevation.de> (2004, 2005)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Registry for Extensions.

This module defines a registry for Extensions.
This is basically used for displaying information on
the active Extensions in the general About-Dialog.
A registration is not mandatory to make a Extension
work, but any Extension should register to provide
a complete information in the Thuban About Dialog.

A Extension should define a ExtensionDesc object
and apply ext_registry.add() to register it.
"""

__version__ = "$Revision: 2580 $"
# $Source$
# $Id: extensionregistry.py 2580 2005-03-10 22:39:11Z jan $

from Thuban import _

class ExtensionDesc:

    """
    A description of a Thuban Extension.
    """

    def __init__(self, name, version, authors, copyright, desc,
                 init_callback = None):
        """
        Initialize a new Thuban Extension information.

        name -- The name of the Thuban Extension.
                Example: 'gns2shp'

        version -- The version number of the Thuban Extension.
                   Example: '1.0.0'

        authors -- List of authors.
                   Example: [ 'Jan-Oliver Wagner' ]

        copyright -- Copyright notice.
                     Example: '2003, 2004 Intevation GmbH'

        desc -- Short description of the Thuban Extension.
                Example: _('''
                         Converts GNS (Geographical Name Service of NIMA)
                         to Shapefile format and displays the data.
                         ''')
        init_callback -- a callback method that is executed if not
                         None. This method should contain gui specific
                         initializations of the extension.
                         Default is None.

                         This callback should return None if
                         the intialisation as successful. Else
                         a string describing the problems during
                         intialization.
        """
        self.name = name
        self.version = version
        self.authors = authors
        self.copyright = copyright
        self.desc = desc
        self.init_callback = init_callback
        self.status = _("Initialization not yet requested.")

    def init_ext(self):
        """Execute the init callback for the extension.

        Do nothing if the callback is None.

        It is expected that the callback function returns
        None if all was OK and a string if something was wrong.
        The string is believed to describe the problems.
        """
        if self.init_callback is not None:
            result = self.init_callback()
            if result is not None:
                self.status = result
            else:
                self.status = _("Initialization successful.")

class ExtensionRegistry:

    """
    A ExtensionRegistry keeps information on the registered
    Thuban Extensions.

    The registry will raise a exception either when the version
    required for the Extensions exceeds the version of the currently
    running Thuban or if the name of a registered extension already
    is used for registering another extension.
    """

    def __init__(self):
        self.registry = []

    def __nonzero__(self):
        return len(self.registry) <> 0

    def __iter__(self):
        return iter(self.registry)

    def add(self, ext):
        self.registry.append(ext)


# The central Thuban Extensions registry
ext_registry = ExtensionRegistry()
