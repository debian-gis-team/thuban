# Copyright (c) 2005 by Intevation GmbH
# Authors:
# Jonathan Coles <jonathan@intevation.de> 
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""Base class for Layer Properties dialogs"""

__version__ = "$Revision: 2700 $"
# $Source$
# $Id: layerproperties.py 2700 2006-09-18 14:27:02Z dpinte $

from Thuban import _

import wx
from Thuban.Model.messages import MAP_LAYERS_REMOVED, LAYER_SHAPESTORE_REPLACED
from dialogs import NonModalNonParentDialog
from messages import MAP_REPLACED

ID_PROPERTY_REVERT = 4002
ID_PROPERTY_TRY = 4008
ID_PROPERTY_TITLE = 4012

class LayerProperties(NonModalNonParentDialog):

    def __init__(self, parent, name, layer):

        NonModalNonParentDialog.__init__(self, parent, name, "")

        self.SetTitle(layer.Title())

        self.parent.Subscribe(MAP_REPLACED, self.map_replaced)
        self.layer = layer
        self.map = parent.Map()

        self.map.Subscribe(MAP_LAYERS_REMOVED, self.map_layers_removed)

        self.layout_recurse = False

    def dialog_layout(self, *args, **kw):

        if self.layout_recurse: return
        self.layout_recurse = True

        panel = wx.Panel(self, -1)

        topBox = wx.BoxSizer(wx.VERTICAL)
        panelBox = wx.BoxSizer(wx.VERTICAL)

        # Title

        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(wx.StaticText(panel, -1, _("Title: ")),
            0, wx.ALIGN_LEFT | wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 4)

        text_title = wx.TextCtrl(panel, ID_PROPERTY_TITLE, self.layer.Title())
        text_title.SetInsertionPointEnd()
        sizer.Add(text_title, 1, wx.GROW | wx.RIGHT, 0)

        panelBox.Add(sizer, 0, wx.GROW | wx.ALL, 4)

        # Type
        panelBox.Add(wx.StaticText(panel, -1,
            _("Layer Type: %s") % self.layer.Type()),
            0, wx.ALIGN_LEFT | wx.ALL, 4)

        # Projection
        proj = self.layer.GetProjection()
        if proj is None:
            text = _("Projection: None")
        else:
            text = _("Projection: %s") % proj.Label()

        panelBox.Add(wx.StaticText(panel, -1, text), 0, wx.ALIGN_LEFT | wx.ALL, 4)

        self.dialog_layout(panel, panelBox)

        button_try = wx.Button(self, ID_PROPERTY_TRY, _("Try"))
        button_revert = wx.Button(self, ID_PROPERTY_REVERT, _("Revert"))
        button_ok = wx.Button(self, wx.ID_OK, _("OK"))
        button_close = wx.Button(self, wx.ID_CANCEL, _("Close"))
        button_ok.SetDefault()

        buttonBox = wx.BoxSizer(wx.HORIZONTAL)
        buttonBox.Add(button_try, 0, wx.RIGHT|wx.EXPAND, 10)
        buttonBox.Add(button_revert, 0, wx.RIGHT|wx.EXPAND, 10)
        buttonBox.Add(button_ok, 0, wx.RIGHT|wx.EXPAND, 10)
        buttonBox.Add(button_close, 0, wx.RIGHT|wx.EXPAND, 0)

        panel.SetAutoLayout(True)
        panel.SetSizer(panelBox)
        panelBox.Fit(panel)
        panelBox.SetSizeHints(panel)

        topBox.Add(panel, 1, wx.GROW | wx.ALL, 4)
        topBox.Add(buttonBox, 0, wx.ALIGN_RIGHT|wx.ALL, 10)

        self.SetAutoLayout(True)
        self.SetSizer(topBox)
        topBox.Fit(self)
        topBox.SetSizeHints(self)
        self.Layout()

        ###########

        self.Bind(wx.EVT_TEXT, self.OnTitleChanged, id=ID_PROPERTY_TITLE)
        self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)
        self.Bind(wx.EVT_BUTTON, self.OnTry, id=ID_PROPERTY_TRY)
        self.Bind(wx.EVT_BUTTON, self.OnCloseBtn, id=wx.ID_CANCEL)
        self.Bind(wx.EVT_BUTTON, self.OnRevert, id=ID_PROPERTY_REVERT)

        ######################

        text_title.SetFocus()
        self.haveApplied = False


    def unsubscribe_messages(self):
        """Unsubscribe from all messages."""
        self.parent.Unsubscribe(MAP_REPLACED, self.map_replaced)
        self.map.Unsubscribe(MAP_LAYERS_REMOVED, self.map_layers_removed)

    def map_layers_removed(self):
        """Subscribed to MAP_LAYERS_REMOVED. If this layer was removed,
        Close self.
        """
        if self.layer not in self.map.Layers():
            self.Close()

    def map_replaced(self, *args):
        """Subscribed to the mainwindow's MAP_REPLACED message. Close self."""
        self.Close()

    def OnTry(self, event):
        return

    def OnOK(self, event):
        self.Close()

    def OnClose(self, event):
        self.unsubscribe_messages()
        NonModalNonParentDialog.OnClose(self, event)

    def OnCloseBtn(self, event):
        """Close is similar to Cancel except that any changes that were
        made and applied remain applied.
        """

        self.Close()

    def OnRevert(self, event):
        return

    def SetTitle(self, title):
        """Set the title of the dialog."""
        if title != "":
            title = ": " + title

        NonModalNonParentDialog.SetTitle(self, _("Layer Properties") + title)

    def OnTitleChanged(self, event):
        """Update the dialog title when the user changed the layer name."""
        obj = event.GetEventObject()

        self.layer.SetTitle(obj.GetValue())
        self.SetTitle(self.layer.Title())

