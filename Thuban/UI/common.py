# Copyright (c) 2001, 2003 by Intevation GmbH
# Authors:
# Jonathan Coles <jonathan@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""Miscellaneous UI related functions"""

__version__ = "$Revision: 2700 $"
# $Source$
# $Id: common.py 2700 2006-09-18 14:27:02Z dpinte $


from Thuban.Model.color import Color
import wx

def Color2wxColour(color):
    """Return a wxColor object for the Thuban color object color"""
    return wx.Colour(int(round(color.red * 255)),
                    int(round(color.green * 255)),
                    int(round(color.blue * 255)))

def wxColour2Color(colour):
    """Return a Thuban color object for the wxColor object color"""
    assert(colour is not None)
    # this doesn't work because colour is really a wxColourPtr!
    #assert(isinstance(colour, wxColour))
    return Color(colour.Red()   / 255.0,
                 colour.Green() / 255.0,
                 colour.Blue()  / 255.0)

def ThubanBeginBusyCursor():
    """Thuban wrapper for wxBeginBusyCursor

    In addition to calling wxBeginBusyCursor this function also calls
    wxSafeYield to make sure that the cursor change takes effect. wxGTK
    2.4 at least doesn't do that automatically.

    This function and the corresponding ThubanEndBusyCursor function are
    the functions to use in Thuban to set a busy cursor.
    """
    wx.BeginBusyCursor()
    wx.SafeYield()

def ThubanEndBusyCursor():
    """Thuban wrapper for wxEndBusyCursor

    This function doesn't do anything more than calling wxEndBusyCursor
    yet, but please use this whereever you use ThubanBeginBusyCursor.
    """
    wx.EndBusyCursor()
