# Copyright (c) 2001, 2002, 2003, 2005, 2007 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>  2001, 2002, 2003, 2005
# Bernhard Reiter <bernhard@intevation.de> 2007
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.



_wx_can_be_imported = False

_locale = None
def install_wx_translation():
    """Install the wx translation function in Thuban.

    For more information about translations in Thuban see the comment in
    Thuban/__init__.py
    """
    global _locale
    global _wx_can_be_imported
    import Thuban
    if not Thuban.translation_function_installed():
        # Only import wx modules when we actually can install the
        # function so that the test suite can inhibit this
        _wx_can_be_imported = True
        import wx
        wx.Locale_AddCatalogLookupPathPrefix(Thuban._message_dir)
        _locale = wx.Locale(wx.LANGUAGE_DEFAULT)
        _locale.AddCatalog("thuban")

        # With a unicode build of wxPython, wxGetTranslation returns a
        # unicode object, so we have a wrapper that handles the case of
        # not using unicode as the internal string representation of
        # Thuban: If wxGetTranslation returns unicode and the internal
        # string representation of Thuban is not unicode, we convert the
        # translated string to the internal representation.  wxPython
        # will convert such strings back to unicode if necessary,
        # provided the internal encoding used is the one expected by
        # wxPython, which is taken care of below.
        def thuban_wx_translation(s):
            t = wx.GetTranslation(s)
            if isinstance(t, unicode) and Thuban._internal_encoding!="unicode":
                t = t.encode(Thuban._internal_encoding, "replace")
            return t

        Thuban.install_translation_function(thuban_wx_translation)

        # Reset the python locale. This makes sure that the LC_NUMERIC
        # seen by the python interpreter and by C modules is "C" instead
        # of the user's locale. Python code will still see the user's
        # locale where it matters. Without this, drawing the map doesn't
        # work for some reason
        import locale
        locale.setlocale(locale.LC_NUMERIC, "")

        # determine the internal encoding to use.
        # This is very tricky.  It's probably not optimal yet.
        encoding = None

        # If we have a wxPython >= 2.5.4.1, we use the
        # GetDefaultPyEncoding function to use exactly what wxPython
        # also uses when converting between unicode and byte-strings ...
        if hasattr(wx, "GetDefaultPyEncoding"):
            # AFAICT from the code this will always be a usable string,
            # although it could be "ascii".
            internal_encoding = wx.GetDefaultPyEncoding()

        # ... otherwise we use Python's getdefaultlocale.  This is what
        # GetDefaultPyEncoding also uses ...
        if encoding is None:
            encoding = locale.getdefaultlocale()[1]

        # ... finally, if we haven't figured it out yet, use latin 1 for
        # historical reasons.  Maybe ascii would be better.
        if encoding is None:
            encoding = "latin1"

        # set the encoding
        Thuban.set_internal_encoding(encoding)


#install_wx_translation()

# We define a function that will return the internal representation
# for a value returned from wxString.
# This depends on if we have a unicode build and which encoding we run.
internal_from_wxstring = lambda x: x  # identity

def install_internal_from_wxstring():
    """Install the needed internal_from_wxstring() function.

    For unicode builds wxString will return a unicode object according to
    the wxPython documentation (looked up in changelog coming with 2.6.3)
    we need to convert this back to internal, unless we use unicode objects
    internally already.

    Otherwise no conversion will need to be done.
    """
    global _wx_can_be_imported
    global internal_from_wxstring

    if _wx_can_be_imported:
        import Thuban
        import wx
        if wx.USE_UNICODE == 1 and Thuban.get_internal_encoding != "unicode":
            internal_from_wxstring = lambda x : Thuban.internal_from_unicode(x)

install_internal_from_wxstring()
