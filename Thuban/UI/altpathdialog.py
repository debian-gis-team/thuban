# Copyright (c) 2004 by Intevation GmbH
# Authors:
# Frank Koormann <frank@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.


"""Dialogs for alternative paths (path recovery).

   AltPathFileDialog:    File dialog to specify alterative path.
   AltPathConfirmDialog: Confirm or cancel alternative path suggestion.
"""

from Thuban import _
from Thuban.UI import internal_from_wxstring

import wx

import os

class AltPathFileDialog(wx.FileDialog):

    def __init__(self, filename):
        msg = _("Select an alternative data file for %s") % \
                        os.path.basename(filename)

        wx.FileDialog.__init__(self, None,
                           msg,
                           os.path.dirname(filename),
                           os.path.basename(filename),
                           _("Shapefiles (*.shp)") + "|*.shp;*.SHP|" +
                           _("All Files (*.*)") + "|*.*",
                           wx.OPEN)

    def RunDialog(self):
        val = self.ShowModal()
        self.Destroy()
        if val == wx.ID_OK:
            return internal_from_wxstring(self.GetPaths()[0])
        else:
            return None

class AltPathConfirmDialog(wx.MessageDialog):

    def __init__(self, filename):
        self.filename = filename
        msg = _("Found the following as an alternative for '%s':\n%s\n\nPlease confirm with Yes or select alternative with No.") % (os.path.basename(filename), filename)

        wx.MessageDialog.__init__(self, None, msg, _("Alternative Path"),
                        wx.YES_NO|wx.YES_DEFAULT|wx.ICON_INFORMATION)

    def RunDialog(self):
        val = self.ShowModal()
        self.Destroy()
        if val == wx.ID_YES:
            return self.filename
        else:
            dlg = AltPathFileDialog(self.filename)
            fname = dlg.RunDialog()
            return fname
