# Copyright (c) 2005, 2007 by Intevation GmbH
# Authors:
# Jonathan Coles <jonathan@intevation.de> 
# Bernhard Reiter <bernhard@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""Raster Layer Properties dialog"""

__version__ = "$Revision: 2774 $"
# $Source$
# $Id: rasterlayerproperties.py 2774 2007-06-22 22:14:15Z bernhard $

import wx

from Thuban import _
from Thuban.UI.layerproperties import LayerProperties
from Thuban.Model.resource import has_gdal_support, gdal_support_status

from Thuban.version import versions

#ID_RB_MASK = 4002

class RasterLayerProperties(LayerProperties):

    def __init__(self, parent, name, layer, *args, **kw):
        LayerProperties.__init__(self, parent, name, layer)

        #self.old_state = {}
        #self.old_state["mask_type"] = layer.MaskType()

        LayerProperties.dialog_layout(self)

    def dialog_layout(self, panel, panelBox):

        info = self.layer.ImageInfo()

        if info is None:
            panelBox.Add(
                wx.StaticText(panel, -1,
                _("GDAL image information unavailable. See About box for details.")),
                0, wx.ALIGN_LEFT | wx.ALL, 4)
            return


        # Bounding Box
        bbox = self.layer.LatLongBoundingBox()
        if bbox is None:
            text = _("Extent (lat-lon): None")
        else:
            text = _("Extent (lat-lon): (%g, %g, %g, %g)") % tuple(bbox)

        panelBox.Add(wx.StaticText(panel, -1, text), 0, wx.ALIGN_LEFT|wx.ALL, 4)

        rasterBox = wx.StaticBoxSizer(wx.StaticBox(panel, -1,
                    _("Image Properties")), wx.VERTICAL)


        rasterBox.Add(
            wx.StaticText(panel, -1,
            _("Source: %s") % self.layer.GetImageFilename()),
            0, wx.ALIGN_LEFT | wx.ALL, 4)

        infoBox = wx.BoxSizer(wx.HORIZONTAL)

        nBands = info["nBands"]

        self.usePalIndex = nBands == 1

        infoBox.Add(
            wx.StaticText(panel, -1, _("Driver: %s") % info["Driver"]),
            0, wx.ALIGN_LEFT | wx.RIGHT, 10)
        infoBox.Add(
            wx.StaticText(panel, -1, _("Size: %ix%i") % info["Size"]),
            0, wx.ALIGN_LEFT | wx.RIGHT, 10)
        infoBox.Add(
            wx.StaticText(panel, -1, _("Number of Bands: %i") % nBands),
            0, wx.ALIGN_LEFT | wx.RIGHT, 0)

        rasterBox.Add(infoBox, 0, wx.ALIGN_LEFT|wx.ALL, 4)

        # Mask 

        #maskBox = wx.BoxSizer(wx.HORIZONTAL)

        # choices = ["None", "Bitmap", "Alpha"]

        #self.maskRadioBox = wx.RadioBox(panel, ID_RB_MASK, _("Mask Type"),
        #                    choices=choices)
        #maskBox.Add(self.maskRadioBox, 0, wx.RIGHT, 10)

        self.opBox = wx.BoxSizer(wx.HORIZONTAL)
        self.opSpinLabel = wx.StaticText(panel, -1, _("Opacity:"))
        self.opBox.Add(self.opSpinLabel, 0, 
		      wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 4)
        self.opSpin = wx.SpinCtrl(panel, -1,
                                 str(self.layer.Opacity()*255),
                                 initial = self.layer.Opacity()*255,
                                 min=0, max=255)
        self.opBox.Add(self.opSpin, 0, wx.ALL, 4)
        #maskBox.Add(self.opBox, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)

        #rasterBox.Add(maskBox, 0, wx.ALL, 4)

        rasterBox.Add(self.opBox, 0, wx.ALL, 4)

        panelBox.Add(rasterBox, 1, wx.GROW | wx.ALL, 4)

        #self.maskRadioBox.SetSelection(self.old_state["mask_type"])

        #self.OnMaskSelect(None)

        #self.Bind(wx.EVT_RADIOBOX, self.OnMaskSelect, id=ID_RB_MASK)

    def OnTry(self, event):
        self.set_state()

    def OnOK(self, event):
        if self.set_state():
            self.Close()

    def OnRevert(self, event):
        #self.maskRadioBox.SetSelection(self.old_state["mask_type"])
        self.set_state()

    #def OnMaskSelect(self, event):
    #    allowOpacity = self.maskRadioBox.GetSelection()==2
    #    self.opSpin.Enable(allowOpacity)
    #    self.opSpinLabel.Enable(allowOpacity)

    def set_state(self):
        #self.layer.SetMaskType(self.maskRadioBox.GetSelection())
        self.layer.SetOpacity(self.opSpin.GetValue()/255.0)
        return True
