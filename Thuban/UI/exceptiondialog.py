# Copyright (c) 2003 by Intevation GmbH
# Authors:
# Jan-Oliver Wagnber <jan@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

__version__ = "$Revision: 2700 $"

import sys

import wx

from Thuban import _

class ExceptionDialog(wx.Dialog):

    """The exception dialog shows the exception message and then allows
    to either proceed with the application or to exit it. In the
    latter case, sys.exit(1) is applied immediately.
    """

    def __init__(self, parent, message, title = _('Thuban: Internal Error')):
        wx.Dialog.__init__(self, parent, -1, title,
                          wx.DefaultPosition,
                          style = wx.RESIZE_BORDER|wx.CAPTION|wx.DIALOG_MODAL)

        self.parent = parent
        self.dialog_layout(message)

    def dialog_layout(self, message):
        top_box = wx.BoxSizer(wx.VERTICAL)

        textBox = wx.TextCtrl(self, -1, message,
            style=wx.TE_READONLY|wx.TE_MULTILINE|wx.TE_LINEWRAP)
        w, h = (500, 300)
        textBox.SetSizeHints(w, h)
        textBox.SetSize((w, h))

        top_box.Add(textBox, 1, wx.EXPAND|wx.ALL, 10)

        box = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(wx.Button(self, wx.ID_OK, _("Proceed")), 0, wx.ALL, 4)
        box.Add(wx.Button(self, wx.ID_CANCEL, _("Exit Thuban now")), 0, wx.ALL, 4)
        top_box.Add(box, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 10)

        self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)
        self.Bind(wx.EVT_BUTTON, self.OnExit, id=wx.ID_CANCEL)

        self.SetAutoLayout(True)
        self.SetSizer(top_box)
        top_box.Fit(self)
        top_box.SetSizeHints(self)

    def OnOK(self, event):
        self.EndModal(wx.ID_OK)

    def OnExit(self, event):
        sys.exit(1)

def run_exception_dialog(parent, message, title = _('Thuban: Internal Error')):
    dialog = ExceptionDialog(parent, message, title)
    dialog.ShowModal()
    dialog.Destroy()
