# Copyright (c) 2001-2007 by Intevation GmbH 
# vim: set fileencoding=ISO-8859-15 :
# Authors:
# Jonathan Coles <jonathan@intevation.de>
# Bernhard Reiter <bernhard@intevation.de>
# Silke Reimer <silke@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""The About Box"""

__version__ = "$Revision: 2825 $"
# $Source$
# $Id: about.py 2825 2008-01-31 13:58:23Z bernhard $

import wx

from Thuban import _, internal_from_unicode, get_internal_encoding
from Thuban.version import versions
from Thuban.Model.resource import gdal_support_status

from Thuban.UI.extensionregistry import ext_registry

class About(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, -1, _("About Thuban"),
            style = wx.DEFAULT_DIALOG_STYLE
                  | wx.SYSTEM_MENU
                  | wx.MINIMIZE_BOX
                  | wx.MAXIMIZE_BOX
                  | wx.RESIZE_BORDER,
            size = (400, 250))

        # Note: The source code is in ASCII, so we escape some
        # characters to get byte strings in latin1.
        maintainers = [ 'Bernhard Reiter (2006 - current)',
                        'Bernhard Herzog (2001 - 2005)' ]
        lead_developer = 'Bernhard Herzog (2001 - 2005)'
        developers = [ 'Jonathan Coles (2003, 2005)',
                       'Frank Koormann (2002 - )',
                       internal_from_unicode(u'Martin M\xfcller (2003)'),
                       'Didrik Pinte (2006 - )',
                       'Bernhard Reiter (2004 - )',
                       'Jan-Oliver Wagner (2002 - 2005)' ]
        translators = [ ( _('French'), 'Daniel Calvelo Aros, Didrik Pinte' ),
                        ( _('German'),
                        internal_from_unicode(u'Bj\xf6rn Broscheit (2003)') +
                            ', Bernhard Reiter') ,
                        ( _('Hungarian'), 'Norbert Solymosi'),
                        ( _('Italian'), 'Maurizio Napolitano'),
                        ( _('Portuguese (Brazilian)'), 'Eduardo Patto Kanegae'),
                        ( _('Russian'), 'Alex Shevlakov'),
                        ( _('Spanish'), 'Daniel Calvelo Aros'),
                        ( _('Czech'), 'Jachym Cepicky') ]
        other_contributors = [ 'Jonathan Byron (2004)',
                               'Ole Rahn (2004)',
                               'Silke Reimer (2003, 2004)',
                               'Martin "Joey" Schulze (2004, 2005)' ]
        dyn_modules = [ ('wxPython', versions['wxPython']),
                        ('Python',   versions['python']),
                        ('PySQLite', versions['pysqlite']),
                        ('SQLite',  versions['sqlite']),
                        ('GDAL', versions.get('gdal', _('- not available'))),
                        ('psycopg', versions.get('psycopg',
                                                 _('- not available')))]
        direct_modules = [ \
                ('GTK',      versions.get('gtk', _('- not available'))),
                ('proj',     versions['proj']) ]

        text = 'Thuban %s\n\n' % versions['thuban-long']

        text += _('Currently using:\n')

        for name, version in dyn_modules:
            text+= '\t%s %s\n' % (name, version)
        text += '\n'

        text += _('\tInternal encoding: %s\n') % get_internal_encoding()
        text += '\n'

        if gdal_support_status:
            text += gdal_support_status + "\n\n"

        text += _('Compiled for:\n')

        for name, version in direct_modules:
            text+= '\t%s %s\n' % (name, version)
        text += '\n'

        text += _('Extensions:\n')
        if ext_registry:
            for ext in ext_registry:
                text += '\t%s %s\n' % (ext.name, ext.version)
        else:
            text += _('\tNone registered.\n')
        text += '\n'

        text += _('Maintainers:\n')
        for name in maintainers:
            text += '\t%s\n' % name
        text += '\n'

        text += _('Lead Developer:\n')
        text += '\t%s\n\n' % lead_developer

        text += _('Developers:\n')
        for name in developers:
            text += '\t%s\n' % name
        text += '\n'

        text += _('Translators:\n')
        for lang, name in translators:
            text += '\t%s: %s\n' % (lang, name)
        text += '\n'

        text += _('Other Contributors:\n')
        for name in other_contributors:
            text += '\t%s\n' % name
        text += '\n'

        text += \
            _("Questions and comments can be sent to the following addresses:\n"
            "\tGeneral list (public):\n\t\t<thuban-list@intevation.de>\n"
            "\tDevelopers list (public):\n\t\t<thuban-devel@intevation.de>\n"
            "\tThuban team at Intevation:\n\t\t<thuban@intevation.de>\n"
            )

        text += '\n\n'

        text += _("Details on the registered extensions:\n\n")

        if ext_registry:
            for ext in ext_registry:
                text += '%s %s:\n' % (ext.name, ext.version)
                text += _('Copyright %s\n') % ext.copyright
                text += _('Authors:\n')
                for author in ext.authors:
                    text+= '\t%s\n' % author
                text += ext.desc
                text += '\n'
                text += 'Status: %s' % ext.status
                text += '\n\n'
        else:
            text += _('\tNone registered.\n')

        self.text = text

        text_title = wx.StaticText(self, -1,
            _("Thuban is a program for exploring geographic data.\n\n") +
            "Copyright 2001-2008 Intevation GmbH.\n" +
            _("Thuban is licensed under the GNU GPL v>=2"),
                                  style=wx.ST_NO_AUTORESIZE|wx.ALIGN_CENTRE)

        textBox = wx.TextCtrl(self, -1, text,
            style=wx.TE_READONLY|wx.TE_MULTILINE|wx.TE_LINEWRAP)
        w, h = (300, 150)
        textBox.SetSizeHints(w, h)
        textBox.SetSize((w, h))

        button_close = wx.Button(self, wx.ID_CANCEL, _("Close"))
        button_close.SetDefault()

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(text_title, 0, wx.ALL|wx.EXPAND|wx.ADJUST_MINSIZE, 10)
        sizer.Add(textBox, 1, wx.ALL|wx.EXPAND, 10)
        sizer.Add(button_close, 0, wx.ALL|wx.ALIGN_RIGHT, 10)

        self.SetAutoLayout(True)
        self.SetSizer(sizer)
        sizer.Fit(self)
        sizer.SetSizeHints(self)
        self.Layout()

        self.Bind(wx.EVT_BUTTON, self.OnCancel, id=wx.ID_CANCEL)

    def OnCancel(self, event):
        self.EndModal(wx.ID_CANCEL)
