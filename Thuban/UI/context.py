# Copyright (C) 2001, 2002 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
The command execution context,
"""

__version__ = "$Revision: 374 $"

class Context:

    """Command Execution Context

    Public Attributes:

       application -- The application object
       session -- The session object
       mainwindow -- The main window

    """

    def __init__(self, application, session, mainwindow):
        self.application = application
        self.session = session
        self.mainwindow = mainwindow
