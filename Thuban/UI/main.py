# Copyright (C) 2001, 2002, 2003 by Intevation GmbH
# Authors:
# Jan-Oliver Wagner <jan@intevation.de>
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
The main entry point for the Thuban GUI.
"""

__version__ = "$Revision: 2700 $"

import sys
import getopt

import application
import Thuban.version


class options:

    attribute_editing_enabled = False


def main():
    """Instantiate the application object and run the application"""

    if verify_versions():
        app = application.ThubanApplication(0)
        opts, args = getopt.getopt(sys.argv[1:], '',
                                   ['enable-attribute-editing'])
        for optchar, value in opts:
            if optchar == '--enable-attribute-editing':
                options.attribute_editing_enabled = True
            else:
                print >>sys.stderr, "Unknown option", optchar

        # If there was a non-flag argument it's the name of a thuban
        # file.
        if args:
            app.OpenSession(args[0])

        app.MainLoop()
        # sys.excepthook is set in ThubanApplication.OnInit()
        sys.excepthook = sys.__excepthook__


def verify_versions():
    """Check some library versions.

    Print a message containing any libraries which are wrong.
    Return True if everything is OK, otherwise False.
    """

    errors = Thuban.version.verify_versions()

    if len(errors) > 0:
        msg = " The following version errors were detected:"

        for e in errors:
            msg += "\n     " + e

#       if use_msg_box:
#           # XXX: use a message box to display the errors
#           pass

        print "\n*******************************************************"
        print msg
        print "*******************************************************\n"

        return False

    return True
