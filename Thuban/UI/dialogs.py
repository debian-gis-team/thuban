# Copyright (c) 2001, 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
# Frank Koormann <frank.koormann@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""Base classes for dialogs"""

__version__ = "$Revision: 2700 $"

import wx

class ThubanFrame(wx.Frame):

    def __init__(self, parent, name, title):
        wx.Frame.__init__(self, parent, -1, title,
            style = wx.DEFAULT_FRAME_STYLE )
        self.parent = parent
        self.name = name
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.Bind(wx.EVT_WINDOW_DESTROY, self.OnDestroy)

    def OnClose(self, event):
        # FIXME: Shouldn't this really be in OnDestroy?
        if self.parent.dialogs.has_key(self.name):
            self.parent.remove_dialog(self.name)
        self.Destroy()

    def OnDestroy(self, event):
        """Implement in derived classes for resource cleanup, etc."""


class NonModalDialog(wx.Dialog):
    def __init__(self, parent, name, title):
        wx.Dialog.__init__(self, parent, -1, title,
            style = wx.DEFAULT_DIALOG_STYLE
                  | wx.SYSTEM_MENU
                  | wx.MINIMIZE_BOX
                  | wx.MAXIMIZE_BOX
                  | wx.RESIZE_BORDER
            )
        self.parent = parent
        self.name = name
        self.Bind(wx.EVT_CLOSE, self.OnClose)

    def OnClose(self, event):
        if self.parent.dialogs.has_key(self.name):
            self.parent.remove_dialog(self.name)
        self.Destroy()

class NonModalNonParentDialog(wx.Dialog):
    def __init__(self, parent, name, title):
        wx.Dialog.__init__(self, None, -1, title,
            style = wx.DEFAULT_DIALOG_STYLE
                  | wx.SYSTEM_MENU
                  | wx.MINIMIZE_BOX
                  | wx.MAXIMIZE_BOX
                  | wx.RESIZE_BORDER
                  | wx.DIALOG_NO_PARENT
            )
        self.parent = parent
        self.name = name
        self.Bind(wx.EVT_CLOSE, self.OnClose)

    def OnClose(self, event):
        if self.parent.dialogs.has_key(self.name):
            self.parent.remove_dialog(self.name)
        self.Destroy()
