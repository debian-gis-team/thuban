# Copyright (c) 2001, 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Define the message types used by the classes implementing Thuban's data
model. The messages types are simply strings. The message system itself
is implemented in Thuban.Lib.connector.
"""
__version__ = "$Revision: 1823 $"

# Common message types
TITLE_CHANGED = "TITLE_CHANGED"
CHANGED = "CHANGED"

# ProjFile message types
PROJECTION_ADDED = "PROJECTION_ADDED"
PROJECTION_REPLACED = "PROJECTION_REPLACED"
PROJECTION_REMOVED = "PROJECTION_REMOVED"

# classification specific message types
CLASS_CHANGED = "CLASS_CHANGED"

# layer specific message types
LAYER_PROJECTION_CHANGED = "LAYER_PROJECTION_CHANGED"
LAYER_LEGEND_CHANGED = "LAYER_LEGEND_CHANGED"
LAYER_VISIBILITY_CHANGED = "LAYER_VISIBILITY_CHANGED"
LAYER_CHANGED = "LAYER_CHANGED"
LAYER_SHAPESTORE_REPLACED = "LAYER_SHAPESTORE_REPLACED"

# Map specific message types
MAP_STACKING_CHANGED = "MAP_STACKING_CHANGED"
MAP_LAYERS_CHANGED = "MAP_LAYERS_CHANGED"
MAP_LAYERS_ADDED = "MAP_LAYERS_ADDED"
MAP_LAYERS_REMOVED = "MAP_LAYERS_REMOVED"
MAP_PROJECTION_CHANGED = "MAP_PROJECTION_CHANGED"

# Extension specific message types
EXTENSION_CHANGED = "EXTENSION_CHANGED"
EXTENSION_OBJECTS_CHANGED = "EXTENSION_OBJECTS_CHANGED"

# Session specific message types
MAPS_CHANGED = "MAPS_CHANGED"
EXTENSIONS_CHANGED = "EXTENSIONS_CHANGED"
FILENAME_CHANGED = "FILENAME_CHANGED"

TABLE_REMOVED = "TABLE_REMOVED"

DBCONN_ADDED = "DBCONN_ADDED"
DBCONN_REMOVED = "DBCONN_REMOVED"
