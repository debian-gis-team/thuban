# Copyright (c) 2001, 2002, 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Various base classes that did not fit elsewhere.
"""

__version__ = "$Revision: 2413 $"

from Thuban.Lib.connector import Publisher

from messages import TITLE_CHANGED, CHANGED

class TitledObject:

    """Mix-in class for objects that have titles"""

    def __init__(self, title):
        self.title = title

    def Title(self):
        return self.title

    def SetTitle(self, title):
        self.title = title

        # FIXME: The TitledObject is almost always used in conjunction
        # with Modifiable (the only exceptions currently are the
        # tables). We should reall derive TitledObject from modifiable
        # (would be good for the tables too) but that's taking things a
        # bit far at the moment.
        if hasattr(self, 'changed'):
            self.changed(TITLE_CHANGED, self)

class Modifiable(Publisher):

    """Class for objects maintaining a modified flag."""

    def __init__(self):
        self.modified = 0

    def WasModified(self):
        """Return true if the layer was modified"""
        return self.modified

    def UnsetModified(self):
        """Unset the modified flag.

        If the modified flag is changed from set to unset by the call,
        issue a CHANGED message.

        The modified flag itself is part of the state of the object so
        some other objects such as a field in the status bar indication
        whether e.g. the session has changed might be interested in
        being notified when this flag has changed.
        """
        was_modified = self.modified
        self.modified = 0
        if was_modified:
            self.issue(CHANGED)

    def changed(self, channel = None, *args):
        """Set the modified flag and optionally issue a message

        The message is issued on the channel given by channel with args
        as the arguments. If channel is None issue no message.

        Subclasses should call this method whenever anything has
        changed.
        """
        self.modified = 1
        if channel is not None:
            self.issue(channel, *args)
