# Copyright (c) 2001, 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
# Jonathan Coles <jonathan@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

__version__ = "$Revision: 1546 $"

class Color:
    """An RGB color. RGB values are floats in the range 0.0 .. 1.0

    Color objects should be treated as immutable
    """

    def __init__(self, red, green, blue):
        self.red = red
        self.green = green
        self.blue = blue

    def hex(self):
        """Return the color as a HTML/CSS hex code"""
        return "#%02x%02x%02x" % (255 * self.red,
                                  255 * self.green,
                                  255 * self.blue)

    def __eq__(self, other):
        """Test equality with other Color objects."""
        return isinstance(other, Color)   \
            and self.red   == other.red   \
            and self.green == other.green \
            and self.blue  == other.blue

    def __ne__(self, other):
        """Test inequality with other Color objects."""
        return not self.__eq__(other)

    def __repr__(self):
        """Return a string of the rgb values"""
        return "Color(%r, %r, %r)" % (self.red, self.green, self.blue)

class _Transparent: 
    """An object which represents no color."""

    def hex(self):
        return "None"

    def __eq__(self, other):
        return isinstance(other, _Transparent)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return "Transparent"

Transparent = _Transparent()
Black = Color(0, 0, 0)

