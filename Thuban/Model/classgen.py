# -*- encoding: iso-8859-1 -*-
#
# Copyright (c) 2003-2004 by Intevation GmbH
# Authors:
# Jan-Oliver Wagner <jan@intevation.de> (2004)
# Bernhard Herzog <bh@intevation.de> (2003)
# Thomas K�ster <tkoester@intevation.de> (2003)
# Jonathan Coles <jonathan@intevation.de> (2003)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Functions to generate Classifications
"""

__version__ = "$Revision: 2441 $"
# $Source$
# $Id: classgen.py 2441 2004-12-09 10:50:34Z joey $

import operator

from color import Color, Transparent
from range import Range
from classification import Classification, ClassGroupSingleton, \
    ClassGroupRange, ClassGroupProperties

def generate_singletons(_list, ramp):
    """Generate a new classification consisting solely of singletons.

    The resulting classification will consist of one group for each
    item in _list whose properties ramp between 'prop1' and 'prop2'. 

    _list -- a list of values for each singleton

    ramp -- an object which implements the CustomRamp interface
    """

    clazz = Classification()

    i = 0
    maxValue = float(len(_list) - 1)
    if maxValue < 1: maxValue = 1

    for value in _list:
        prop = ramp.GetProperties(i / maxValue)
        clazz.AppendGroup(ClassGroupSingleton(value, prop))
        i += 1

    return clazz

def generate_uniform_distribution(min, max, numGroups, ramp, intStep = False):
    """Generate a classification with numGroups range groups
    each with the same interval.

    intStep -- force the calculated stepping to an integer.
               Useful if the values are integers but the
               number of groups specified doesn't evenly
               divide (max - min).
    """

    clazz = Classification()

    cur_min = min

    end = "["
    maxValue = float(numGroups - 1)
    if maxValue < 1: maxValue = 1

    for i in range(1, numGroups + 1):

        prop = ramp.GetProperties(float(i-1) / maxValue)

        if intStep:
            cur_max = min + int(round((i * (max - min + 1)) / float(numGroups)))
        else:
            cur_max = min + (i * (max - min)) / float(numGroups)

        if i == numGroups:
            cur_max = max
            end = "]"

        if cur_min == cur_max:
            _range = Range(("[", cur_min, cur_max, "]"))
        else:
            _range = Range(("[", cur_min, cur_max, end))

        clazz.AppendGroup(ClassGroupRange(_range, prop))

        cur_min = cur_max

    return clazz

def generate_quantiles(_list, percents, ramp, _range):
    """Generates a Classification which has groups of ranges that
    represent quantiles of _list at the percentages given in percents.
    Only the values that fall within _range are considered. 

    Returns a tuple (adjusted, Classification) where adjusted is
    True if the Classification does not exactly represent the given
    range, or if the Classification is empty.

    _list -- a sort list of values

    percents -- a sorted list of floats in the range 0.0-1.0 which
                represent the upper bound of each quantile. the
                union of all percentiles should be the entire 
                range from 0.0-1.0

    ramp -- an object which implements the CustomRamp interface

    _range -- a Range object

    Raises a Value Error if 'percents' has fewer than two items, or
    does not cover the entire range.
    """

    clazz = Classification()
    quantiles = calculate_quantiles(_list, percents, _range)
    adjusted = True

    if quantiles is not None:

        numGroups = len(quantiles[3])

        if numGroups != 0:

            adjusted = quantiles[0]

            start, min, endMax, right = _range.GetRange()

            oldp = 0
            i = 1
            end = "]"

            maxValue = float(numGroups - 1)
            if maxValue < 1: maxValue = 1
            for (q, p) in quantiles[3]: 

                prop = ramp.GetProperties(float(i-1) / maxValue)

                if i == numGroups:
                    max = endMax
                    end = right
                else:
                    max = _list[q]

                group = ClassGroupRange(Range((start, min, max, end)), prop)

                group.SetLabel("%s%% - %s%%" % (round(oldp*100, 2), 
                                                round(p*100, 2)))
                oldp = p
                start = "]"
                min = max
                clazz.AppendGroup(group)
                i += 1

    return (adjusted, clazz)


def calculate_quantiles(_list, percents, _range):
    """Calculate quantiles for the given _list of percents from the
    sorted list of values that are in range.

    This may not actually generate len(percents) quantiles if
    many of the values that fall on quantile borders are the same.

    Returns a tuple of the form: 
        (adjusted, minIndex, maxIndex, [quantile_list])

    where adjusted is True if the the quantile percentages differ from
    those supplied, minIndex is the index into _list where the 
    minimum value used is located, maxIndex is the index into _list
    where the maximum value used is located, and quantile_list is a 
    list of tuples of the form: (list_index, quantile_percentage)

    Returns None, if no quantiles could be generated based on the
    given range or input list.

    _list -- a sort list of values

    percents -- a sorted list of floats in the range 0.0-1.0 which
                represent the upper bound of each quantile. the
                union of all percentiles should be the entire 
                range from 0.0-1.0

    _range -- a Range object

    Raises a Value Error if 'percents' has fewer than two items, or
    does not cover the entire range.
    """

    quantiles = []
    adjusted = False

    if len(percents) <= 1:
        raise ValueError("percents parameter must have more than one item")

    if percents[-1] != 1.0:
        raise ValueError("percents does not cover the entire range")

    #
    # find what part of the _list range covers
    #
    minIndex = -1
    maxIndex = -2
    for i in xrange(0, len(_list), 1):
        if operator.contains(_range, _list[i]):
            minIndex = i
            break

    for i in xrange(len(_list)-1, -1, -1):
        if operator.contains(_range, _list[i]):
            maxIndex = i
            break

    numValues = maxIndex - minIndex + 1

    if numValues > 0:

        #
        # build a list of unique indices into list of where each
        # quantile *should* be. set adjusted if the resulting
        # indices are different
        #
        quantiles = {}
        for p in percents:
            index = min(minIndex + int(p*numValues)-1, maxIndex)

            adjusted = adjusted \
                or quantiles.has_key(index) \
                or ((index - minIndex + 1) / float(numValues)) != p

            quantiles[index] = 0

        quantiles = quantiles.keys()
        quantiles.sort()

        #
        # the current quantile index must be strictly greater than
        # the lowerBound
        #
        lowerBound = minIndex - 1

        for qindex in xrange(len(quantiles)):
            if lowerBound >= maxIndex:
                # discard higher quantiles
                quantiles = quantiles[:qindex]
                break

            # lowerBound + 1 is always a valid index

            #
            # bump up the current quantile index to be a usable index
            # if it currently falls below the lowerBound
            #
            if quantiles[qindex] <= lowerBound:
                quantiles[qindex] = lowerBound + 1

            listIndex = quantiles[qindex]
            value = _list[listIndex]

            #
            # look for similar values around the quantile index
            #
            lindex = listIndex - 1
            while lindex > lowerBound and value == _list[lindex]:
                lindex -= 1
            lcount = (listIndex - 1) - lindex

            rindex = listIndex + 1
            while rindex < maxIndex + 1 and value == _list[rindex]:
                rindex += 1
            rcount = (listIndex + 1) - rindex

            #
            # adjust the current quantile index based on how many 
            # numbers in the _list are the same as the current value
            #
            newIndex = listIndex
            if lcount == rcount:
                if lcount != 0:
                    #
                    # there are an equal number of numbers to the left
                    # and right, try going to the left first unless
                    # doing so creates an empty quantile.
                    #
                    if lindex != lowerBound:
                        newIndex = lindex
                    else:
                        newIndex = rindex - 1

            elif lcount < rcount:
                # there are fewer items to the left, so 
                # try going to the left first unless
                # doing so creates an empty quantile.
                if lindex != lowerBound:
                    newIndex = lindex
                else:
                    newIndex = rindex - 1

            elif rcount < lcount:
                # there are fewer items to the right, so go to the right
                newIndex = rindex - 1

            adjusted = adjusted or newIndex != listIndex

            quantiles[qindex] = newIndex
            lowerBound = quantiles[qindex]

    if len(quantiles) == 0:
        return None
    else:
        return (adjusted, minIndex, maxIndex,
                [(q, (q - minIndex+1) / float(numValues)) \
                 for q in quantiles])

class CustomRamp:

    def __init__(self, prop1, prop2):
        """Create a ramp between prop1 and prop2."""
        self.prop1 = prop1
        self.prop2 = prop2

    def GetRamp(self):
        """Return this ramp."""
        return self

    def GetProperties(self, index):
        """Return a ClassGroupProperties object whose properties
        represent a point at 'index' between prop1 and prop2 in
        the constructor.

        index -- a value such that 0 <= index <= 1
        """

        if not (0 <= index <= 1):
            raise ValueError(_("invalid index"))

        newProps = ClassGroupProperties()

        self.__SetProperty(self.prop1.GetLineColor(),
                           self.prop2.GetLineColor(),
                           index, newProps.SetLineColor)
        self.__SetProperty(self.prop1.GetFill(), self.prop2.GetFill(),
                           index, newProps.SetFill)

        w = (self.prop2.GetLineWidth() - self.prop1.GetLineWidth()) \
            * index \
            + self.prop1.GetLineWidth()
        newProps.SetLineWidth(int(round(w)))

        s = (self.prop2.GetSize() - self.prop1.GetSize()) \
            * index \
            + self.prop1.GetSize()
        newProps.SetSize(int(round(s)))

        return newProps

    def __SetProperty(self, color1, color2, index, setf):
        """Use setf to set the appropriate property for the point
        index percent between color1 and color2. setf is a function
        to call that accepts a Color object or Transparent.
        """

        if color1 is Transparent and color2 is Transparent:
            setf(Transparent)
        elif color1 is Transparent:
            setf(Color(
                 color2.red   * index,
                 color2.green * index,
                 color2.blue  * index))
        elif color2 is Transparent:
            setf(Color(
                 color1.red   * index,
                 color1.green * index,
                 color1.blue  * index))
        else:
            setf(Color(
                (color2.red   - color1.red)   * index + color1.red,
                (color2.green - color1.green) * index + color1.green,
                (color2.blue  - color1.blue)  * index + color1.blue))

class MonochromaticRamp(CustomRamp):
    """Helper class to make ramps between two colors."""

    def __init__(self, start, end):
        """Create a Monochromatic Ramp.

        start -- starting Color

        end -- ending Color
        """
        sp = ClassGroupProperties()
        sp.SetLineColor(start)
        sp.SetFill(start)

        ep = ClassGroupProperties()
        ep.SetLineColor(end)
        ep.SetFill(end)

        CustomRamp.__init__(self, sp, ep)

grey_ramp         = MonochromaticRamp(Color(1, 1, 1),  Color(0, 0, 0))
red_ramp          = MonochromaticRamp(Color(1, 1, 1),  Color(.8, 0, 0))
green_ramp        = MonochromaticRamp(Color(1, 1, 1),  Color(0, .8, 0))
blue_ramp         = MonochromaticRamp(Color(1, 1, 1),  Color(0, 0, .8))
green_to_red_ramp = MonochromaticRamp(Color(0, .8, 0), Color(1, 0, 0))

class HotToColdRamp:
    """A ramp that generates properties with colors ranging from
    'hot' colors (e.g. red, orange) to 'cold' colors (e.g. green, blue)
    """

    def GetRamp(self):
        """Return this ramp."""
        return self

    def GetProperties(self, index):
        """Return a ClassGroupProperties object whose properties
        represent a point at 'index' between "hot" and "cold".

        index -- a value such that 0 <= index <= 1
        """

        clr = [1.0, 1.0, 1.0]

        if index < .25:
            clr[0] = 0
            clr[1] = 4 * index
        elif index < .5:
            clr[0] = 0
            clr[2] = 1 + 4 * (.25 - index)
        elif index < .75:
            clr[0] = 4 * (index - .5)
            clr[2] = 0
        else:
            clr[1] = 1 + 4 * (.75 - index)
            clr[2] = 0

        prop = ClassGroupProperties()
        prop.SetLineColor(Color(clr[0], clr[1], clr[2]))
        prop.SetFill(Color(clr[0], clr[1], clr[2]))

        return prop

class FixedRamp:
    """FixedRamp allows particular properties of a ramp to be
    held constant over the ramp.
    """

    def __init__(self, ramp, fixes):
        """
        ramp -- a source ramp to get the default properties

        fixes -- a tuple (lineColor, lineWidth, fillColor) such that
             if any item is not None, the appropriate property will 
             be fixed to that item value.
        """

        self.fixes = fixes
        self.ramp = ramp

    def GetRamp(self):
        """Return this ramp."""
        return self

    def GetProperties(self, index):
        """Return a ClassGroupProperties object whose properties
        represent a point at 'index' between the properties in 
        the ramp that initialized this FixedRamp.

        index -- a value such that 0 <= index <= 1
        """

        props = self.ramp.GetProperties(index)
        if self.fixes[0] is not None: props.SetLineColor(self.fixes[0])
        if self.fixes[1] is not None: props.SetLineWidth(self.fixes[1])
        if self.fixes[2] is not None: props.SetFill(self.fixes[2])

        return props
