# Copyright (c) 2001, 2002, 2005 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

__version__ = "$Revision: 2569 $"

from Thuban import _

from messages import CHANGED

from base import TitledObject, Modifiable

ALIGN_CENTER = "center"
ALIGN_TOP = "top"
ALIGN_BOTTOM = "bottom"
ALIGN_LEFT = "left"
ALIGN_RIGHT = "right"
ALIGN_BASELINE = "baseline"

class Label:

    """This class repesents a single label.

    The label is defined by its coordinate, the text as well
    as vertical and horizontal alignment concerning
    the coordinate.
    """

    def __init__(self, x, y, text, halign, valign):
        """Initialize the label with the given parameters."""
        self.x = x
        self.y = y
        self.text = text
        self.halign = halign
        self.valign = valign


class LabelLayer(TitledObject, Modifiable):

    """This represent a layer holding a number of labels."""

    def __init__(self, title):
        """Initialize the LabeleLayer.

        Initialization is done with an empty
        list of labels and set the title to "title".
        """
        TitledObject.__init__(self, title)
        Modifiable.__init__(self)
        self.labels = []

    def Labels(self):
        """Return a list of all labels."""
        return self.labels

    def AddLabel(self, x, y, text, halign = ALIGN_LEFT,
                 valign = ALIGN_CENTER):
        """Add a label at position (x,y) with contents "text".

        This will emit a CHANGED signal.
        """
        self.labels.append(Label(x, y, text, halign, valign))
        self.changed(CHANGED)

    def RemoveLabel(self, index):
        """Remove the label specified by index.

        This will emit a CHANGED signal.
        """
        del self.labels[index]
        self.changed(CHANGED)

    def ClearLabels(self):
        """Remove all labels.

        This will emit a CHANGED signal.
        """
        del self.labels[:]
        self.changed(CHANGED)

    def TreeInfo(self):
        """Return a description of the object.

        A tuple of (title, tupel) describing the contents
        of the object in a tree-structure is returned.
        """
        items = []
        items.append(_("Number of labels: %d") % len(self.labels))
        return (_("Label Layer: %s") % self.title, items)
