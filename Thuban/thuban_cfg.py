# Copyright (C) 2008 by Intevation GmbH
# Author(s):
# Bernhard Reiter <bernhard@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

"""
Installation wide configuration file.
"""

try:
    import Extensions.bboxdump
except Exception, x:
    print x

try:
    import Extensions.export_shapefile
except Exception, x:
    print x

try:
    import Extensions.gns2shp
except Exception, x:
    print x

try:
    import Extensions.importAPR
except Exception, x:
    print x

try:
    import Extensions.importMP
except Exception, x:
    print x

try:
    import Extensions.gMapTiles
except Exception, x:
    print x

try:
    import Extensions.mouseposition
except Exception, x:
    print x

try:
    import Extensions.ogr
except Exception, x:
    print x

# no import of profiling, because it uses the non-free profiling module
# of python which does not come with some distributions.

try:
    import Extensions.svgexport
except Exception, x:
    print x

try:
    import Extensions.umn_mapserver
except Exception, x:
    print x

try:
    import Extensions.wms
except Exception, x:
    print x

