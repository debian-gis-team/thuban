# Copyright (C) 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

"""Version number handling"""

__version__ = "$Revision: 2011 $"
# $Source$
# $Id: version.py 2011 2003-12-03 09:46:27Z bh $


import re

rx_version = re.compile("[0-9]+(\\.[0-9]+)*")

def make_tuple(s):
    """Return a tuple version of the version number string s

    The version string must start with a sequence of decimal integers
    separated by dots. The return value is a tuple of up to three
    integers, one for each number at the beginnin of the version string.

    If the string does not start with a number return None

    Examples:
    >>> import version
    >>> version.make_tuple('1.2')
    (1, 2)
    >>> version.make_tuple('2.4.0.7')
    (2, 4, 0)
    >>> version.make_tuple('1.2+cvs.20031111')
    (1, 2)
    >>> version.make_tuple('foo')
    >>>
    """
    match = rx_version.match(s)
    if match:
        return tuple(map(int, match.group(0).split(".")[:3]))

