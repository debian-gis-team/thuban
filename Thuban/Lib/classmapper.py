# Copyright (c) 2004 by Intevation GmbH
# Authors:
# Martin Schulze <joey@infodrom.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
The provided class maintains a mapping between classes and objects.
Instances of the class are used as keys, not classes, though.

Usually layer classes and their instances are used in combination with
layer property dialog classes in order to utilise the proper dialog
for a given layer instance.

"""

__version__ = "$Revision: 2299 $"
# $Source$
# $Id: classmapper.py 2299 2004-07-26 16:08:07Z joey $


class ClassMapper:
    """
    Thuban class to implement a mapping of classes and objects,
    usually also classes.
    """

    mapping = None


    def __init__(self):
        """
        Initialises a mapping.
        """

        self.mapping = []


    def add(self, key_class, obj):
        """
        Register an object with a class.

        This method adds the provided pair (key_class,obj) to the
        internal list.  Subsequently instances of key_class are used
        to retrieved the stored object.
        """

        self.mapping.append((key_class, obj))


    def get(self, instance):
        """
        Retrieve a stored object corresponding to the provided class
        instance.
        """

        for key_class, obj in self.mapping:
            if isinstance(instance, key_class):
                return obj
        return None


    def has(self, instance):
        """
        Determine if the ClassMapper contains an object corresponding
        to the provided instance
        """

        return self.get(instance) is not None
