# Copyright (c) 2001, 2003, 2005 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
# Jan-Oliver Wagner <jan@intevation.de>
# Bernhard Reiter <bernhard@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

import os
import sys

# Thuban Message Translation
#
# This is somewhat tricky. On the one hand we want to use the wx
# facilities because that way the standard wx messages are also
# translated and we get automatic conversion of encodings so the we can
# use e.g. an UTF po/mo file in a Latin 1 environment. OTOH, we do not
# want to import the wxPython modules at all when running the test suite
# because otherwise the test suite would require a working X server
# connection.
#
# Therefore this module only provides the hooks for installing the
# correct translation function with a default translation function that
# does nothing and simply returns the string it gets as argument.
#
# The front end to the installed translation function is _ (see it's
# doc-string).
#
# The Thuban.UI module is responsible for installing the wx translation
# function. It must take care to install the translation function as
# early as possible, i.e. when Thuban/UI/__init__.py is executed so that
# strings translated at module import time are translated (this also
# means that a program built on top of Thuban and which uses Thuban.UI
# should start by importing Thuban.UI before any other Thuban module.
#
# At the same time Thuban/UI/__init__.py should not import any wxPython
# module unless it really has to install the translation function, i.e.
# when no other translation function has already been installed. That
# way the test suite can override the wx translation by installing its
# own translation function before importing anything from Thuban.UI and
# actually before importing anything but the Thuban module itself.

# Thedirectory holding the translation files (actually they're in
# language specific subdirectories of _message_dir)



def _(s):
    """Return a localized version of the the string s

    This is the function to use in the sources to translate strings and
    it simply delegates the translation to the installable translation
    function. It's done this way so that _ may be imported with 'from
    Thuban import _' even when the correct translation function hasn't
    been installed yet.
    """
    return _translation_function(s)

from Lib.fileutil import get_thuban_dir;
_message_dir = os.path.join(get_thuban_dir(), "Resources", "Locale")

def gettext_identity(s):
    """Default gettext implementation which returns the string as is"""
    return s

_translation_function = gettext_identity

def translation_function_installed():
    """Return whether a translation function has been installed."""
    return _translation_function is not gettext_identity

def install_translation_function(function):
    """Install function as the translation function

    If a translation has already been installed that is not the default
    implementation (gettext_identity) do nothing.
    """
    global _translation_function
    if not translation_function_installed():
        _translation_function = function



# String representation in Thuban
#
# Thuban has an internal representation for textual data that all text
# that comes into Thuban has to be converted into.  Any text written by
# Thuban has to be converted to whatever is needed by the output device.
#
# Currently, the internal representation is usually a byte-string in a
# particuler encoding.  For more details see the file
# Doc/technotes/string_representation.txt.

# The encoding to use for the internal string representation.  Usually
# it's a string with the encoding name to use when converting between
# Python byte-strings and unicode objects.  The special value "unicode"
# means the use unicode objects as the internal representation.
_internal_encoding = None

def internal_from_unicode(unistr):
    """Return Thuban's internal representation for the unicode object unistr"""
    if _internal_encoding != "unicode":
        # we use replace so that we don't get exceptions when the
        # conversion can't be done properly.
        return unistr.encode(_internal_encoding, "replace")
    else:
        return unistr

def unicode_from_internal(s):
    """Return the unicode object for the string s in internal representation"""
    if _internal_encoding != "unicode":
        return unicode(s, _internal_encoding)
    else:
        return s


def set_internal_encoding(encoding):
    """Set the encoding to use for the internal string representation

    The parameter should be the name of an encoding known to Python so
    that it can be used with e.g. the encode method of unicode objects.
    As a special case it can be the string 'unicode' to indicate that
    the internal representation are unicode objects.
    """
    global _internal_encoding
    _internal_encoding = encoding

    # and now let us test, if we can go back and forth
    # it is better to complain now than to have runtime problems later
    unicode_from_internal(internal_from_unicode(u''))

def get_internal_encoding():
    """Return the encoding used for Thuban's internal string representation."""
    global _internal_encoding
    return _internal_encoding
