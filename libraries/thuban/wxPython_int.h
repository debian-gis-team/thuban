////////////////////////////////////////////////////////////////////////////
// Name:        wxPython_int.h   (int == internal)
// Purpose:     Helper functions/classes for the wxPython extension module
//              This header should only be inclued directly by those source
//              modules included in the wx._core module.  All others should
//              include wx/wxPython/wxPython.h instead.
//
// Author:      Robin Dunn
//
// Created:     1-July-1997
// RCS-ID:      $Id: wxPython_int.h 2562 2005-02-16 21:14:47Z jonathan $
// Copyright:   (c) 1998 by Total Control Software
// Licence:     wxWindows license
/////////////////////////////////////////////////////////////////////////////

#ifndef __wxp_helpers__
#define __wxp_helpers__

#include <wx/wx.h>

#if 1
#include <wx/busyinfo.h>
#include <wx/caret.h>
//#include <wx/choicebk.h>
#include <wx/clipbrd.h>
#include <wx/colordlg.h>
#include <wx/config.h>
#include <wx/cshelp.h>
//#include <wx/dcmirror.h>
#include <wx/dcps.h>
#include <wx/dirctrl.h>
#include <wx/dirdlg.h>
#include <wx/dnd.h>
#include <wx/docview.h>
#include <wx/encconv.h>
#include <wx/fdrepdlg.h>
#include <wx/fileconf.h>
#include <wx/filesys.h>
#include <wx/fontdlg.h>
#include <wx/fs_inet.h>
#include <wx/fs_mem.h>
#include <wx/fs_zip.h>
//#include <wx/gbsizer.h>
#include <wx/geometry.h>
//#include <wx/htmllbox.h>
#include <wx/image.h>
#include <wx/imaglist.h>
#include <wx/intl.h>
#include <wx/laywin.h>
//#include <wx/listbook.h>
#include <wx/minifram.h>
#include <wx/notebook.h>
#include <wx/print.h>
#include <wx/printdlg.h>
#include <wx/process.h>
#include <wx/progdlg.h>
#include <wx/sashwin.h>
#include <wx/spinbutt.h>
#include <wx/spinctrl.h>
#include <wx/splash.h>
#include <wx/splitter.h>
#include <wx/statline.h>
#include <wx/stream.h>
#include <wx/sysopt.h>
#include <wx/taskbar.h>
#include <wx/tglbtn.h>
#include <wx/tipwin.h>
#include <wx/tooltip.h>
//#include <wx/vlbox.h>
//#include <wx/vscroll.h>
#endif

typedef unsigned char byte;
typedef wxPoint2DDouble wxPoint2D;    
//---------------------------------------------------------------------------

// A macro that will help to execute simple statments wrapped in
// StartBlock/EndBlockThreads calls
#define wxPyBLOCK_THREADS(stmt) \
    { bool blocked = wxPyBeginBlockThreads(); stmt; wxPyEndBlockThreads(blocked); }

// Raise the NotImplementedError exception  (blocking threads)
#define wxPyRaiseNotImplemented() \
    wxPyBLOCK_THREADS(PyErr_SetNone(PyExc_NotImplementedError))

// Raise any exception witha string value  (blocking threads)
#define wxPyErr_SetString(err, str) \
    wxPyBLOCK_THREADS(PyErr_SetString(err, str))


//---------------------------------------------------------------------------

#if PYTHON_API_VERSION < 1009
#define PySequence_Fast_GET_ITEM(o, i) \
     (PyList_Check(o) ? PyList_GET_ITEM(o, i) : PyTuple_GET_ITEM(o, i))
#endif

#define RETURN_NONE()                 { Py_INCREF(Py_None); return Py_None; }
#define DECLARE_DEF_STRING(name)      static const wxString wxPy##name(wx##name)
#define DECLARE_DEF_STRING2(name,val) static const wxString wxPy##name(val)

//---------------------------------------------------------------------------

#ifndef wxPyUSE_EXPORTED_API

class wxPyCallback : public wxObject {
    DECLARE_ABSTRACT_CLASS(wxPyCallback);
public:
    wxPyCallback(PyObject* func);
    wxPyCallback(const wxPyCallback& other);
    ~wxPyCallback();

    void EventThunker(wxEvent& event);

    PyObject*   m_func;
};

#endif // wxPyUSE_EXPORTED_API
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// These Event classes can be derived from in Python and passed through the
// event system without loosing anything.  They do this by keeping a reference
// to themselves and some special case handling in wxPyCallback::EventThunker.



class wxPyEvtSelfRef {
public:
    wxPyEvtSelfRef();
    ~wxPyEvtSelfRef();

    void SetSelf(PyObject* self, bool clone=false);
    PyObject* GetSelf() const;
    bool GetCloned() const { return m_cloned; }

protected:
    PyObject*   m_self;
    bool        m_cloned;
};


class wxPyEvent : public wxEvent, public wxPyEvtSelfRef {
    DECLARE_ABSTRACT_CLASS(wxPyEvent)
public:
    wxPyEvent(int winid=0, wxEventType commandType = wxEVT_NULL);
    wxPyEvent(const wxPyEvent& evt);
    ~wxPyEvent();

    virtual wxEvent* Clone() const { return new wxPyEvent(*this); }
};


class wxPyCommandEvent : public wxCommandEvent, public wxPyEvtSelfRef {
    DECLARE_ABSTRACT_CLASS(wxPyCommandEvent)
public:
    wxPyCommandEvent(wxEventType commandType = wxEVT_NULL, int id=0);
    wxPyCommandEvent(const wxPyCommandEvent& evt);
    ~wxPyCommandEvent();

    virtual wxEvent* Clone() const { return new wxPyCommandEvent(*this); }
};



//----------------------------------------------------------------------
// Forward decalre a few things used in the exported API
class wxPyClientData;
class wxPyUserData;
class wxPyOORClientData;
class wxPyCBInputStream;

void wxPyClientData_dtor(wxPyClientData* self);
void wxPyUserData_dtor(wxPyUserData* self);
void wxPyOORClientData_dtor(wxPyOORClientData* self);
wxPyCBInputStream* wxPyCBInputStream_create(PyObject *py, bool block);


//---------------------------------------------------------------------------
// Export a C API in a struct.  Other modules will be able to load this from
// the wx.core module and will then have safe access to these functions, even if
// in another shared library.

class wxPyCallbackHelper;
struct swig_type_info;
struct swig_const_info;

typedef double (*py_objasdbl_conv)(PyObject *obj);

// Make SunCC happy and make typedef's for these that are extern "C"
typedef swig_type_info* (*p_SWIG_Python_TypeRegister_t)(swig_type_info *);
typedef swig_type_info* (*p_SWIG_Python_TypeCheck_t)(char *c, swig_type_info *);
typedef void*           (*p_SWIG_Python_TypeCast_t)(swig_type_info *, void *);
typedef swig_type_info* (*p_SWIG_Python_TypeDynamicCast_t)(swig_type_info *, void **);
typedef const char*     (*p_SWIG_Python_TypeName_t)(const swig_type_info *);
typedef const char *    (*p_SWIG_Python_TypePrettyName_t)(const swig_type_info *);
typedef swig_type_info* (*p_SWIG_Python_TypeQuery_t)(const char *);
typedef void            (*p_SWIG_Python_TypeClientData_t)(swig_type_info *, void *);
typedef PyObject*       (*p_SWIG_Python_newvarlink_t)(void);
typedef void            (*p_SWIG_Python_addvarlink_t)(PyObject *, char *, PyObject *(*)(void), int (*)(PyObject *));
typedef int             (*p_SWIG_Python_ConvertPtr_t)(PyObject *, void **, swig_type_info *, int);
typedef int             (*p_SWIG_Python_ConvertPacked_t)(PyObject *, void *, int sz, swig_type_info *, int);
typedef char*           (*p_SWIG_Python_PackData_t)(char *c, void *, int);
typedef char*           (*p_SWIG_Python_UnpackData_t)(char *c, void *, int);
typedef PyObject*       (*p_SWIG_Python_NewPointerObj_t)(void *, swig_type_info *,int own);
typedef PyObject*       (*p_SWIG_Python_NewPackedObj_t)(void *, int sz, swig_type_info *);
typedef void            (*p_SWIG_Python_InstallConstants_t)(PyObject *d, swig_const_info constants[]);
typedef void*           (*p_SWIG_Python_MustGetPtr_t)(PyObject *, swig_type_info *, int, int);


struct wxPyCoreAPI {

    p_SWIG_Python_TypeRegister_t       p_SWIG_Python_TypeRegister;
    p_SWIG_Python_TypeCheck_t          p_SWIG_Python_TypeCheck;
    p_SWIG_Python_TypeCast_t           p_SWIG_Python_TypeCast;
    p_SWIG_Python_TypeDynamicCast_t    p_SWIG_Python_TypeDynamicCast;
    p_SWIG_Python_TypeName_t           p_SWIG_Python_TypeName;
    p_SWIG_Python_TypePrettyName_t     p_SWIG_Python_TypePrettyName; 
    p_SWIG_Python_TypeQuery_t          p_SWIG_Python_TypeQuery;
    p_SWIG_Python_TypeClientData_t     p_SWIG_Python_TypeClientData;
    p_SWIG_Python_newvarlink_t         p_SWIG_Python_newvarlink;
    p_SWIG_Python_addvarlink_t         p_SWIG_Python_addvarlink;
    p_SWIG_Python_ConvertPtr_t         p_SWIG_Python_ConvertPtr;
    p_SWIG_Python_ConvertPacked_t      p_SWIG_Python_ConvertPacked;
    p_SWIG_Python_PackData_t           p_SWIG_Python_PackData;
    p_SWIG_Python_UnpackData_t         p_SWIG_Python_UnpackData;
    p_SWIG_Python_NewPointerObj_t      p_SWIG_Python_NewPointerObj;
    p_SWIG_Python_NewPackedObj_t       p_SWIG_Python_NewPackedObj;
    p_SWIG_Python_InstallConstants_t   p_SWIG_Python_InstallConstants;
    p_SWIG_Python_MustGetPtr_t         p_SWIG_Python_MustGetPtr;
    
    bool                (*p_wxPyCheckSwigType)(const wxChar* className);
    PyObject*           (*p_wxPyConstructObject)(void* ptr, const wxChar* className, int setThisOwn);
    bool                (*p_wxPyConvertSwigPtr)(PyObject* obj, void **ptr, const wxChar* className);
    PyObject*           (*p_wxPyMakeSwigPtr)(void* ptr, const wxChar* className);
        
    PyThreadState*      (*p_wxPyBeginAllowThreads)();
    void                (*p_wxPyEndAllowThreads)(PyThreadState* state);
    bool                (*p_wxPyBeginBlockThreads)();
    void                (*p_wxPyEndBlockThreads)(bool blocked);

    PyObject*           (*p_wxPy_ConvertList)(wxListBase* list);

    wxString*           (*p_wxString_in_helper)(PyObject* source);
    wxString            (*p_Py2wxString)(PyObject* source);
    PyObject*           (*p_wx2PyString)(const wxString& src);

    byte*               (*p_byte_LIST_helper)(PyObject* source);
    int*                (*p_int_LIST_helper)(PyObject* source);
    long*               (*p_long_LIST_helper)(PyObject* source);
    char**              (*p_string_LIST_helper)(PyObject* source);
    wxPoint*            (*p_wxPoint_LIST_helper)(PyObject* source, int* npoints);
    wxBitmap**          (*p_wxBitmap_LIST_helper)(PyObject* source);
    wxString*           (*p_wxString_LIST_helper)(PyObject* source);
    wxAcceleratorEntry* (*p_wxAcceleratorEntry_LIST_helper)(PyObject* source);

    bool                (*p_wxSize_helper)(PyObject* source, wxSize** obj);
    bool                (*p_wxPoint_helper)(PyObject* source, wxPoint** obj);
    bool                (*p_wxRealPoint_helper)(PyObject* source, wxRealPoint** obj);
    bool                (*p_wxRect_helper)(PyObject* source, wxRect** obj);
    bool                (*p_wxColour_helper)(PyObject* source, wxColour** obj);
    bool                (*p_wxPoint2D_helper)(PyObject* source, wxPoint2DDouble** obj);

    
    bool                (*p_wxPySimple_typecheck)(PyObject* source, const wxChar* classname, int seqLen);
    bool                (*p_wxColour_typecheck)(PyObject* source);

    void                (*p_wxPyCBH_setCallbackInfo)(wxPyCallbackHelper& cbh, PyObject* self, PyObject* klass, int incref);
    bool                (*p_wxPyCBH_findCallback)(const wxPyCallbackHelper& cbh, const char* name);
    int                 (*p_wxPyCBH_callCallback)(const wxPyCallbackHelper& cbh, PyObject* argTuple);
    PyObject*           (*p_wxPyCBH_callCallbackObj)(const wxPyCallbackHelper& cbh, PyObject* argTuple);
    void                (*p_wxPyCBH_delete)(wxPyCallbackHelper* cbh);

    PyObject*           (*p_wxPyMake_wxObject)(wxObject* source, bool setThisOwn, bool checkEvtHandler);
    PyObject*           (*p_wxPyMake_wxSizer)(wxSizer* source, bool setThisOwn);
    void                (*p_wxPyPtrTypeMap_Add)(const char* commonName, const char* ptrName);
    bool                (*p_wxPy2int_seq_helper)(PyObject* source, int* i1, int* i2);
    bool                (*p_wxPy4int_seq_helper)(PyObject* source, int* i1, int* i2, int* i3, int* i4);
    PyObject*           (*p_wxArrayString2PyList_helper)(const wxArrayString& arr);
    PyObject*           (*p_wxArrayInt2PyList_helper)(const wxArrayInt& arr);

    void                (*p_wxPyClientData_dtor)(wxPyClientData*);
    void                (*p_wxPyUserData_dtor)(wxPyUserData*);
    void                (*p_wxPyOORClientData_dtor)(wxPyOORClientData*);

    wxPyCBInputStream*  (*p_wxPyCBInputStream_create)(PyObject *py, bool block);

    bool                (*p_wxPyInstance_Check)(PyObject* obj);
    bool                (*p_wxPySwigInstance_Check)(PyObject* obj);

    bool                (*p_wxPyCheckForApp)();

};


#ifdef wxPyUSE_EXPORTED_API
// Notice that this is static, not extern.  This is by design, each module
// needs one, but doesn't have to use it.
static wxPyCoreAPI* wxPyCoreAPIPtr = NULL;
inline wxPyCoreAPI* wxPyGetCoreAPIPtr();
#endif // wxPyUSE_EXPORTED_API

#endif
