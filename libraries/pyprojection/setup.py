
import os, os.path
from distutils.core import setup, Extension

PROJ4_PREFIX = "/"
PROJ4_INCLUDE = os.path.join(PROJ4_PREFIX, "usr/local/include")
PROJ4_LIB = os.path.join(PROJ4_PREFIX, "usr/local/lib")

extensions = [Extension("Projectionc",
                        ["Projection_wrap.c"],
                        include_dirs = [PROJ4_INCLUDE],
                        library_dirs = [PROJ4_LIB],
                        libraries = ["proj"])]

setup(name = "py-Projection",
      version = "0.1",
      description = "Python bindings for PROJ.4",
      author = "Douglas K. Rand",
      author_email = "rand@meridian-enviro.com",
      py_modules = ["Projection"],
      ext_modules = extensions)

