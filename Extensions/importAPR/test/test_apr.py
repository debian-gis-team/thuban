# Copyright (c) 2003 by Intevation GmbH
# Authors:
# Jan-Oliver Wagner <jan@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Tests for APR classes.
"""

__version__ = "$Revision: 1732 $"
# $Source$
# $Id: test_apr.py 1732 2003-09-22 12:22:16Z jan $

import unittest

from Thuban.Model.color import Color, Transparent
from Thuban.Model.range import Range

from Extensions.importAPR.apr import APR_LClass, APR_TClr
from Extensions.importAPR.importAPR import ODB

class aprTest(unittest.TestCase):

    def test_TClr(self):
        """Test the APR_TClr class."""
        eq = self.assertEquals

        # list with tupels (TClr object, Color object to test against)
        lst = []

        # a standard RGB TClr object (here: white)
        obj = APR_TClr(None, 'TClr', 1)
        setattr(obj, 'Red', '0xffff')
        setattr(obj, 'Green', '0xffff')
        setattr(obj, 'Blue', '0xffff')
        lst.append( (obj, Color(1,1,1)) )

        # an empty TClr object
        obj = APR_TClr(None, 'TClr', 2)
        lst.append( (obj, Color(0,0,0)) )

        # an incomplete RGB TClr object (here: only red)
        obj = APR_TClr(None, 'TClr', 3)
        setattr(obj, 'Red', '0xffff')
        lst.append( (obj, Color(1,0,0)) )

        # an incomplete RGB TClr object (here: no full 4 digits of hex-code)
        obj = APR_TClr(None, 'TClr', 4)
        setattr(obj, 'Green', '0x0f')
        obj2 = APR_TClr(None, 'TClr', 5)
        setattr(obj2, 'Green', '0x0f00')
        lst.append( (obj, obj2.GetThubanColor()) )

        # a transparent TClr object
        obj = APR_TClr(None, 'TClr', 6)
        setattr(obj, 'Name', 'Transparent')
        lst.append( (obj, Transparent) )

        # finally test all the colors
        for tclr, color in lst:
            eq(tclr.GetThubanColor(), color)

    def test_LClass(self):
        """Test the APR_LClass class."""
        eq = self.assertEquals

        # list with tupels (TClr object, Color object to test against)
        lst = []

        # a standard LClass object with numerical range
        obj = APR_LClass(None, 'LClass', 1)
        setattr(obj, 'MinNum', '4.00000000000000')
        setattr(obj, 'MaxNum', '8.00000000000000')
        setattr(obj, 'Precision', '-3')
        setattr(obj, 'Label', '3')
        eq(obj.GetLabel(), '3') # label OK?
        eq(obj.GetThubanRange().__str__(), Range(']4;8]').__str__()) # range OK?

        # a LClass object with half of the numerical range
        obj = APR_LClass(None, 'LClass', 1)
        setattr(obj, 'MaxNum', '8.00000000000000')
        setattr(obj, 'Precision', '-3')
        setattr(obj, 'Label', '3')
        eq(obj.GetLabel(), '3') # label OK?
        eq(obj.GetThubanRange(), Range(']-oo;8]')) # range OK?

        # a LClass object with a string range describing a single value
        obj = APR_LClass(None, 'LClass', 1)
        setattr(obj, 'IsText', '1')
        setattr(obj, 'MinStr', 'BUILDING')
        setattr(obj, 'MaxStr', 'BUILDING')
        setattr(obj, 'Precision', '-3')
        setattr(obj, 'Label', 'BUILDING')
        eq(obj.GetLabel(), 'BUILDING') # label OK?
        eq(obj.GetThubanRange(), 'BUILDING') # range OK?

        # a LClass text object without minstr/maxstr/label
        obj = APR_LClass(None, 'LClass', 1)
        setattr(obj, 'IsText', '1')
        setattr(obj, 'Precision', '-3')
        eq(obj.GetLabel(), '') # label OK?
        eq(obj.GetThubanRange(), '') # range OK?


if __name__ == "__main__":
    unittest.main()
