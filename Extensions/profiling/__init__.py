# Copyright (c) 2003, 2004 by Intevation GmbH
# Authors:
# Jan-Oliver Wagner <jan@intevation.de> (2003, 2004)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

# import the actual module
import profiling

# perform the registration of the extension
from Thuban import _
from Thuban.UI.extensionregistry import ExtensionDesc, ext_registry

ext_registry.add(ExtensionDesc(
    name = 'profiling',
    version = '1.0.0',
    authors= [ 'Bernhard Herzog' ],
    copyright = '2003, 2004 Intevation GmbH',
    desc = _("Provide a profiler and a timer\n"
             "for screen rendering.")))

