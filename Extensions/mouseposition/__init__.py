# Copyright (C) 2005 by Intevation GmbH
# Authors:
# Frank Koormann <frank@intevation.de> (2005)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

# import the actual module
import mouseposition

# perform the registration of the extension
from Thuban import _
from Thuban.UI.extensionregistry import ExtensionDesc, ext_registry

ext_registry.add(ExtensionDesc(
    name = 'mouseposition',
    version = '1.0.0',
    authors= [ 'Frank Koormann' ],
    copyright = '2005 Intevation GmbH',
    desc = _("On mouse click display the current coordinates\n" \
             "in a dialog for easy further processing.")))
