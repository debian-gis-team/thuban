# Copyright (C) 2005 by Intevation GmbH
# Authors:
# Frank Koormann <frank@intevation.de> (2005)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
xtend thuban with a locator tool.

Collect positions of mouse clicks (in map coordinates) in a text control. 

The tool was implemented in the need to collect some coordinates for some
work (outside Thuban). The status bar display of the coordinates is quite 
transient (each mouse movement changes it) and cannot be copied. The tool let 
one simply collect the coordinates needed and copy them in one block later.
"""

__version__ = '$Revision: 2721 $'
# $Source$
# $Id: mouseposition.py 2721 2007-01-13 15:11:42Z dpinte $

import os, sys
import string

import wx
from wx.lib.layoutf import Layoutf

from Thuban.UI.common import ThubanBeginBusyCursor, ThubanEndBusyCursor
from Thuban.UI.command import registry, ToolCommand
from Thuban.UI.mainwindow import main_menu, main_toolbar, \
                                 make_check_current_tool
from Thuban.UI.viewport import Tool
from Thuban.UI.dialogs import NonModalDialog 
from Thuban import _

import Thuban

class DynamicMessageDialog(NonModalDialog):
    """Similar to the wx.ScrolledMessageDialog, contents dynamically 
       changeable by calling applications.

    """
    def __init__(self, parent, msg, name, caption, pos = wx.DefaultPosition):
        NonModalDialog.__init__(self, parent, name, caption)
        x, y = pos
        if x == -1 and y == -1:
            self.CenterOnScreen(wx.BOTH)
        text = wx.TextCtrl(self, -1, msg, wx.DefaultPosition,
                             wx.DefaultSize,
                             wx.TE_MULTILINE | wx.TE_READONLY)
        ok = wx.Button(self, wx.ID_OK, "OK")
        text.SetConstraints(Layoutf('t=t5#1;b=t5#2;l=l5#1;r=r5#1', (self,ok)))
        ok.SetConstraints(Layoutf('b=b5#1;x%w50#1;w!80;h!25', (self,)))
        wx.EVT_BUTTON(self, wx.ID_OK, self.OnClose)
        self.text = text
        self.SetAutoLayout(1)
        self.Layout()

    def getText(self):
        return self.text.GetValue()

    def setText(self, text):
        self.text.SetValue(text)

    def appendText(self, text):
        self.text.AppendText(text)

class MousePositionTool(Tool):

    def __init__(self, view, context):
        Tool.__init__(self, view)
        self.context = context
        self.dlg = None

    def Name(self):
        return "MousePositionTool"

    def MouseDown(self, event):
        map_proj = self.view.Map().GetProjection()
        pos = self.view.CurrentPosition()
        if pos is not None:
            pMsg = "%10.10g, %10.10g\n" % pos
            name = "extension_mouse_position"

            dialog = self.context.mainwindow.get_open_dialog(name)
            if dialog is None:
                dialog = DynamicMessageDialog(self.context.mainwindow, 
                                    pMsg, name, _("Mouse Position Tool"))
                self.context.mainwindow.add_dialog(name, dialog)
                dialog.Show(True)
            else:
                dialog.appendText(pMsg)
                dialog.Raise()
                          
def mouse_position_tool(context):
    canvas = context.mainwindow.canvas
    canvas.SelectTool(MousePositionTool(canvas, context))


# locator executed as an tool/extension to Thuban
iconfile = os.path.join(os.path.abspath(Thuban.__path__[0]),
                        "..", "Resources", "Bitmaps", "identify")
iconfile = os.path.join(os.path.abspath(os.path.dirname(__file__)), 
                        'position')
registry.Add(ToolCommand("mouse_position_tool", "Mouse Position Tool",
                 mouse_position_tool, icon = iconfile,
                 helptext = "Collect mouse click coordinates in a dialog",
                 checked = make_check_current_tool("MousePositionTool")))

# Add the command to the toolbar
main_toolbar.InsertSeparator()
main_toolbar.InsertItem("mouse_position_tool")

# find the extensions menu (create it anew if not found)
extensions_menu = main_menu.FindOrInsertMenu('extensions', _('E&xtensions'))

# finally add the new entry to the extensions menu
extensions_menu.InsertItem('mouse_position_tool')
