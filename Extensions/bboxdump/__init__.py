# Copyright (C) 2004 by Intevation GmbH
# Authors:
# Frank Koormann <frank@intevation.de> (2004)
# Jan-Oliver Wagner <jan@intevation.de> (2004)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

# import the actual module
import bboxdump

# perform the registration of the extension
from Thuban import _
from Thuban.UI.extensionregistry import ExtensionDesc, ext_registry

ext_registry.add(ExtensionDesc(
    name = 'bboxdump',
    version = '1.0.0',
    authors= [ 'Frank Koormann' ],
    copyright = '2004 Intevation GmbH',
    desc = _("Dumps the bounding boxes of all\n" \
             "shapes of the selected layer.")))
