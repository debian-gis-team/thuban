# Copyright (c) 2003, 2004 by Intevation GmbH
# Authors:
# Jan-Oliver Wagner <jan@intevation.de> (2003, 2004)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Test gns2shp extension.
"""

__version__ = "$Revision: 2845 $"
# $Source$
# $Id: test_gns2shp.py 2845 2008-06-20 05:20:03Z elachuni $

import unittest
import os
import sys

# If run directly as a script, add Thuban's test directory, the Lib
# directory and the Thuban directory itself to the path.
# Otherwise we assume that the importing code as already done it
if __name__ == "__main__":
    sys.path.append(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])),
                                 "..", "..", "..", "test"))
    sys.path.append(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])),
                                 "..", "..", "..", "Lib"))
    sys.path.append(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])),
                                 "..", "..", ".."))

import support
import shapelib
from dbflib import DBFFile, FTString, FTInteger, FTDouble
from Extensions.gns2shp.gns2shp import gns2shp

class gns2shpTest(unittest.TestCase, support.FileTestMixin):

    def test(self):
        """Test for correct creation of Shapefile from GNS text file format"""
        eq = self.assertEquals

        # create a temporary gns file (use liechtenstein as reference)
        filename = self.temp_file_name('test.gns')
        os.system('cp ls.txt %s' % filename)

        # convert the reference gns file to  shapefile
        dest_filename = self.temp_file_name('test')
        n = gns2shp(filename, dest_filename)

        # is the number of shapes correct?
        eq(n, 109) # what gns2shp reports
        # and now the actually written ones
        shp = shapelib.ShapeFile(dest_filename)
        numshapes, shapetype, mins, maxs = shp.info()
        eq(numshapes, n)

        # correct shapefile type?
        eq(shapetype, shapelib.SHPT_POINT)

        # attribute data correct?
        field_types = { 'RC': FTInteger,
                        'UFI': FTInteger,
                        'UNI': FTInteger,
                        'MGRS': FTString,
                        'JOG': FTString,
                        'FC': FTString,
                        'DSG': FTString,
                        'PC': FTInteger,
                        'CC1': FTString,
                        'ADM1': FTString,
                        'ADM2': FTString,
                        'POP': FTInteger,
                        'ELEV': FTInteger,
                        'CC2': FTString,
                        'NT': FTString,
                        'LC': FTString,
                        'SHORT_FORM': FTString,
                        'GENERIC': FTString,
                        'SORT_NAME': FTString,
                        'FULL_NAME': FTString,
                        'FULL_ND': FTString,
                        'MODIFY_DAT': FTString}
        dbf = DBFFile(dest_filename)
        eq(dbf.record_count(), n) # correct number of data sets?
        eq(dbf.field_count(), len(field_types)) # correct number of fields?
        for i in range(dbf.field_count()):
            ftype, name, width, prec = dbf.field_info(i)
            eq(ftype, field_types[name]) # field of correct type?

if __name__ == "__main__":
    unittest.main()
