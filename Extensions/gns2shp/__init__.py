# Copyright (c) 2003-2005 by Intevation GmbH
# Authors:
# Jan-Oliver Wagner <jan@intevation.de> (2003-2005)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

# perform the registration of the extension
from Thuban import _
from Thuban.UI.extensionregistry import ExtensionDesc, ext_registry

def init():
    """Initialize gns2shp extension module."""
    import gns2shp
    return None

ext_registry.add(ExtensionDesc(
    name = 'gns2shp',
    version = '1.0.0',
    authors= [ 'Jan-Oliver Wagner' ],
    copyright = '2003, 2004 Intevation GmbH',
    desc = _("Converts GNS (Geographical Name Service\n" \
             "of NIMA) to Shapefile format and\n" \
             "displays the data."),
    init_callback = init))
