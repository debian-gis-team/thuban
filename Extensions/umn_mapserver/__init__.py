# -*- coding:iso-8859-15 -*-
# Copyright (C) 2004 by Intevation GmbH
# Authors:
# Jan Sch�ngel <jschuengel@intevation.de> (2004)
# Jan-Oliver Wagner <jan@intevation.de> (2004)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

# first try out whether we can import the required module
# of UMN MapServer MapScript.
ok = True
try:
    import mapscript
except:
    print "Problems with UMN MapServer MapScript (not installed?)"
    ok = False

if ok:
    import mf_import
    import mf_export
    import mf_handle

    from Thuban.UI.extensionregistry import ExtensionDesc, ext_registry
    from Thuban import _, internal_from_unicode

    ext_registry.add(ExtensionDesc(
        name = 'UMN MapServer Management',
        version = '1.0.0',
        authors= [ internal_from_unicode(u'Jan Sch\xfcngel') ],
        copyright = '2004 Intevation GmbH',
        desc = _("Provide management methods for UMN MapServer\n" \
                 ".map-files. These can be created/imported/modified.")))
