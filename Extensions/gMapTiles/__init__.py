# Copyright (c) 2008 by Intevation GmbH
# Authors:
# Anthony Lenton <anthony@except.com.ar>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

# import the actual module
import exportGMapTiles

# perform the registration of the extension
from Thuban import _
from Thuban.UI.extensionregistry import ExtensionDesc, ext_registry

ext_registry.add(ExtensionDesc(
    name = 'exportGMapTiles',
    version = '0.1.1',
    authors= [ 'Anthony Lenton' ],
    copyright = '2008 by Intevation GmbH',
    desc = _("Export PNG tiles to be loaded with Google Maps\n" \
             "as a GTileLayerOverlay.")))

