This extension generates 256x256px png tiles out of Thuban maps, to be able to
overlay the data on Google Maps.

The extension reprojects and chops the tiles accordingly so that the data
matches Google's imagery.

For now the extension only generates the tiles, you'll need to write your
own javascript to use them.  Check the "sample/" subdirectory for a very simple
way of viewing the data you generate.

