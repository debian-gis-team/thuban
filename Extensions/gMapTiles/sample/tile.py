#!/usr/bin/python
import cgi
import cgitb
cgitb.enable()
import cStringIO

PATH_TO_TILES = "/path/to/tiles"

size = 256 #image width and height

def getTile(x, y, z):
    z = int(z)
    y = int(y)
    x = int(x)
    
    try:
        f = open('%s/%d/%d.%d.png' % (PATH_TO_TILES, z, x, y))
    except IOError:
        print "Content-type: text/plain\n\nNothing"
        return

    print "Content-type: image/png\n"
    print f.read()

if __name__ == "__main__":
    form = cgi.FieldStorage()
    if "x" in form and "y" in form and "z" in form:
        getTile(form["x"].value, form["y"].value, form["z"].value)
    else:
        print "Content-type: text/html\n"
        print """<html><body>Missing input arguments</body></html>"""

