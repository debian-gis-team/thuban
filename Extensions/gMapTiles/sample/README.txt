This is a sample application of the gMapTiles extension.  To test this sample
you need to:

 * Use thuban to generate tiles for some data.

 * Make tile.py accessible via web on your web server, and make sure that it has
   execution permissions.  Let's say that the URL to tile.py is now
   http://yourserver/tile.py.  If you open this URL in a web browser you should
   see the message "Missing input arguments"

 * Edit tile.py and replace the PATH_TO_TILES string constant for the absolute
   path where you saved the tiles.

 * Make sample.html accessible via web on your web server.  Edit sample.html
   to use your own Google Maps API Key, and replace the URL_TO_TILE_PY string
   constant with tile.py's absolute URL. (make sure your key is valid for the
   sample's URL, more info, http://code.google.com/apis/maps/documentation/ )

Enjoy!

