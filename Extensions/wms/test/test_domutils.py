# Copyright (c) 2004 by Intevation GmbH
# Authors:
# Martin Schulze <joey@infodrom.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Test for the domutils module

"""

__version__ = "$Revision: 2793 $"
# $Source$
# $Id: test_domutils.py 2793 2007-12-08 23:55:23Z bernhard $

from sys import path
import unittest
import xml.dom.minidom

from adjustpath import thubandir
path.insert(0, thubandir + "/test")

import support
support.initthuban()


from Extensions.wms.domutils import getElementByName, getElementsByName


class TestDOMutils(unittest.TestCase):
    """
    Defines a test environment for the class WMSCapabilities.
    """

    root = None

    def setUp(self):
        """
        Set up the XML test string, parse it into a DOM element and
        hook it to the internal root variable.
        """
        data = "\n".join(['<?xml version="1.0" encoding="ISO-8859-1" standalone="no" ?>',
                          '<!DOCTYPE WMT_MS_Capabilities SYSTEM "http://www.digitalearth.gov/wmt/xml/capabilities_1_0_8.dtd">',
                          '<WMT_MS_Capabilities version="1.0.8" updateSequence="0">',
                          '  <Layer>',
                          '    <Name>Osnabrueck</Name>',
                          '    <Layer>',
                          '      <Name>gewaesser</Name>',
                          '      <Layer queryable="0" opaque="0" cascaded="0">',
                          '        <Name>gewaesserpolyl</Name>',
                          '      </Layer>',
                          '    </Layer>',
                          '    <Layer queryable="0" opaque="0" cascaded="0">',
                          '      <Name>gruenflaechen</Name>',
                          '    </Layer>',
                          '  </Layer>',
                          '</WMT_MS_Capabilities>'])
        self.root = xml.dom.minidom.parseString(data).documentElement


    def compareLists(self, foo, bar):
        """
        Compare two lists
        - check same number of elements
        - check whether all elements in the first list are part of the second
        """

        # Check for same number of elements
        if len(foo) != len(bar):
            self.fail("Different number of elements");

        # Loop through all elements for existance
        for elm in foo:
            if elm not in bar:
                self.fail("%s not in second list" % elm);


    def compareNodeLists (self, result, nodelist):
        """
        Compares the Name children versus the given result list.
        """

        names = []
        for pivot in nodelist:
            for i in range (pivot.childNodes.length):
                if pivot.childNodes[i].nodeName == 'Name':
                    names.append(pivot.childNodes[i].childNodes[0].data)

        self.compareLists(result, names)


    def compareNode (self, result, node):
        """
        Compares the Name child versus the given result.
        """

        for i in range (node.childNodes.length):
            if node.childNodes[i].nodeName == 'Name':
                self.assertEquals(result, node.childNodes[i].childNodes[0].data)
                break
        else:
            self.fail("No Name child found.");


    def test_compareLists (self):
        """
        Test the internal compareLists function.
        """

        # Zero element
        self.compareLists([], [])

        # Single element
        self.compareLists(['alpha'], ['alpha'])

        # Multiple elements
        self.compareLists(['alpha', 'beta', 'gamma'], ['alpha', 'beta', 'gamma'])

        # Multiple elements, different order
        self.compareLists(['alpha', 'beta', 'gamma'], ['gamma', 'alpha', 'beta'])


    def test_getElementsByName (self):
        """
        Test for the getElementsByName function.
        """

        # Test for level 1
        result = ['Osnabrueck']
        self.compareNodeLists(result, getElementsByName(self.root, 'Layer'))

        # Test for level 2
        result = ['gruenflaechen', 'gewaesser']
        self.compareNodeLists(result, getElementsByName(getElementsByName(self.root, 'Layer')[0], 'Layer'))

        # Test for level 3
        result = ['gewaesserpolyl']
        self.compareNodeLists(result,
                              getElementsByName
                              (getElementsByName
                               (getElementsByName
                                (self.root, 'Layer')[0], 'Layer')[0], 'Layer'))


    def test_getElementByName (self):
        """
        Test for the getElementByName function.
        """

        # Test for level 1
        self.compareNode('Osnabrueck', getElementByName(self.root, 'Layer'))

        # Test for level 2
        self.compareNode('gewaesser', getElementByName(getElementByName(self.root, 'Layer'), 'Layer'))

        # Test for level 3
        self.compareNode('gewaesserpolyl',
                         getElementByName
                         (getElementByName
                          (getElementByName
                           (self.root, 'Layer'), 'Layer'), 'Layer'))


if __name__ == "__main__":
    unittest.main()
