# Copyright (c) 2004 by Intevation GmbH
# Authors:
# Martin Schulze <joey@infodrom.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Add the main Thuban directory to the path, so that Thuban modules can
be added without problems.

Exported variables:

thubandir -- absolute path to the main Thuban directory

"""

__version__ = "$Revision: 2117 $"
# $Source$
# $Id: adjustpath.py 2117 2004-03-19 17:00:17Z joey $

import os
from sys import path

if os.path.dirname(__file__) == "" or os.path.dirname(__file__) == ".":
    thubandir = os.path.abspath("../../..")
else:
    thubandir = os.path.abspath(os.path.dirname(__file__) + "/../../..")
path.insert(0, thubandir)
