# Copyright (c) 2003, 2004, 2008 by Intevation GmbH
# Authors:
# Jan-Oliver Wagner <jan@intevation.de> (2003, 2004)
# Didrik Pinte <dpinte@dipole-consulting.com> (2008)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

# first try out whether we can import the required module OWSLib
ok = True
try:
    import owslib
except:
    print "Problems with OWSLib (not installed?)"
    ok = False

if ok:
    import wms

    from Thuban.UI.extensionregistry import ExtensionDesc, ext_registry
    from Thuban import _

    ext_registry.add(ExtensionDesc(
        name = 'WMS',
        version = '0.3.0',
        authors= [ 'Didrik Pinte', 'Jan-Oliver Wagner', 'Martin Schulze' ],
        copyright = '2003, 2004, 2008 Intevation GmbH',
        desc = _("Provide layers via OGC WMS.")))
