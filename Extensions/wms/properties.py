# Copyright (c) 2004, 2007 by Intevation GmbH
# Authors:
# Martin Schulze <joey@infodrom.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Edit Layer Properties

class wmsProperties(NonModalNonParentDialog):
    __init__

    dialog_layout(text)

    OnOK(event)
    OnCancel(event)

Problems:

    1. The TextCtrl should fit to the right side of the horizontal
       box/sizer.  It doesn't.  I only found the sizer.Add() method to
       be able to take proportions but no way to tell the object to
       fill all remaining space.

    2. The framed box "Layers" needs to be scrollable if there are
       more than 12 items, since the dialog would probably not fit on
       people's screen anymore.

       Todo: This can be solved (in C it was possible at least),
       probably with ScrolledWindow

    3. The button hbox is not aligned to the right side which should
       be.  For some reason ALIGN_RIGHT does not have any effect.
       Maybe I just misunderstood the documentation?

"""

__version__ = "$Revision: 2785 $"
# $Source$
# $Id: properties.py 2785 2007-11-27 23:39:49Z bernhard $

from Thuban import _
from Thuban.UI.dialogs import NonModalNonParentDialog

import wx
#    wx.BoxSizer, wx.VERTICAL, wx.HORIZONTAL, \
#    wx.Button, wx.ID_OK, wx.ID_CANCEL, wx.ALL, wx.ALIGN_CENTER_HORIZONTAL, \
#    EVT_BUTTON, wx.EXPAND, wx.StaticBoxSizer, wx.StaticBox, wx.ALIGN_RIGHT, \
#    wx.ALIGN_BOTTOM

ID_WMS_TITLE   = 5001
ID_WMS_LAYER   = 5002
ID_WMS_FORMATS = 5003

MAX_LAYERNAME_LENGTH = 45
MAX_VISIBLE_LAYERS   = 12

class wmsProperties(NonModalNonParentDialog):
    """
    Representation for the WMS properties dialog
    """

    def __init__(self, parent, name, layer, *args, **kw):
        """
        Build the properties dialog
        """
        title = _("Edit WMS Properties")
        NonModalNonParentDialog.__init__(self, parent, name, title)

        self.layer = layer

        # Hooks for the widgets to get user data
        self.entry = None
        self.layers = {}
        self.formats = None

        self.dialog_layout(layer)


    def dialog_layout(self, layer):
        """
        Set up the information dialog
        """

        # main box for the entire dialog
        mainbox = wx.BoxSizer(wx.HORIZONTAL)
        
        # vertical box to contain the three parts
        #     (title, layers, formats+buttons)
        vbox = wx.BoxSizer(wx.VERTICAL)
        mainbox.Add(vbox, 0, wx.ALL|wx.EXPAND, 4)
        
        # edit the title
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        vbox.Add(hbox, 0, wx.ALL, 0)
        label = wx.StaticText(self, ID_WMS_TITLE, _("Title:"))
        hbox.Add(label, 1, wx.ALL|wx.EXPAND, 4)
        self.entry = wx.TextCtrl(self, ID_WMS_TITLE, layer.Title())
        hbox.Add(self.entry, 7, wx.ALL|wx.EXPAND|wx.ALIGN_RIGHT, 0)

        layerbox = wx.StaticBox(self, ID_WMS_LAYER, _("Layers"))
        lbox = wx.StaticBoxSizer(layerbox, wx.VERTICAL)
        vbox.Add(lbox, 0, wx.ALL, 0)
        visible = layer.getVisibleLayers()
        for l in layer.getLayers():
            checker = wx.CheckBox(self, ID_WMS_LAYER, layer.getLayerTitle(l)[0:MAX_LAYERNAME_LENGTH])
            self.layers[l] = checker
            if l in visible:
                checker.SetValue(True)
            lbox.Add(checker, 0, wx.ALL|wx.EXPAND, 0)

        # tiled box:
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        vbox.Add(hbox, 0, wx.ALL|wx.EXPAND, 0)
        
        # left part: image format selection
        formatbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(formatbox, 1, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 0)
        label = wx.StaticText(self, ID_WMS_FORMATS, _("Format:"))
        formatbox.Add(label, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        self.formats = wx.Choice(self, ID_WMS_FORMATS)
        formatbox.Add(self.formats, 1, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 4)

        # fill the select box
        match = 0
        count = 0
        format = layer.getWMSFormat()
        for f in layer.getFormats():
            self.formats.Append(f)
            if f == format:
                match = count
            count += 1

        self.formats.Fit()
        self.formats.SetSelection(match)

        # Build the button hbox, to be added into row
        buttons = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(buttons, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 0)
        buttons.Add(wx.Button(self, wx.ID_OK, _("OK")), 0, wx.ALL, 4)
        buttons.Add(wx.Button(self, wx.ID_CANCEL, _("Cancel")), 0, wx.ALL, 4)
        wx.EVT_BUTTON(self, wx.ID_OK, self.OnOK)
        wx.EVT_BUTTON(self, wx.ID_CANCEL, self.OnCancel)

        self.SetAutoLayout(True)
        self.SetSizer(mainbox)
        mainbox.Fit(self)
        mainbox.SetSizeHints(self)


    def OnOK(self, event):
        """
        Handle the 'OK button pressed' event

        i.e. transfer user input into the layer
        """

        # Set the title
        self.layer.SetTitle(self.entry.GetValue())

        # Set the image format for WMS GetMap requests
        selection = self.formats.GetSelection()
        if selection > -1:
            self.layer.setWMSFormat(self.formats.GetString(self.formats.GetSelection()))

        # Set the list of visible layers
        visible = []
        for l in self.layers.keys():
            if self.layers[l].IsChecked():
                visible.append(l)
        self.layer.setVisibleLayers(visible)

        self.Close()


    def OnCancel(self, event):
        """
        Handle the 'Cancel button pressed' event
        """
        self.Close()
