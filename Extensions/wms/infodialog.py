# Copyright (c) 2004 by Intevation GmbH
# Authors:
# Martin Schulze <joey@infodrom.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Information dialog to display various information about a WMS layer.

class wmsInfoDialog(ThubanFrame):
    __init__

    dialog_layout(text)

    calcText(layer)
    
"""

__version__ = "$Revision: 2818 $"
# $Source$
# $Id: infodialog.py 2818 2008-01-27 23:41:08Z bernhard $

from Thuban import _
from Thuban.UI.dialogs import ThubanFrame

from wx import BoxSizer, TextCtrl, VERTICAL, \
    HORIZONTAL, TE_READONLY, TE_MULTILINE, TE_LINEWRAP, \
    EXPAND, ALL, Button, ALIGN_CENTER_HORIZONTAL, ID_OK, \
    EVT_BUTTON


class wmsInfoDialog(ThubanFrame):
    """
    Representation for a simple information dialog

    This dialog will display the title of the WMS resource
    """

    def __init__(self, parent, name, layer, *args, **kw):
        """
        Build the information dialog
        """
        title = _("WMS Information")
        ThubanFrame.__init__(self, parent, name, title)

        self.dialog_layout(self.calcText(layer))


    def dialog_layout(self, text):
        """
        Set up the information dialog
        """

        vbox = BoxSizer(VERTICAL)

        textBox = TextCtrl(self, -1, text,
                             style=TE_READONLY|TE_MULTILINE|TE_LINEWRAP)
        w, h = (500, 300)
        textBox.SetSizeHints(w, h)
        textBox.SetSize((w, h))
        
        vbox.Add(textBox, 1, EXPAND|ALL, 10)
        
        buttons = BoxSizer(HORIZONTAL)
        buttons.Add(Button(self, ID_OK, _("Close")), 0, ALL, 4)
        vbox.Add(buttons, 0, ALIGN_CENTER_HORIZONTAL|ALL, 10)
        
        EVT_BUTTON(self, ID_OK, self.OnClose)
        
        self.SetAutoLayout(True)
        self.SetSizer(vbox)
        vbox.Fit(self)
        vbox.SetSizeHints(self)


    def calcText(self, layer):
        """
        Generate the text to be displayed in the information window

        It will use several nodes returned by the GetCapabilities
        request, such as the title, the abstract, fees and access
        constraints, if they are documented.
        """

        text = ''

        foo = layer.capabilities.getTitle()
        if foo != "":
            text += foo + "\n\n"

        foo = layer.capabilities.getAbstract()
        if foo != "":
            text += foo + "\n\n"

        foo = layer.capabilities.getFees()
        if foo != "":
            text += _("Fees:") + "\n\n" + foo + "\n\n"

        foo = layer.capabilities.getAccessConstraints()
        if foo != "":
            text += _("Access Constraints:") + "\n\n" + foo + "\n\n"

        text += "URL: " + layer.url

        return text
