# Copyright (C) 2004, 2005, 2006, 20007 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de> (2004)
# Jan-Oliver Wagner <jan@intevation.de> (2004)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

# import the actual modules
from os import environ
from sys import platform
try:
    if platform != 'win32':
      dummy = environ["DISPLAY"]
    import svgsaver
    import maplegend
except:    
    pass # For non-win32 platform, we don't have a DISPLAY, so don't import 
         # the modules (for test mode)
         # For win32 platform, there is  always have a graphical mode
         # Not sure whether this is the best method to avoid problems
         # in the global test routine.

# perform the registration of the extension
from Thuban import _
from Thuban.UI.extensionregistry import ExtensionDesc, ext_registry

ext_registry.add(ExtensionDesc(
    name = 'SVG Export',
    version = '1.0.1',
    authors= [ 'Markus Rechtien', 'Bernhard Reiter' ],
    copyright = '2004-2007 Intevation GmbH',
    desc = _("Export the current map and legend in Thuban-map-SVG format.")))
