# Copyright (c) 2005 by Intevation GmbH
# Authors:
# Jan-Oliver Wagner <jan@intevation.de> (2005)
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

# perform the registration of the extension
from Thuban import _
from Thuban.UI.extensionregistry import ExtensionDesc, ext_registry

def init():
    """Initialize storeshape extension module."""
    import export_shapefile
    return None

ext_registry.add(ExtensionDesc(
    name = 'export_shapefile',
    version = '0.1.0',
    authors= [ 'Jan-Oliver Wagner' ],
    copyright = '2005 Intevation GmbH',
    desc = _("Exports the selected layer as a Shapefile."),
    init_callback = init))
