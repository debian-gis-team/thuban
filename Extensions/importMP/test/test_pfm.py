# Copyright (c) 2008 by Intevation GmbH
# Authors:
# Anthony Lenton <anthony@except.com.ar>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with Thuban for details.

"""
Tests for the PFM importer.
"""
import unittest
from StringIO import StringIO
from Extensions.importMP.pfm import MPParser, MPParseError
import snippets

class TestPFM (unittest.TestCase):
    def testInitialValue(self):
        """ Check that the parser has good initial values """
        parser = MPParser()
        self.assertEquals (parser.mapName, "")
        self.assertEquals (parser.parsedLines, 0)
        self.assertEquals (parser.parsedSections, 0)
        self.assertEquals (parser.pois, [])
        self.assertEquals (parser.polygons, [])
        self.assertEquals (parser.polylines, [])

    def testParsedLines(self):
        """ Check that comments and blank lines are skipped correctly """
        parser = MPParser()
        parser.parse (StringIO(snippets.justHeader))
        self.assertEquals (parser.parsedLines, 4)
        parser = MPParser()
        parser.parse (StringIO(snippets.headerWithComments))
        self.assertEquals (parser.parsedLines, 12)

    def testSections (self):
        """ Check that sections are matched up properly """
        parser = MPParser()
        parser.parse (StringIO(snippets.justHeader))
        self.assertEquals (parser.parsedSections, 1)
        parser = MPParser()
        mp = StringIO (snippets.nonsenseBounder)        
        self.assertRaises (MPParseError, parser.parse, mp)
        parser = MPParser()
        mp = StringIO (snippets.bounderMismatch)
        self.assertRaises (MPParseError, parser.parse, mp)
        parser = MPParser()
        mp = StringIO (snippets.doubleSectionClose)
        self.assertRaises (MPParseError, parser.parse, mp)

    def testCaseInsensitive (self):
        """ Check that no exception is raised if capitals are used"""
        parser = MPParser()
        parser.parse (StringIO (snippets.caseSensitivity1))
        parser = MPParser()
        parser.parse (StringIO (snippets.caseSensitivity2))
        parser = MPParser()
        parser.parse (StringIO (snippets.caseSensitivity3))

    def testIncompleteSection (self):
        """ Check that an exception is raised if the last section is
            left open """
        parser = MPParser()
        mp = StringIO(snippets.incompleteSection)
        self.assertRaises (MPParseError, parser.parse, mp)

    def testHeaderSection (self):
        """ Check that the header section is parsed """
        parser = MPParser ()
        mp = StringIO(snippets.nothingness)
        self.assertRaises (MPParseError, parser.parse, mp)
        parser = MPParser()
        mp = StringIO(snippets.twoHeaderSections)
        self.assertRaises (MPParseError, parser.parse, mp)
        parser = MPParser()
        parser.parse(StringIO(snippets.mapWithName))
        self.assertEquals (parser.mapName, "My Test Map")

    def testSample (self):
        parser = MPParser()
        parser.parse (StringIO(snippets.miniLobos))
        self.assertEquals(len(parser.pois), 4)
        self.assertEquals(len(parser.polylines), 2)
        self.assertEquals(len(parser.polygons), 1)

    def testPolylines (self):
        parser = MPParser()
        parser.parse (StringIO(snippets.RGN40s))
        self.assertEquals(len(parser.polylines), 4)
        parser = MPParser()
        parser.parse (StringIO(snippets.mixedRGN40sAndPolylines))
        self.assertEquals(len(parser.polylines), 4)
       
    def testPolygons (self):
        parser = MPParser()
        parser.parse (StringIO(snippets.RGN80s))
        self.assertEquals(len(parser.polygons), 4)
        parser = MPParser()
        parser.parse (StringIO(snippets.mixedRGN80sAndPolygons))
        self.assertEquals(len(parser.polygons), 4)
       
    def testPois (self):
        parser = MPParser()
        parser.parse (StringIO(snippets.RGN10s))
        self.assertEquals(len(parser.pois), 4)
        parser = MPParser()
        parser.parse (StringIO(snippets.RGN20s))
        self.assertEquals(len(parser.pois), 4)
        parser = MPParser()
        parser.parse (StringIO(snippets.mixedPois))
        self.assertEquals(len(parser.pois), 6)

    def testDontCrashWithWeirdTypes (self):
        parser = MPParser()
        parser.parse (StringIO(snippets.weirdTypes))
        self.assertEquals(len(parser.pois), 3)
        self.assertEquals(len(parser.polylines), 2)
        self.assertEquals(len(parser.polygons), 1)

    def testMissingTwoCharFormatWorks (self):
        """ Test that if the type 0xe works as well as 0x0e, for example"""
        parser = MPParser()
        parser.parse (StringIO(snippets.missingZeroType))
        self.assertEquals(len(parser.polygons), 2)
        self.assertEquals(parser.polygons[0]['parsedType'], "Airport Runway")
        self.assertEquals(parser.polygons[1]['parsedType'], "Airport Runway")

    def testTypesAreTheRightKind (self):
        """ Test that types from polylines and polygons come from the
            right list of types """
        p = MPParser()
        p.parse (StringIO(snippets.polyTypes))
        self.assertEquals(p.polygons[0]['parsedType'], "City")
        self.assertEquals(p.polylines[0]['parsedType'], "Major Highway-thick")

if __name__ == "__main__":
    unittest.main()

