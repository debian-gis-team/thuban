# This file contains snippets used by the testing functions.
# They should be in alphabetical order

bounderMismatch = """
[img 1]
[end-yonk]
"""

caseSensitivity1 = """
[IMG ID]
[END-IMG ID]
"""

caseSensitivity2 = """
[img ID]
[enD-IMG id]
"""

caseSensitivity3 = """
[ImG Id]
[EnD-imG iD]
"""

doubleSectionClose = """
[img id]
[end-img id]
[end-img id]
"""

headerWithComments = """[img id]
This=is a test MPFile
;I have no idea what it's about, But I shouldn't care
; I do know that this is a comment
; So it should be removed.
; And blank lines should be removed too.

=This isn't a comment.
T=his; isn't either
   ; let's say this is a comment
   ; to be a bit more robust
[end]
"""

incompleteSection = """
[img id]
"""

justHeader = """
[img id]
Dummy=I'm a fake MPFile
[end]
"""

mapWithName = """
[img id]
Name = My Test Map
[end]
"""

miniLobos = """
[IMG ID]
CodePage=850
LblCoding=9
ID=54010076
Name=Lobos
[END-IMG ID]

[Countries]
Country1=Argentina~[0x1d]ARG
[END-Countries]

[Regions]
Region1=Buenos Aires~[0x1d]BA
CountryIdx1=1
[END-Regions]

[Cities]
City1=Abbott
RegionIdx1=1
[END-Cities]

[ZipCodes]
ZipCode1=7200
[END-ZipCodes]

[Restrict]
Nod=155306
TraffPoints=155988,155306,155988
TraffRoads=8609,8609
Time=
[END-Restrict]

[DICTIONARY]
;           0        1         2         3         4   
;           1234567890123456789012345678901234567890123
Level1RGN40=1111111111110000000111011111111111111111111
Level2RGN40=1111111111110000000111011111111111111111111
Level3RGN40=1111100101110000000111011111111011011101100
Level4RGN40=1111000001110000000010001111111001001000000
Level5RGN40=1100000000000000000010001011110000000000000
Level6RGN40=1000000000000000000010001000010000000000000
[END-DICTIONARY]

[POI]
Type=0x2500
Label=Peaje Saladillo
EndLevel=6
StreetDesc=Ruta 205 Km. 167,00
Data0=(-35.51966,-59.67464)
[END]

[POI]
Type=0x4400
Label=ACA
EndLevel=6
Data0=(-35.70035,-58.88981)
[END]

[POI]
Type=0x5903
Label=Saladillo
EndLevel=6
Data0=(-35.60043,-59.81360)
[END]

[POI]
Type=0x6401
EndLevel=6
Data0=(-35.65026,-58.85069)
[END]

[POLYLINE]
Type=0xa
EndLevel=6
RoadID=1970
RouteParam=3,0,0,0,0,0,0,0,0,0,0,0
Data0=(-35.07797,-59.09923),(-35.08840,-59.11088)
Nod1=0,34258,0
Nod2=1,34259,0
[END]

[POLYLINE]
Type=0xa
EndLevel=6
RoadID=1973
RouteParam=3,0,0,0,0,0,0,0,0,0,0,0
Data0=(-35.24004,-59.48067),(-35.24277,-59.49114),(-35.24497,-59.49020)
Nod1=0,29389,0
Nod2=2,29386,0
[END]

[POLYGON]
Type=0x17
Label=Plaza Arturo Illia
EndLevel=6
Data0=(-35.76743,-58.49350),(-35.76776,-58.49378),(-35.76782,-58.49371)
[END]

"""

missingZeroType = """
[IMG ID]
[END]

[POLYGON]
Type=0x0e
Label=An Airport Runway
EndLevel=6
Data0=(-35.76743,-58.49350),(-35.76776,-58.49378),(-35.76782,-58.49371)
[END]

[POLYGON]
Type=0xe
Label=Another Airport Runway
EndLevel=6
Data0=(-35.76743,-58.49350),(-35.76776,-58.49378),(-35.76782,-58.49371)
[END]

"""

mixedPois = """
[IMG ID]
[END]

[RGN20]
Type=0xd
Label=BIALET MASSE
City=Y
CityIdx=17
Origin0=(-31.315284,-64.461891)
[END-RGN20]

[RGN20]
Type=0xc
Label=VALLE HERMOSO
City=Y
CityIdx=184
Origin0=(-31.117530,-64.482666)
[END-RGN20]

[RGN10]
Type=0x3002
Label=MATERNIDAD
CityIdx=44
Origin0=(-31.423515,-64.163887)
[END-RGN10]

[RGN10]
Type=0x2b01
Label=HOTEL
CityIdx=92
Origin0=(-31.092639,-64.477066)
[END-RGN10]

[POI]
Type=0x2f08
Label=Est. Salvador Maria
EndLevel=6
StreetDesc=F. C. G. R.
CityIdx=28
Data0=(-35.30298,-59.16738)
[END]

[POI]
Type=0xe00
Label=Salvador Maria
EndLevel=6
City=Y
CityIdx=28
Data0=(-35.29979,-59.16765)
[END]

"""

mixedRGN40sAndPolylines = """
[IMG ID]
[END]

[RGN40]
Type=0x18
Data0=(-31.172805,-64.999877),(-31.170059,-64.997474)
[END-RGN40]

[RGN40]
Type=0x18
Data0=(-31.124225,-64.999877),(-31.137958,-64.982543)
[END-RGN40]

[POLYLINE]
Type=0xa
EndLevel=6
RoadID=4532
RouteParam=3,0,0,0,0,0,0,0,0,0,0,0
Data0=(-35.39861,-59.34420),(-35.40269,-59.34945)
Nod1=0,29465,0
Nod2=1,29456,0
[END]

[POLYLINE]
Type=0xa
EndLevel=6
RoadID=4433
RouteParam=3,0,0,0,0,0,0,0,0,0,0,0
Data0=(-35.38428,-59.32179),(-35.37814,-59.31504)
Nod1=0,29646,0
Nod2=1,34580,0
[END]

"""

mixedRGN80sAndPolygons = """
[IMG ID]
[END]

[RGN80]
Type=0x41
Label=LA QUINTANA
Data0=(-31.839323,-64.431465),(-31.843271,-64.430091),(-31.848936,-64.435241)
[END-RGN80]

[RGN80]
Type=0x3c
Label=EMBALSE LOS MOLINOS
Data0=(-31.826664,-64.571586),(-31.826664,-64.572616),(-31.826320,-64.572273)
[END-RGN80]

[POLYGON]
Type=0x19
EndLevel=6
Data0=(-35.40248,-59.32782),(-35.40348,-59.32778),(-35.40357,-59.32937)
[END]

[POLYGON]
Type=0xb
EndLevel=6
Data0=(-35.40273,-59.32503),(-35.40338,-59.32611),(-35.40402,-59.32553)
[END]

"""

nonsenseBounder = """
[img id]
[yonk]
"""

nothingness = ""

polyTypes = """
[IMG ID]
[END]

[POLYLINE]
Type=0x1
Data0=(-35.38428,-59.32179),(-35.37814,-59.31504)
[END]

[POLYGON]
Type=0x1
Data0=(-35.40273,-59.32503),(-35.40338,-59.32611)
[END]

"""

RGN10s = """
[IMG ID]
[END]

[RGN10]
Type=0x2f04
Label=FALDA DEL CARMEN
CityIdx=6
Origin0=(-31.583763,-64.456230)
[END-RGN10]

[RGN10]
Type=0x2f03
Label=BERTA MOTORSPORT
CityIdx=6
Origin0=(-31.647449,-64.408508)
[END-RGN10]

[RGN10]
Type=0x2f01
Label=ESSO
CityIdx=6
Origin0=(-31.658950,-64.409706)
[END-RGN10]

[RGN10]
Type=0x2f01
Label=YPF
CityIdx=6
Origin0=(-31.658950,-64.409019)
[END-RGN10]
"""

RGN20s = """
[IMG ID]
[END]

[RGN20]
Type=0xc
Label=MINA CLAVERO
City=Y
CityIdx=122
Origin0=(-31.724071,-65.003494)
[END-RGN20]

[RGN20]
Type=0x6
Label=VILLA GENERAL BELGRANO
City=Y
CityIdx=200
Origin0=(-31.976524,-64.558883)
[END-RGN20]

[RGN20]
Type=0xb
Label=LA FALDA
City=Y
CityIdx=92
Origin0=(-31.099506,-64.477340)
[END-RGN20]

[RGN20]
Type=0xd
Label=VILLA CAEIRO
City=Y
CityIdx=190
Origin0=(-31.291595,-64.462577)
[END-RGN20]
"""

RGN40s = """
[IMG ID]
[END]

[RGN40]
Type=0x1f
Label=RIO LOS CONDORITOS
Data0=(-31.688133,-64.676124),(-31.686073,-64.674064),(-31.685730,-64.673210)
[END-RGN40]

[RGN40]
Type=0x18
Data0=(-31.463600,-64.859291),(-31.451927,-64.880577),(-31.446777,-64.897911)
[END-RGN40]

[RGN40]
Type=0x18
Data0=(-31.529861,-64.898086),(-31.530376,-64.900314),(-31.526428,-64.909240)
[END-RGN40]

[RGN40]
Type=0x14
Data0=(-30.821243,-64.993354),(-30.830513,-64.999877)
[END-RGN40]
"""

RGN80s = """
[IMG ID]
[END]

[RGN80]
Type=0x41
Data0=(-30.129791,-64.816200),(-30.133224,-64.814140),(-30.137859,-64.818092)
[END-RGN80]

[RGN80]
Type=0x3
Data0=(-30.712581,-64.813629),(-30.713096,-64.808303),(-30.720478,-64.803672)
[END-RGN80]

[RGN80]
Type=0x41
Label=LA QUINTANA
Data0=(-31.839323,-64.431465),(-31.843271,-64.430091),(-31.848936,-64.435241)
[END-RGN80]

[RGN80]
Type=0x3c
Label=EMBALSE LOS MOLINOS
Data0=(-31.826664,-64.571586),(-31.826664,-64.572616),(-31.826320,-64.572273)
[END-RGN80]

"""


twoHeaderSections = """
[img id]
[end]
; And now, for something completely different:
[img id]
[end]
"""

weirdTypes = """
[IMG ID]
[END]

[POI]
Type=0x999999
Data0=(-35.51966,-59.67464)
[END]

[POI]
Type=-3.14e27
Data0=(-35.60043,-59.81360)
[END]

[POI]
Type=Sarasa
Data0=(-35.70035,-58.88981)
[END]

[POLYLINE]
Type=0x123456789
Data0=(-35.07797,-59.09923),(-35.08840,-59.11088)
[END]

[POLYLINE]
Type=1919191919191919191919191919
Data0=(-35.24004,-59.48067),(-35.24277,-59.49114),(-35.24497,-59.49020)
[END]

[POLYGON]
Type=NobodyExpectsTheSpanishInquisition
Data0=(-35.76743,-58.49350),(-35.76776,-58.49378),(-35.76782,-58.49371)
[END]

"""

