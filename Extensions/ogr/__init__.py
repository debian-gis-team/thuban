# Copyright (C) 2004 by Intevation GmbH     vim:encoding=iso-8859-15:
# Authors:
# Nina H�ffmeyer <nhueffme@intevation.de>
#
# This program is free software under the GPL (>=v2)
# Read the file COPYING coming with the software for details.

__version__ = "$Revision: 2814 $"
# $Source$
# $Id: __init__.py 2814 2008-01-19 01:23:31Z bernhard $

# import the actual modules
from os import environ
from sys import platform

try:
    if platform != 'win32':
        dummy = environ['DISPLAY']
    import ogrstart
except KeyError:
    pass # For non-win32 platform, we don't have a DISPLAY, so don't import 
         # the modules (for test mode)
         # For win32 platform, there is  always have a graphical mode
         # Not sure whether this is the best method to avoid problems
         # in the global test routine.

# perform the registration of the extension
from Thuban import _, internal_from_unicode
from Thuban.UI.extensionregistry import ExtensionDesc, ext_registry

ext_registry.add(ExtensionDesc(
    name = 'OGRstart',
    version = '0.9.0',
    authors= [ internal_from_unicode(u'Nina H\xfcffmeyer') ],
    copyright = '2004 Intevation GmbH',
    desc = _("Open a file supported by ogr.")))
