<?xml version="1.0" encoding="UTF-8"?>
<!--    
    $Id: thuban-1.2.1.dtd 2842 2008-06-02 19:16:01Z bh $
    $HeadURL: svn+ssh://dpinte@scm.wald.intevation.org/thuban/trunk/thuban/Resources/XML/thuban-1.2.1.dtd $

    Copyright (C) 2001, 2003, 2004, 2005, 2008 by Intevation GmbH
    Authors:
        Jan-Oliver Wagner <jan@intevation.de>
        Bernhard Herzog <bh@intevation.de>

    This program is free software under the GPL (>=v2)
    Read the file COPYING coming with Thuban for details.

-->

<!-- a session contains zero or more database connections zero or more
     datasources and and zero or more maps

At some point, the namespace attribute will likely be used by Thuban to
identify the version of the file format. 
-->

<!ENTITY % shapesources "fileshapesource | derivedshapesource | dbshapesource">
<!ENTITY % tables "filetable | jointable">
<!ELEMENT session ((dbconnection | %shapesources; | %tables; )*, map*)>
<!ATTLIST session 
        title CDATA #REQUIRED
        xmlns CDATA #IMPLIED
>


<!--
    A db connection represents the connection to a database.

    Currently only connections to postgis databases are supported and
    thus The dbtype attribute must be "postgis".
-->

<!ELEMENT dbconnection EMPTY>
<!ATTLIST dbconnection
        id     ID    #REQUIRED
        dbtype CDATA #REQUIRED
        dbname CDATA #REQUIRED
        host   CDATA ""
        port   CDATA ""
        user   CDATA ""
>


<!-- A dbshapesource represents a table with geometry data in a database

     The dbconn attribute must be the id of a dbconnection element
     occurring earlier in the document.
-->

<!ELEMENT dbshapesource EMPTY>
<!ATTLIST dbshapesource
        id        ID    #REQUIRED
        dbconn    IDREF #REQUIRED
        tablename CDATA #REQUIRED
        id_column CDATA #REQUIRED
        geometry_column CDATA #REQUIRED
>

<!-- 
    A fileshapesource is a source of shapes and perhaps attribute data
    read from a file. The filetype attribute must be one of the
    supported file types. Currently only "shapefile" is supported.
 -->

<!ELEMENT fileshapesource EMPTY>
<!ATTLIST fileshapesource
        id       ID   #REQUIRED
        filename CDATA #REQUIRED
        filetype CDATA #REQUIRED
>

<!--
     A derivedshapesource is a source of shapes and attribute data
    derived from another shapesource for the shapes and a table for the
    attribute data.
 -->

<!ELEMENT derivedshapesource EMPTY>
<!ATTLIST derivedshapesource
        id          ID   #REQUIRED
        shapesource IDREF #REQUIRED
        table       IDREF #REQUIRED
>


<!-- a filetable points to the actual file that
	contains the tabular data.

     The filetype Attribute may be onve of "DBF" or "CSV"
-->
<!ELEMENT filetable EMPTY>
<!ATTLIST filetable 
        id       ID    #REQUIRED
        title    CDATA #REQUIRED
        filename CDATA #REQUIRED
        filetype CDATA #REQUIRED>

<!-- 
    A jointable is a table with the result of something equivalent to
    this SQL Statement:

     SELECT * FROM left JOIN right WHERE left.leftcolumn = right.rightcolumn;

    The attributes left and right must be the ids of two tables defined
    previously in the XML-Document file. leftcolumn and rightcolumn are 
    the column names to join on.

    The jointype attribute should be either "INNER" or "LEFT OUTER".
-->
<!ELEMENT jointable EMPTY>
<!ATTLIST jointable
        id          ID    #REQUIRED
        title       CDATA #REQUIRED
        left        IDREF #REQUIRED
        leftcolumn  CDATA #REQUIRED
        right       IDREF #REQUIRED
        rightcolumn CDATA #REQUIRED
        jointype    CDATA #REQUIRED
>

<!-- A Map

        A map consists of a number of layers
	the projection refers to the projection
	used to display the map. If the projection
	is missing, geographic coordinates in decimal
	degrees are assumed.
-->
<!ELEMENT map (projection?, (layer | rasterlayer)*, labellayer?)>
<!ATTLIST map title CDATA #REQUIRED>

<!-- a layer represents a set of geographic objects.
	pointers to the actual data are stored.
	The shapestore attributes contains the id of the data store
        shown in the layer.
	the projection refers to the projection
	in which the data are stored. If the projection
	is missing, geographic coordinates in decimal
	degrees are assumed. Visibility is assumed true
    unless otherwise specified.
-->
<!ELEMENT layer (projection?, classification?)>
<!ATTLIST layer title CDATA #REQUIRED>
<!ATTLIST layer shapestore IDREF #REQUIRED>
<!ATTLIST layer visible (true|false) "true">
<!-- the fill and stroke attributes can be either "None" or "#RRGGBB"
     RGB hex values

     All of fill, stroke, stroke_width and size may be omitted and default
     to "None", "#000000", "1" and "5" respectively.
 -->
<!ATTLIST layer fill CDATA "None">
<!ATTLIST layer stroke CDATA "#000000">
<!ATTLIST layer stroke_width CDATA "1">
<!ATTLIST layer size CDATA "5">


<!-- a rasterlayer represents an image that has some geographic data
    associated with it. The filename points to the image used.

    The opacity is the opacity as a decimal floating point number in the
    range 0.0 ... 1.0
-->
<!ELEMENT rasterlayer (projection?, classification?)>
<!ATTLIST rasterlayer 
        title    CDATA #REQUIRED
        filename CDATA #REQUIRED
        opacity CDATA #IMPLIED
        visible  (true|false) "true">


<!-- Classification data -->
<!ELEMENT classification (clnull?, clpoint*, clrange*, clpattern*, clcont*)>
<!ATTLIST classification field CDATA #IMPLIED>
<!ATTLIST classification field_type CDATA #IMPLIED>

<!ELEMENT clnull (cldata*)>
<!ELEMENT clpoint (cldata*)>
<!ELEMENT clrange (cldata*)>
<!ELEMENT clpattern (cldata*)>
<!ELEMENT clcont (cldata*)>

<!ATTLIST clnull label CDATA #IMPLIED>

<!ATTLIST clpoint value CDATA #REQUIRED>
<!ATTLIST clpoint label CDATA #IMPLIED>

<!ATTLIST clrange min CDATA #IMPLIED>
<!ATTLIST clrange max CDATA #IMPLIED>
<!ATTLIST clrange range CDATA #IMPLIED>
<!ATTLIST clrange label CDATA #IMPLIED>

<!ATTLIST clpattern pattern CDATA #REQUIRED>
<!ATTLIST clpattern label CDATA #IMPLIED>

<!ATTLIST clcont rmin CDATA #REQUIRED>
<!ATTLIST clcont rmax CDATA #REQUIRED>
<!ATTLIST clcont dmin CDATA #REQUIRED>
<!ATTLIST clcont dmax CDATA #REQUIRED>

<!-- Visual appearance of the classification 

     the fill and stroke attributes can be either "None" or "#RRGGBB"
     RGB hex values

     TODO: Actually, 'size' is only sensible for points. But adding it to
     clpoint would not be enough since the 'size' must also be available
     for the clnull. And clnull again is applied for lines and polygons.
     So, at some later point, maybe with introducing symbols, this
     has to be improved.

     All of fill, stroke, stroke_width and size may be omitted and default to
     "None", "#000000", "1" and "5" respectively.
 -->
<!ELEMENT cldata EMPTY>
<!ATTLIST cldata 
        stroke       CDATA #IMPLIED
        stroke_width CDATA #IMPLIED
        fill         CDATA #IMPLIED
        size         CDATA #IMPLIED
>


<!-- a projection has a number of parameters
-->
<!ELEMENT projection (parameter*)>
<!ATTLIST projection 
        name CDATA "" 
        epsg CDATA #IMPLIED >

<!-- just a simple parameter consisting of a value
-->
<!ELEMENT parameter EMPTY>
<!ATTLIST parameter value CDATA #REQUIRED>


<!-- The label layer contains text labels -->
<!ELEMENT labellayer (label*) >

<!ELEMENT label EMPTY>
<!ATTLIST label
        x CDATA #REQUIRED
        y CDATA #REQUIRED
        text CDATA #REQUIRED
        halign (left|center|right) #REQUIRED
        valign (top|center|bottom) #REQUIRED>
