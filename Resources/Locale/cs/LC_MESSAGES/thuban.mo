��    7     �  �  �#      `/     a/     y/     �/     �/     �/  !   �/     �/  
   �/     �/     0      0  	   <0     F0     Z0     h0  	   p0     z0  
   �0     �0     �0     �0     �0     �0     �0     �0  	   �0     �0     1     1     1     %1     ,1  
   11     <1     I1     Z1     c1     h1     w1     ~1  
   �1     �1     �1     �1     �1     �1     �1     �1     �1     �1  
   �1     �1     2  	   2     2     ,2     02     G2     T2     b2  	   r2     |2     �2     �2  &   �2      �2     �2     3     %3     63     H3     M3     ]3     q3  g   �3     �3  $   �3  	   4  	   (4     24  	   H4  |   R4     �4  	   �4  (   �4     5     5     -5     B5     I5     b5  '   t5      �5      �5  "   �5     6      6      :6     [6  $   b6  %   �6     �6     �6     �6     �6     �6     �6     7     7     )7     67     Q7     ]7     i7     �7  %   �7     �7     �7     �7  $   �7     �7     8     8     #8     28     Q8  !   c8  [   �8     �8     �8     �8     9  #   29     V9     k9     ~9     �9     �9     �9     �9     �9     �9     �9     :     :  	   +:     5:     =:     E:  
   M:  '   X:     �:     �:  
   �:     �:  $   �:  *   �:  =   ;     B;     [;     a;     i;     u;     z;     �;     �;     �;     �;     �;     �;     <     <  )   4<  
   ^<     i<     n<     t<     �<     �<     �<     �<     �<     �<     =     =     =  
   9=     D=     U=     f=     v=  &   �=  ;   �=     �=  	   �=  *   	>     4>     B>     O>  "   a>     �>  $   �>     �>     �>     �>     �>     �>     �>  	   �>     ?  	   
?     ?     !?     &?     A?     R?     e?  j   m?     �?  >   �?     @     0@     F@     O@     ^@     v@  	   �@  
   �@     �@     �@  
   �@     �@  	   �@  	   �@     �@     �@     A  
   A  	   A     A  	   .A  	   8A     BA     QA     ]A  
   nA  
   yA  
   �A  	   �A     �A  
   �A     �A  >   �A     B     &B     7B     JB     dB  	   sB  -   }B     �B  .   �B  !   �B     C     C     $C     AC  	   ^C     hC  *   �C     �C     �C     �C     �C     �C  -   �C     D  "   -D     PD  
   XD     cD     iD     yD     �D     �D  $   �D     �D  %   �D  	   E     E  
   E     E     4E     EE     UE     bE     qE     ~E     �E     �E     �E     �E  
   �E     �E     �E     �E     �E  
   F     F     F     /F     =F     [F     wF     {F  
   �F  	   �F     �F     �F     �F     �F     �F     �F     �F     �F     �F  	   G     G     G     <G     BG  N   FG     �G     �G     �G  ?   �G  =   H     UH      gH     �H     �H     �H     �H     �H     �H     �H     
I     I     1I     ?I     BI  W   II     �I     �I     �I  
   �I     �I     �I     J      J     0J     CJ  )   XJ  	   �J     �J     �J     �J     �J     �J     �J     �J     K     K     $K     2K     AK     XK     `K  
   qK     |K     �K     �K     �K     �K  4   �K     L  `   L      L     �L     �L     �L  �   �L     �M     �M     �M     �M     �M     �M     N     N     N     5N     KN  
   XN     cN     pN     �N     �N     �N  :   �N  +   �N     (O     1O     EO     YO     aO     hO  9   pO     �O     �O     �O  0   �O     P     -P     6P     FP     SP     cP     wP     �P     �P  &   �P     �P  A   �P     ,Q  	   JQ  	   TQ     ^Q     fQ     tQ      �Q     �Q     �Q  
   �Q     �Q  
   �Q  
   �Q     R  
   R  #   R     >R     ^R  	   dR     nR     sR     zR     �R     �R     �R     �R  
   �R     �R     �R  %   �R     S     "S  	   )S     3S     FS     YS     rS     �S     �S     �S     �S     �S  %   �S     T  	   %T     /T     6T  	   ;T     ET  $   JT  #   oT  !   �T  .   �T  ^   �T     CU  h   aU  6   �U  Q   V     SV     ZV  �   fV  4   �V  )   .W  F   XW     �W     �W     �W     �W  .   �W  #   	X  	   -X     7X     EX     QX     eX  (   iX  9   �X     �X  %   �X     �X     �X     Y      Y     5Y  	   CY     MY     kY     sY     �Y  )   �Y     �Y     �Y  
   �Y  (   
Z     3Z     :Z     >Z     DZ     JZ     RZ     YZ     ]Z     mZ  %   �Z     �Z     �Z     �Z     �Z  	   �Z     �Z     �Z  !   [     )[     -[     1[  	   7[     A[     _[  !   {[  +   �[     �[  
   �[  
   �[     �[     �[  !   \  4   %\  2   Z\     �\     �\     �\  %   �\  ^  �\     7^     N^     d^     t^     �^  #   �^     �^  
   �^     �^  "   �^     _     _     -_     ?_     Q_  
   W_     b_     y_     �_     �_     �_     �_     �_     �_     �_     �_     �_  
    `     `     `     `     *`  
   0`     ;`     H`     [`     g`     n`     |`     �`     �`     �`  
   �`     �`     �`     �`     �`  
   �`     a     a  
   a     (a     8a  	   @a  	   Ja     Ta     [a     ya     �a     �a     �a     �a     �a     �a  '   �a  #   b     Cb     _b     qb     �b     �b     �b     �b     �b  m   �b     Nc  '   _c     �c     �c     �c  	   �c  o   �c     %d     1d  *   =d     hd     vd     �d     �d     �d     �d  4   �d  #   e  #   >e  &   be     �e     �e  $   �e     �e  (   �e  %   f     =f     Qf     Xf     ef     xf     �f     �f     �f     �f     �f     �f     �f     g     g     *g     Ig     Pg     _g  ,   kg     �g     �g     �g     �g     �g     �g      h  S    h  
   th     h     �h     �h  &   �h     �h     �h  
   i     !i     3i     Pi     di     i     �i     �i     �i     �i     �i  	   �i     �i     �i  	   �i  %   �i  	   j     &j     +j     7j  )   Uj  0   j  ;   �j     �j     k     k  
   k      k     )k     Ak     Sk     dk     |k     �k     �k     �k     �k     �k  
   �k     
l     l     l     1l  "   @l     cl     }l     �l     �l     �l     �l  $   �l     �l     �l     m     "m     8m  '   Km  8   sm     �m  	   �m  )   �m     �m     	n  !   n  3   7n     kn  0   �n     �n     �n  	   �n     �n     �n     �n     �n     �n     o     o     (o     .o     Go     ^o     po  h   xo     �o  [   �o     Ip     ]p     sp     |p     �p  !   �p  	   �p     �p     �p     �p     �p     q  
   q     q     /q     8q     @q     Gq     Uq     eq     vq  
   q     �q     �q     �q  
   �q     �q     �q     �q     �q     �q      
r  <   +r     hr     mr     �r  $   �r     �r  	   �r  ,   �r     s  &   s  $   <s     as  
   ws     �s     �s  	   �s     �s  +   �s     t     t      t     0t     It  *   Xt     �t  #   �t     �t     �t     �t     �t     �t     �t     �t  %   u     6u  %   Iu     ou     {u     �u     �u     �u     �u     �u     �u     �u     �u     v     v     !v     )v     @v     Lv     Zv     jv     yv     �v     �v     �v  	   �v     �v     �v      w     w     w  	   %w     /w     8w     Pw     Uw     ^w     cw     xw     �w     �w     �w     �w     �w     �w     �w  _   �w     Wx     ux     �x  @   �x  >   �x     *y  &   Fy  $   my  (   �y      �y     �y     �y     �y     �y     z     z     'z     7z     :z  \   Cz     �z     �z     �z     �z     �z     �z     {     7{     I{     c{  .   x{     �{     �{     �{     �{     �{     |     "|     (|     4|     <|  	   B|     L|     Y|     l|     t|     �|  	   �|     �|     �|     �|     �|  %   �|     �|  i   }  #   �}  %   �}     �}     �}  �   �}     �~     �~     �~               (     6  	   D     N     k     �     �     �  &   �     �     �     �  *   �  *   H�     s�     z�     ��     ��  	   ��     ��  7   ��     �     �     �  2   7�     j�     |�     ��     ��     ��     ��     ʁ     ܁     �      �     $�  9   =�     w�  	   ��     ��     ��     ��  
   ��  "   Ƃ     �     �  	   �     $�     4�  	   A�     K�     [�  +   l�     ��  	   ��  	        ̃  
   Ճ     ��     ��     ��     �     �  	   .�     8�  	   H�  $   R�     w�     ��     ��     ��     ��     Ä     ل     ��     �     $�     <�     C�  (   T�     }�     ��     ��     ��  	   ��     ��     ��     υ     �  #   �  j   +�  !   ��  l   ��  +   %�  v   Q�     ȇ     χ  s   ۇ  5   O�  :   ��  D   ��     �     �     $�     -�  +   D�  #   p�     ��     ��     ��     ��     ҉  (   ى  L   �     O�  %   S�     y�  '   ~�     ��     ��          Ԋ     �     ��  &   �     .�  ,   G�     t�     ��     ��  '   ��     ̋     ً  	   ��     �  	   �     ��     �     �     �  -   .�     \�     e�     ��     ��  
   ��     ��     ��  !   ��     ތ     �     �     �     ��     �     )�  1   ;�     m�  
   u�  
   ��     ��     ��  !   ��  :   ˍ  -   �     4�     A�     T�  (   i�     �  $  �       �   �   1  d  �              r            �   Y      $      �   #      @   �  T  �   x  X  �   +  �   >      N   A  �          `   5      �   �   �    D   �       �  5             *  q  v  �  @  9  }                     �   �   Z    6        �  �       �       �          �                     6      �   �   �  4   �   {       e   �   #             �  Y   .   �   �  f  F  �      .  	   d   S        �  }  %  �    �          P   �        
       �   i  �       Z       ?   :  /      8       �     \   M  �           �       %   �  �  ?      �  �  A   �  �   �      
  �  <  9   N  �  C   `  1   �       �   &  (  o  �   n       {      �  a  7  �   �   �   �               [   �   �           2   �   �       M   �          �     �  �   3       �       C  |      �  �   s       �             �  �  V   �   �  	  u       �   t       �   _   �       �   -   ,  a   m      �      �   �       j     �              �   B       �       l          �  �     �      �  �  �  k   p       F               �  '  H     �   >   �    j   �   h      �   �  5     �  �  �      "   
  �         T      �       �  �      6      �   �  b   o       �          �       K   �   m   �      �  ]  ^  �  e    �   �          0      �   �  �         �              �  �   �   w         �  �  �  Q       �   �       �   i       "      �               �  w   (   /   �     b      �   )   �  �   z          �   �      "  1  �       �             '   �   :   �  s      ;          J   3  $   ,   �     �   �  O    7           �   �      �       �  h          !      �   �   �   �      E  �  �  �  �       u      3  D  <   g    X   &  �       �   �   �   �  �   E         G   #  �      �  �          �   x   �       t  �  +          *  �   �  L   �           �                B    �  L      �  �           |     �      �   f   U           *   �  �   y      �   �  �             4      �   +       R           )  K  �       �  �     �   ~   ^   �   7  =     �  �  �   z   �       8  �       (  V  	  I  \  �       �               �  �   n  �   �      _  !  �  c          �            H   I     �     �  ,    �     �     �   �  P  p  �         �   ;   �   g   �      W      �           �          �                  '    �   �  R  �   �     2      /        !   S   �   0      y     �      &     �  4  =  �   ]       �      �           �          �  v   %            �      �  �  �           �  �   J  �  �  �   �              �  �    �  U     �   )  �  G  �      �   �  �   �  �  q       [  O            ~      �   -    W      �  �         c                       �      r   2  l   0   k    �      Q  �      .         �       �       �     -  �   	Internal encoding: %s
 	None registered.
 	method %s of %s %d locations converted %d objects loaded %i rows (%i selected), %i columns %s 
 %s %s < %s %s (current) %s Layer loaded from file. %s Layer loaded into Thuban &About... &Add Image Layer... &Add Layer... &Bottom &Close... &Database Connections... &Duplicate &Edit mapfile &File &Full extent &Full layer extent &Full selection extent &Help &Hide &Identify &Join Table... &Join... &Label &Layer &Lower &Map &MapServer &New Session &Open Session... &Open... &Pan &Properties... &Raise &Remove Layer &Rename... &Save Session &Show &Show... &Table &Top &Unjoin Table... &Visible &Zoom in (experimental)  (testing)  - not available <None> <Unknown> About Thuban Add Add &Database Layer... Add Database Add GNS Layer Add Image Layer Add Layer Add Layer from database Add WMS layer ... Add a WMS Layer Add a new database layer to active map Add a new image layer to the map Add a new layer to the map Add to List Add to Selection Add/Remove labels Airy All Files (*.*) All Files (*.*)|*.* Alternative Path An unhandled exception occurred:
%s
(please report to http://thuban.intevation.org/bugtracker.html)

%s Apply to Range ArcView Project Files (*.apr)|*.apr| Authors:
 Available Available Projections BBox Dump Based on the data from the table and the input
values, the exact quantiles could not be generated.

Accept a close estimate? Bessel 1841 Blue Ramp Both parameters must have a drive letter Bottom Layer Bounding Box Dump Bounding Box Dump %s Buffer CSV Files (*.csv)|*.csv| CSV files (*.csv) Can not propose: No bounding box found. Can't open file '%s' for reading Can't open file '%s' for writing Can't open the database table '%s' Can't open the datasource '%s' Can't open the file '%s'. Can't open the rasterlayer '%s'. Cancel Cannot import the thuban_cfg module. Cannot import the thubanstart module
 Central Meridian: Change Change Color Change Fill Color Change ImageColor Change Label Change Line Color Choose file format Choose layer Choose layer from database Clarke 1866 Clarke 1880 Clash of layer names!
 Classification Classification range is not a number! Close Close Table Close Window Close one or more tables from a list Collecting shapes ... Color Color Scheme: Compiled for:
 Connection '%s' already exists Conversion failed Convert GNS-file into a shapefile Converts GNS (Geographical Name Service
of NIMA) to Shapefile format and
displays the data. Copy of `%s' Copyright %s
 Could not read "%s": %s Could not write SVG because:  Create a new empty mapscript MapObj Create a new mapfile Create new mapfile Currently using:
 Custom Ramp DB Connection Parameters DBF Files (*.dbf) DBF Files (*.dbf)|*.dbf| DEFAULT DGN files (*.dgn) Data Type: %s Database Management Database Name: Databases Decimal Default Degrees Deprecated Details on the registered extensions:

 Developers:
 Dock Driver: %s Dump Bounding Boxes To Dump Bounding Boxes of Layer Objects Dump bounding boxes of selected shapes ... Dumps the bounding boxes of all
shapes of the selected layer. Duplicate selected layer E&xit E&xport E&xtensions Edit Edit Layer Properties Edit Metadata Edit Symbol Edit WMS Properties Edit the Legend Setting Edit the Map Setting Edit the Metadata Setting Edit the Scalebar Setting Edit the Web Setting Edit the properties of the selected layer Ellipsoid: End: Error Error Loading Expression Error Loading Layer Error Loading Raster Layer Error in projection "%s": %s Error: SVG not written! Exit Exit Thuban now Experimenta&l Export Export Layer as Shapefile ... Export Map Export Selection Export Shapefile Export Table To Export mapfile Export the active layer as a Shapefile Export the current map and legend in Thuban-map-SVG format. Export the map to file Export... Exports the selected layer as a Shapefile. Extension: %s Extensions:
 Extent (lat-lon): Extent (lat-lon): (%g, %g, %g, %g) Extent (lat-lon): None Extent (projected): (%g, %g, %g, %g) False Easting: False Northing: Fees: Field Field: Field:  Field: %s File: Filename: Filename: %s Fill Finish working with Thuban Fix Border Color Format version: %s Format: Found the following as an alternative for '%s':
%s

Please confirm with Yes or select alternative with No. French GDAL image information unavailable. See About box for details. GML files (*.gml) GRS 1980 (IUGG, 1980) Generate Generate Class Generate Classification Generate Files (*.txt)|*.txt| Generate: Geographic Geometry Column German Green Ramp Green-to-Red Ramp Grey Ramp Group by: Group: Height:  Hidden Hide Layer Hostname: Hot-to-Cold Ramp Hungarian ID Column Identify Shape Image Color Image Properties Image Type ImageColor Imagepath: Imageurl: Import Import APR Import a ArcView project file Import a ArcView project file (.apr)
and convert it to Thuban. Import a layer from a mapfile Import a mapfile Import apr-file... Import layer from mapfile Import mapfile Import... Imported %d out of %d themes of view "%s" ... Info Info about Thuban authors, version and modules Initialization not yet requested. Initialization successful. Integer Internal make_id() failure:  International 1909 (Hayford) Intervals Invalid color specification %s Invalid hexadecimal color specification %s Italian Join Join Failed Join Layer with Table Join Tables Join and attach a table to the selected layer Join failed:
  %s Join two tables creating a new one KeySize KeySpacing Label Label Layer: %s Label Values Labels Lambert Conic Conformal Latitude of first standard parallel: Latitude of origin: Latitude of second standard parallel: Latitude: Layer Layer '%s' Layer Projection: %s Layer Properties Layer Table: %s Layer Title: Layer Type: %s Layer loaded Layer-Name: Layers Lead Developer:
 Legend Library not available Line Color Line Width:  Line Width: %s Loading Layer Loading failed Longitude: Lower Layer Lower selected layer Maintainers:
 Make selected layer unvisible Make selected layer visible Map Map Projection: %s Map Title: Map-Name: Map: %s MapInfo files (*.tab) Max: Metadata Min: Mindistance Minfeaturesize Modified Mouse Position Tool Move Down Move Up Multiple Projections selected Name: New No GDAL support because module '%s' cannot be imported. Python exception: '%s' No Layer found. No Projections selected No capabilities available No implementation of get_application_dir available for platform No implementation of relative_filename available for platform No layer selected No thubanstart module available
 No title found for layer #%d No view found in APR file No ~/.thuban directory
 None Normal Number of Bands: %i Number of Classes: Number of Groups: Number of labels: %d ODB File '%s' OK Offset On mouse click display the current coordinates
in a dialog for easy further processing. Opacity: Open Session Open Shapepath Open Table Open a DBF-table from a file Open a file supported by ogr. Open a session file Open datasource Open layer via OGR Other Contributors:
 Outer Join (preserves left table records) Password: Pattern Pick a View to import: Pick the table to rename: Pick the table to show: Pick the tables to close: Port: Portuguese (Brazilian) Preview: Prin&t Print the map Pro&jection... Problem with Quantiles Proceed Project Name: %s Projection Projection: Projection: %s Projection: None Projection: Propose UTM Zone Propose Provide a profiler and a timer
for screen rendering. Provide layers via OGC WMS. Provide management methods for UMN MapServer
.map-files. These can be created/imported/modified. Put selected layer to the bottom Put selected layer to the top Quantiles from Table Query Questions and comments can be sent to the following addresses:
	General list (public):
		<thuban-list@intevation.de>
	Developers list (public):
		<thuban-devel@intevation.de>
	Thuban team at Intevation:
		<thuban@intevation.de>
 Radians Raise Layer Raise selected layer Range Re&name ... Red Ramp Refine Selection Remove Remove Database Connection Remove selected layer Rename Layer Rename Map Rename Table Rename one or more tables Rename selected layer Rename the map Replace Selection Resource '%s' does support neither WMS version 1.1 nor 1.0 Resource '%s' is neither local file nor URL Retrieve Retrieve From Table Retrieve from Table Reverse Revert Russian SRS '%s' is not numerical and not AUTO/NONE in layer '%s' Save Session &As... Save Session As Save this session to a new file Save this session to the file it was opened from Scale Factor: Scalebar Select APR file Select Color Select GNS file Select MapFile file Select Properties Select WMS Server Select a data file Select an alternative data file for %s Select an image file Select layer '%s' and pick a projection using Layer/Projection... Select one or more data files Select... Selection Session Session &Tree Session: %s Set or change the map projection Shapefile  Files (*.shp)|*.shp| Shapefiles (*.shp) Shapes: %d Shapetype: %s Show EPSG: Show Layer Show Ta&ble Show Table Show one or more tables in a dialog Show the selected layer's table Shown Singleton Size Size:  Size: %ix%i Size: %s Sort Source Data is in:  Source of Projection: %s Source: %s Southern Hemisphere Spanish Specify projection for selected layer Start a new session Start: Stepping: Stop Identify Mode Storing shapes ... Submenu %s doesn't exist Switch to map-mode 'identify' Switch to map-mode 'pan' Switch to map-mode 'zoom-in' Switch to map-mode 'zoom-out' Symbol Table Title: Table not compatible with shapestore. Table: Table: %s Tables Take Template: Text The Default group cannot be removed. The Dialog named %s is already open The connection %s
is still in use The current map extent center lies in UTM Zone The current session contains Image layers,
but the GDAL library is not available to draw them. The following error occured:
 The joined table has %(joined)d rows but it must have %(needed)d rows to be used with the selected layer The session has been modified. Do you want to save it? This is the wxPython-based Graphical User Interface for exploring geographic data Thuban Thuban - %s Thuban does not know the parameters
for the current projection and cannot
display a configuration panel.

The unidentified set of parameters is:

 Thuban is a program for exploring geographic data.

 Thuban is licensed under the GNU GPL v>=2 Thuban was compiled with wx %(wxproj-wx)s but wxPython is %(wxPython)s Thuban: Internal Error Title: Title:  Toggle Legend on/off Toggle on/off the session tree analysis window Toggle visibility of selected layer Top Layer Translators:
 Transparent Transverse Mercator Try Trying to read ~/.thuban/thubanstart.py. Two layers probably have the same name, try renaming one. Type UMN MapServer Mapfiles (*.map)|*.map| URL: Undo the last join operation Undock Uniform Distribution Unique Values Unit Type Universal Transverse Mercator Unknown Unknown Object list named: '%s' Unknown Object named: %s Unknown Value named: '%s' with value '%s' Unknown command %s Unknown command ID %d Unmodified Unsupported group type in classification Update Use User: Value Visible WGS 84 WMS WMS Information Warning: %s.%s: %s%s
 Warning: Raster layer not written as  Warnings Warnings when reading "%s":

%s Web Width:  Write SVG Write SVG Legend Write SVG Map Write a basic Legend for the map. X:  Y:  Zone: Zoom &out Zoom to the full layer extent Zoom to the full map extent Zoom to the full selection extent first argument must be an absolute filename gns2shp gns2shp %s gns2shp... invalid index lineWidth < 1 no receivers for channel %s of %s no shp file definied, maybe used a feature obj '%s'. receiver %s%s is not connected to channel %s of %s size < 1 unnamed map unnamed session xml field type differs from database! Project-Id-Version: Thuban 1.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-08-18 15:17+0200
PO-Revision-Date: 2008-01-27 11:09+0100
Last-Translator: Jachym Cepicky <jachym@les-ejk.cz>
Language-Team: Thuban Development List <thuban-devel@intevation.de>
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-2
Content-Transfer-Encoding: 8bit
 	Vnit�n� k�dov�n�: %s
 	��dn� registrovan�.
 	metoda %s z %s %d oblast� p�evedeno %d objekt� na�teno %i ��dk� (%i vybran�ch), %i sloupc� %s 
 %s %s < %s %s (aktu�ln�) Vrstva %s byla na�tena ze souboru. Vrstva %s na�tena do Thubanu &O programu... P�idat &sn�mek... &P�idat vrstvu... &Dolu &Zav��t... &Datab�zov� spojen�... &Duplikovat &Editovat map-soubor &Soubor &Cel� region Region vrstev Zv�t�en� na &v�b�r &Pomoc &Zkr�t &Identifikovat &Spojit tabulky... &Spojit... �&t�tek &Vrstva Posunout &dolu &Mapa &MapServer &Nov� sezen� &Otev��t sezen�... &Otev��t... &Posun &Nastaven�... Posunout &nahoru Smazat v&rstvu P&�ejmenovat... Ulo�it &sezen� &Viditeln� &Zobrazit... &Tabulka &Nahoru Smazat sp&ojen�... &Viditeln� &Zv�t�it (experiment�ln�)  (testing). - nen� dostupn� <��dn�> <Nezn�m�> O Thubanu P�idat P�idat &datab�zovou vrstvu... P�idat datab�zi P�idat vrstvu GNS P�idat sn�mek P�idat vrstvu P�idat vrstvu z datab�ze P�idat vrstvu WMS ... Ebene hinzuf�gen P�idat novou datab�zovou vrstvu do mapy P�idat nov� rastrov� soubor do mapy P�idat novou vrstvu do mapy P�idat do seznamu P�idat do v�b�ru P�idat/Odebrat �t�tky Airy V�echny soubory (*.*) V�echny soubory (*.*)|*.* Alternativn� cesta Byla zachycena nezpracovan� vyj�mka:
%s
(pros�m nahla�te na http://thuban.intevation.org/bugtracker.html)

%s Pou��t na rozsah ArcView·Project·Files·(*.apr)|*.apr| Auto�i:
 Dostupn� Dostupn� projekce BBox Dump Na z�klad� dat z tabulky atribut� a vstupn�ch hodnot
nelze spo��tat p�esn� quantily.

Akceptujete bl�zk� odhad? Bessel 1841 Modr� barvy Oba parametry mus� obsahovat p�smeno disku Spodn� vrstva V�pis hrani�n�ch sou�adnic V�pis hrani�n�ch sou�adnic %s Buffer Soubory CSV (*.csv)|*.csv| Soubory CSV (*.csv) Nelze navrhnout: Nebyly nalezeny hrani�n� sou�adnice Nelze otev��t soubor '%s' pro �ten� Nelze otev��t soubor '%s' pro z�pis Nelze otev��t datab�zovou tabulku '%s' Nelze otev��t zdroj dat '%s' Nelzte otev��t soubor '%s'. Nelze otev��t rastrovou vrstvu '%s'. Zru�it Modul thuban_cfg nebyl nelze importovat. Nelze na��st startovn� modul Thubanu
 Centr�ln� meridian: Zm�nit Zm�nit barvu Zm�na barvy v�pln� Zm�nit barvu obr�zku Zm�nit �t�tek Zm�na barvy linie Vybrat form�t souboru Vybrat vrstvu Vybrat vrstvu z datab�ze Clarke 1866 Clarke 1880 Jm�no vrstvy ji� existuje!
 Klasifikace Rozsah klasifikace nen� ��slo! Zav��t Zav��t tabulku Zav��t okno Zav��t jednu nebo v�echny tabulky ze seznamu Shroma��uji objekty ... Barva Barevn� sch�ma: Kompilov�no pro:
 Spojen� '%s' ji� existuje P�evod selhal P�evede soubor GNS na Shapefile P�evede GNS (Geographical Name Service
of NIMA) na form�t Shapefile a
zobraz� data. Kopie `%s' Copyright %s
 Nelze ��st "%s": %s Nelze zapsat SVG, proto�e:  Vytvo�it nov� pr�zdn� mapscript MapObj Vytvo�it nov� mapfile Vytvo�it nov� mapov� soubor Pou��v�m:
 Vlastn� rozd�len� Parametry spojen� s datab�z� DBF Soubory (*.dbf) Soubory DBF (*.dbf)|*.dbf| V�CHOZ� Soubory DGN (*.dgn) Datov� typ: %s Spr�va datab�ze Jm�no datab�ze: Datab�ze Decim�ln� V�choz� Stupn� Zastaral� Detaily k registrovan�m roz���en�m:

 V�voj��:
 Dock Ovlada�: %s Vypsat hrani�n� sou�adnice do Vyp��e hrani�n� sou�adnice objekt� vrstvy Vyp��e hrani�n� sou�adnice vybran�ch objekt� ... Vyp��e hrani�n� sou�adnice v�ech
objekt� ve vybran� vrstv�. Duplikovat vybranou vrstvu E&xit E&xport Ro&z���en� Editovat Zm�nit vlasnosti vrstvy Editovat metadata Zpracovat symbol Editovat vlastnosti WMS Editovat legendu Editovat nastaven� mapy Editovat metadata Editovat m���tko Editovat nastaven� Web Zm�nit nastaven� vybran� vrstvy Ellipsoid: Konec: Chyba Chyba p�i na��t�n� v�razu Chybov� vrstva Chyba p�i na��t�n� rastrov� vrstvy Chyba v projekti "%s": %s Chyba: SVG nebylo zaps�no! Ukon�it Ukon�it Thuban Experiment�&ln� Export Exportovat vrstvu jako Shapefile ... Exportovan� mapa Exportovat v�b�r Exportovat Shapefile Exportovat tabulku do Exportovat mapfile Exportuje aktivn� vrstvu jako Shapefile Exportuje aktu�ln� mapu a legendu ve form�tu Thuban SVG. Exportovat mapu do souboru Export... Exportuje vybranou vrstvu jako Shapefile. Roz���en�: %s Roz���en�:
 Hrani�n� sou�adnice (���ka-d�lka) Hrani�n� sou�adnice (���ka-d�lka): (%g, %g, %g, %g) Rozlo�en� (���ka-D�lka): ��dn� Hrani�n� sou�adnice (projekce): (%g, %g, %g, %g) False easting: False northing: Poplatky: Pole Pole: Pole:  Pole: %s Soubor: Jm�no souboru: Jm�no souboru: %s V�pl� Ukon�it pr�ci s Thubanem Nastaven� barva okraj� Verze form�tu: %s Form�t: Jako alternativa k %s nalezeny n�sleduj�c�:
%s

Pros�m potvr�te Ano nebo vyberte alternativn� pomoc� Ne. Francouzsky Informace o sn�mku z�skan� pomoc� knihovny GDAL nejsou dostupn�. Pro detaily viz About box. Soubory GML (*.gml) GRS 1980 (IUGG, 1980) Vytvo�it Vytvo�it t��dy Generovat klasifikaci Generovan� soubory (*.txt)|*.txt| Vytvo�it: Geografick� Sloupec s geometri� N�mecky Zelen� barvy Zelen� do �erven� �ed� barvy Seskupit podle: Skupina: V��ka:  Zkryt� Zkryt� vrstva Jm�no po��ta�e: Hork� do studen� Ma�arsky ID-Sloupec Identifikovat objekt Barva obr�zku Vlastnosti sn�mku Typ sn�mku Barva sn�mu Cesta k obr�zku: URL obr�zku: Import Importovat APR Importuje soubor ArcView Project Importuje soubor ArcView Project (.apr)
a p�evede do Thubanu Impo Importovat mapov� soubor Import souboru apr... Importovat vrstvu z mapov�ho souboru Import mapov�ho souboru Import... %d z %d t�mat pohledu "%s" naimportov�no ... Info Informace o autorech, verzi a modulech Inicializace zat�m nebyla po�adov�na Inicializace �sp�n�. Cel� ��slo Vnitřní chyba·make_id():· International 1909 (Hayford) Intervaly Neplatn� specifikace barvy %s Neplatn� hexadecim�ln� specifikace barvy %s Italsky Spojit Spojen� selhalo Spojit vrstvu s tabulkou Spojit tabulky Spojit tabulky a p�i�adit k vybran� vrstv� Pole spojen�:
 %s Spojit dv� tabulky a vytvo�it novou Velikost Mezera �t�tek Vrstva �t�tk�: %s Hodnoty �t�tk� �t�tky Lambert Conic Conformal Zem. ���ka prvn� standardn� pararely: Zem. ���ka p�vodu: Zem. ���ka druh� standardn� paralely: Zem. ���ka: Vrstva Vrstva '%s' Projekce vrstvy: %s Vlastnosti vrstvy Tabulkov� vrstva: %s Titulek vrstvy: %s Typ vrstvy: %s Vrstva na�tena Jm�no vrstvy: &Vrstvy Hlavn� v�voj��:
 Legenda Knihovna nen� dostupn� Barva linie ���ka linie:  ���ka linie: %s Na��t�m vrstvu Import selhal Zem. d�lka: Sn��it vrstvu Posunout vybranou vrstvu dolu Spr�vce:
 Vybran� vrstva bude zkryta Vybran� vrstva je viditeln� Mapa Projekce mapy: %s Titulek mapy: Map-Name: Mapa: %s Soubory MapInfo (*.tab) Max: Metadata Min: Minim�ln� vzd�lenost Minim�ln� velikost prvku Zm�n�no N�stroj pro pozici my�i Posunout n��e Posunout v��e Bylo vybr�no n�kolik projekc� Jm�no: Nov� Podpora pro GDAL nen� dostupn�, proto�e modul '%s' se nepoda�ilo importovat.Chyba pythonu: '%s' ��dn� vrstva nebyla nalezena. Nebyla vybr�na projekce Capabilities nejsou dostupn� Na t�to platform� nen� dostupn� implementace get_application_dir Na t�to platform� nen� dostupn� implementace relative_filename ��dn� vrstva nebyla vybr�na Startovn� modul Thubanu nen� dostupn�
 Pro vrstvu #%d nebyl nalezen titulek V souboru APR nebyl nalezen ��dn� n�hled Adres�� ~/.thuban nen� dostupn�
 ��dn� Norm�ln� Po�et kan�l�: %s Po�et t��d: Po�et skupin: Po�et �t�tk�: %d Soubor ODB '%s' OK Odsazen� Zobraz� aktu�ln� sou�adnice v dialogu 
po kliknut� my�� do mapy pro sna��� dal��
zpracov�n�. Pr�hlednost: Otev��t sezen� Otev��t Shapepath Otev��t tabulku Otev��t tabulku DBF ze souboru Otev��t soubor podporovan� OGR. Otev��t soubor se sezen�m Otev��t zdroj dat Otev��t vrstvy pomoc� OGR Dal�� p�isp�vatel�:
 Vn�j�� spojen� (podr�� z�znamy z lev� tabulky) Heslo: Vzor Vyberte n�hled pro import: Zvolit tabulku k p�ejmenov�n�: Zvolit tabulky k otev�en�: Zvolte tabulky k zav�en�: Port: Portugalsky N�hled: &Tisk Tisk mapy Pro&jekce... Probl�m s quantily Vykonat Jm�no projektu: %s Projekce Projekce: Projekce: %s Projekce: ��dn� Projekce: Navrhuji UTM Z�nu N�vrh Profil a �asova� renderingu obrazovky Nab�dnout vrstvy pomoc� OGC WMS Obsahuje spr�vcovsk� moduly pro mapsoubory UMN MapServer.
Soubory mohou b�t vytv��eny/importov�ny/m�n�ny. Posunout vybranou vrstvu zcela dolu Posunout vybranou vrstvu zcela nahoru Quantily z tabulky Dotaz Ot�zky a koment��e mohou b�t posl�ny na n�sleduj�c� adresy:
	Obecn� konference (ve�ejn�):
		<thuban-list@intevation.de>
	V�voj��sk� konference (ve�ejn�):
		<thuban-devel@intevation.de>
	Thuban� t�m v Intevation:
		<thuban@intevation.de>
 Radi�ny Zv��it vrstvu Posunout vybranou vrstvu nahoru Rozsah &P�ejmenovat ... �erven� barvy Zjemnit v�b�r Odstranit Odstranit spojen� s datab�z� Smazat vybranou vrstvu P�ejmenovat vrstvu P�ejmenovat mapu P�ejmenovat tabulku P�ejmenovat jednu nebo v�echny tabulky P�ejmenovat vybranou vrstvu P�ejmenovat mapu Nahradit v�b�r Zdroj '%s' nepodporuje ani WMS 1.0 ani 1.1 Zdroj '%s' nen� ani lok�ln� soubor ani URL Z�skat Z�skat z tabulky Z�skat z tabulky Obr�cen� Vz�t zp�t Rusky SRS '%s' nen� numerical a nen� AUTO/NONE ve vrstv� '%s' Ulo�it sezen� j&ako... Ulo�it sezen� jako Ulo�it sezen� do nov�ho souboru Ulo�it sezen� do souboru, ze kter�ho bylo otev�eno M���tkov� faktor: M���tko Vybrat soubor APR Vybrat barvu Vybrat soubor GNS Vybrat mapov� soubor Vybrat vlastnosti Vybrat server WMS Vybrat datov� soubor Zvolte jin� jm�no souboru pro %s Zvolit soubor se sn�mkem Zvolte vrstvu '%s' a zvolte projekci v Vrstva/Projekce... Zvolte jeden nebo v�ce soubor� Vybrat... V�b�r Sezen� S&trom sezen� Sezen�: %s Nastavit nebo zm�nit projekci mapy Shapefiles (*.shp)|*.shp| ESRI Shapefile (*.shp) Prvky: %d Typ objektu: %s Uk�zat EPSG: Viditeln� Uk�zat ta&bulku Zobrazit tabulku Uk�zat jednu nebo v�echny tabulky v dialogu Zobrazot tabulku vybran� vrstvy Viditeln� Singleton Velikost Velikost:  Velikost: %ix%i Velikost: %s Se�adit Zdrojov� data jsou v: Zdroj projekc�: %s Zdroj: %s Ji�n� hemisf�ra �pan�lsky Specifikovat projekci vybran� vrstvy Za��t nov� sezen� Start: Krok: Ukon�it m�d identifikace Ukl�d�m objekty ... Submenu %s neexistuje P�epnout m�d 'identifikace' P�epnout m�d pro posun P�epnout zv�t�ovac� m�d P�epnout zmen�ovac� m�d Symbol Titulek tabulky: Tabulka nen� kompatibiln� se shapestore. Tabulka: Tabulka: %s Tabulky P�evz�t �ablona:  Text Nelze smazat v�choz� skupinu. Dialog %s je ji� otev�en� Spojen� %s
je st�le pou��v�no Aktu�ln� st�ed mapy le�� v z�n� UTM Aktu�ln� sezen� obsahuje rastrov� vrstvy,
nebyla v�ak nalezena knihovna GDAL, aby je bylo mo�no vykreslit. Byla zji�t�na n�sleduj�c� chyba:
 Spojen� tabulka m� %(needed) ��dk� ale mus� m�t %(needed) ��dk�, aby ji bylomo�no pou��t s vybranou vrstvou. Sezen� bylo zm�n�no. P�ejete si jej ulo�it? Toto je grafick� u�ivatelsk� rozhran� generovan� pomoc� knihovny wxPython, kter� slou�� k prohl��en� geografick�ch dat Thuban Thuban - %s Thuban nezn� parametryr
pro aktu�ln� projekci a nem��e
zobrazit konfigura�n� panel.

Nerozpoznan� parametry jsou:

 Thuban je program pro prohl��en� geografick�ch dat.

 Thuban je uvoln�n za podm�nek dann�ch licenc� GNU GPL v>=2 Thuban byl zkompilov�n s wx %(wxproj-wx) ale wxPython je %(wxPython) Thuban: Vnit�n� chyba Titulek: Titulek: Zobrazit/zkr�t legendu P�epnout viditelnost okna se stromem sezen� P�epnout viditelnost vybran� vrstvy Nejvy��� vrstva P�ekladatel:
 Pr�hlednost Transverse Mercator Zkusit Zkou��m na��st ~/.thuban/thubanstart.py. Dv� vrstvy maj� pravd�podobn� stejn� jm�no, zkuste jednu z nich p�ejmenovat. Typ UMN MapServer Mapfiles (*.map)|*.map| URL: Zmazat posledn� operaci spojen� tabulek Undock Rovnom�rn� rozd�len� Jedine�n� hodnoty Typ jednotek Universal Transverse Mercator Nezn�m� Nezn�m� seznam objekt� se jm�nem: '%s' Nezn�m� objekt jm�na: %s Nezn�m� hodnota jm�nem: '%s' a hodnotou '%s' Nezn�m� p��kaz %s Nezn�m� ID p��kazu %d Nezm�no Nepodporovan� typ skupiny v klasifikaci Aktualizovat Pou��t U�ivatel: Hodnota Viditeln� WGS 84 WMS Informace o WMS Varov�n�: %s.%s: %s%s
 Varov�n�: Rastrov� vrstva nebyla zaps�na jako Varov�n� Varov�n� p�i �ten� '%s':

%s Web ���ka:  Zapsat SVG Zapsat SVG Legendu Zapsat SVG mapu Zapsat z�kladn� legendu pro mapu. X:  Y:  Z�na: Z&men�it Zoom na cel� region vrstev Zoom na cel� region mapy Zv�t�en� na v�b�r Prvn� argument mus� b�t absolutn� cesta k souboru gns2shp gns2shp %s gns2shp... Neplatn� index ���ka linie <1 ��dn� p�ij�ma� pro kan�l %s z %s  nebyl definov�n ��dn� Shapefile, mo�n� pou�it objekt '%s'. P�ij�ma� %s%s nen� p�ipojen ke kan�lu %s z %s velikost < 1 nepojmenovan� mapa nepojmenovan� sezen� XML typ pole se neshoduje s datab�zov�m! 