��    `       �        �     �  !   �  
   �     �  	   �     �       	          
   3     >     D     Q     d     {     �  	   �     �     �     �     �     �     �     �     �     �     �     �     �       
             ,     2     ;     B     S     \     l  	   s     }     �     �     �     �  	   �     �  &   �            ,      G      S      d      v      {      �   g   �      !     !  |   ,!     �!  	   �!  (   �!     �!     �!  '   "     6"  %   ="     c"     u"     |"     �"     �"     �"     �"     �"  %   �"     #     #     #  $   '#     L#     Z#     i#     �#     �#     �#     �#     �#     �#     �#     $     $     %$     9$  	   H$     R$     Z$     b$  
   j$     u$     �$     �$     �$     �$     �$     �$     �$  )   �$  
   �$     
%     %     %     2%     7%  
   >%     I%     Z%     j%  	   �%     �%     �%  "   �%  $   �%     �%     &     &     &     &  	   '&  	   1&     ;&     H&     M&     h&     y&     �&     �&     �&     �&  	   �&  
   �&     �&  
   �&     �&  	   �&     	'  
   '  	   '     %'     6'     E'  	   L'     V'  .   ['     �'     �'     �'  *   �'     �'     (     (     (     ((  -   4(     b(  "   t(     �(     �(     �(     �(  $   �(     �(  %   )  	   ()  
   2)     =)     R)     c)     s)     �)     �)  
   �)     �)     �)  
   �)     �)     �)     �)     *     .*     2*     E*     M*     R*     W*  	   `*     j*     r*     �*     �*  N   �*     �*  ?   +  =   A+      +     �+     �+     �+     �+     �+     �+     �+  
   �+     ,     !,     5,  )   J,  	   t,     ~,     �,     �,     �,     �,     �,     �,     �,     �,     -  
   -     '-     3-     P-     X-     m-     s-     {-     �-     �-     �-     �-     �-     �-     �-     �-      .     .     '.     =.     L.     ^.     g.     {.     �.     �.     �.     �.     �.     �.  0   �.     /     )/     ;/     P/  	   n/     x/     �/     �/      �/     �/  
   �/     �/  
   �/  
   �/     �/  
   	0  #   0     80     X0  	   ^0     h0     m0     �0     �0     �0  %   �0     �0     �0  	   �0     1     1     -1     K1     d1     �1     �1  %   �1     �1     �1     �1     �1  #   �1  !   2  .   *2  ^   Y2     �2  h   �2  6   ?3  Q   v3     �3     �3  �   �3  4   n4     �4     �4     �4  .   �4  	   5     5     5     *5     >5     B5     _5     f5     {5     �5     �5  
   �5  (   �5     �5     �5     �5     �5     �5     6     6     $6     D6  	   J6     T6     r6  !   �6  +   �6     �6     �6  !   �6  2   7     M7     Y7  %   i7  )  �7     �8  "   �8  
   �8     �8     9     9     &9     29     >9     J9     Z9  
   `9  	   k9     u9     �9  	   �9     �9     �9     �9     �9     �9     �9     �9     �9     �9     �9      :     	:     :     :     +:  
   <:  	   G:     Q:     ^:     g:  	   s:     }:     �:     �:     �:     �:     �:     �:     �:     �:     �:     ;  &   ;     @;     V;     h;     {;     �;     �;     �;  k   �;     &<     <<  {   P<     �<     �<  8   �<     =     +=  .   D=     s=  ,   |=     �=     �=     �=     �=     �=     >     >     '>  *   5>     `>     h>     x>  +   �>     �>     �>      �>  
   �>      ?     ?     .?     =?     W?     j?     �?     �?     �?     �?     �?  
   �?     �?     �?     �?     �?  
   @     @     ,@     3@     ;@     I@     e@  #   z@  
   �@  
   �@     �@     �@     �@     �@     �@     �@      A     A  
   ,A     7A     FA  %   [A  %   �A     �A     �A     �A     �A  	   �A     �A     �A     �A  	   B     B  
   -B     8B     DB     ZB     hB     {B     �B  
   �B     �B     �B     �B     �B     �B     �B  
   �B     �B     C     &C  	   -C     7C  .   <C     kC     qC     �C  0   �C     �C     �C  
   �C     D     D  !   1D     SD     dD     �D     �D     �D     �D  #   �D     �D  #   �D     E  	   E     E     1E     ?E     PE     fE     {E  
   �E     �E     �E     �E     �E     �E     �E     �E     F     F  	   0F     :F     AF     GF     SF     \F     dF  	   {F     �F  N   �F     �F  -   �F  +    G     LG     lG     �G  
   �G     �G     �G     �G     �G     �G     �G     �G     �G  2   
H     =H     EH     VH     gH     xH  	   ~H     �H     �H     �H     �H  
   �H     �H  	   �H     �H  
   �H     
I     I     %I     *I     7I     NI     WI     hI     vI     �I     �I     �I     �I  '   �I     �I     J     +J     9J     BJ     SJ     dJ     lJ     tJ     |J     �J     �J     �J     �J     �J     �J  +   
K     6K     >K     EK  
   SK  &   ^K     �K  
   �K     �K     �K     �K     �K     �K  )   �K  !   L     =L     EL     IL     UL     iL     L  	   �L  $   �L     �L     �L     �L     �L     �L     M     M     2M     CM     TM  !   [M     }M     �M     �M     �M     �M     �M  4   �M  L   N     SN  v   qN      �N  [   	O     eO     lO  y   xO  :   �O     -P  	   GP  $   QP     vP     �P  	   �P  
   �P     �P     �P  1   �P  	   Q     Q     &Q     :Q     XQ     dQ  &   qQ     �Q     �Q     �Q  	   �Q     �Q     �Q     �Q     �Q     R  	   R     R      <R     ]R  5   }R     �R     �R  !   �R  ,   �R     $S     4S     ES     7       �   �   J   Y   �   �   �               _  `         O   a   �   v   �   1      @      G  �   R    �   T  ;  �   �   K   3  T     �   d   �   Q   0   �   �       *           B  �   $  �   w   �       �   �   �   L   %      �       -   	      ^      ?   N                    W   �          f   |      �   �   V          �   �     %   �   e   F     )           �   P      /   Y  k   �   �   R   �                B                   �   �           3       <  m   V   �   H  C  U   p       �   q   +           i   �   \  �   �          )           �       �   /  �   �   "  �   g       S              �   �       �   �   [  !  #   �   F           ~       �   u   z     "       �       �           �   �   5  I  �   �   �   =   �   D      n        �       �     4             Z   �   W             _   D  �       &    �       =              <   E      2       �   J  �         +  \   �                �   �       �   1  j   �   �   P  �       {       �   5   �   �   H   X   @          
               �       �   �       y   �   :      �   ;   K             �      �       ]          ,      
   �       �   �   `   �       �            .      Z  �   9       �   �   0    h       6          N  L  *          �   &       �   O  �       �   �           �     �          �   M   [   ,       '   ?  Q                       �   7      M  �       �   2        !             A       S  t   �       ^             6   �   �         �            �   :   �   C   I             x       �   �     �   l   }   �       �   �                 �   9        ]   �       4   >   �           (      #      X      �   s   A      o       -          >      �       �   (   U          �   c   �      �       �          �   $   �   r   E  b   G   �   .     	   8   8  �   '  �    	method %s of %s %i rows (%i selected), %i columns %s %s < %s %s (current) &About... &Add Image Layer... &Add Layer... &Close... &Database Connections... &Duplicate &File &Full extent &Full layer extent &Full selection extent &Help &Hide &Identify &Join Table... &Join... &Label &Layer &Lower &Map &New Session &Open Session... &Open... &Pan &Properties... &Raise &Remove Layer &Rename... &Save Session &Show &Show... &Table &Unjoin Table... &Zoom in - not available <None> <Unknown> About Thuban Add Add &Database Layer... Add Database Add Image Layer Add Layer Add Layer from database Add a new database layer to active map Add a new image layer to the map Add a new layer to the map Add to List Add to Selection Add/Remove labels Airy All Files (*.*) All Files (*.*)|*.* An unhandled exception occurred:
%s
(please report to http://thuban.intevation.org/bugtracker.html)

%s Apply to Range Available Projections Based on the data from the table and the input
values, the exact quantiles could not be generated.

Accept a close estimate? Bessel 1841 Blue Ramp Both parameters must have a drive letter Bottom Layer CSV Files (*.csv)|*.csv| Can not propose: No bounding box found. Cancel Cannot import the thubanstart module
 Central Meridian: Change Change Fill Color Change Line Color Choose layer from database Clarke 1866 Clarke 1880 Classification Classification range is not a number! Close Close Table Close Window Close one or more tables from a list Color Scheme: Compiled for:
 Connection '%s' already exists Copy of `%s' Could not read "%s": %s Currently using:
 Custom Ramp DB Connection Parameters DBF Files (*.dbf) DBF Files (*.dbf)|*.dbf| DEFAULT Data Type: %s Database Management Database Name: Databases Decimal Default Degrees Deprecated Developers:
 Dock Duplicate selected layer E&xit E&xport Edit Edit Layer Properties Edit Symbol Edit the properties of the selected layer Ellipsoid: End: Error Error in projection "%s": %s Exit Export Export Map Export Selection Export Table To Export the map to file Export... Extension: %s Extent (lat-lon): Extent (lat-lon): (%g, %g, %g, %g) Extent (projected): (%g, %g, %g, %g) False Easting: False Northing: Field Field: Field:  Field: %s Filename: Filename: %s Fill Finish working with Thuban Fix Border Color French GRS 1980 (IUGG, 1980) Generate Generate Class Generate Classification Generate: Geographic German Green Ramp Green-to-Red Ramp Grey Ramp Hidden Hide Layer Hostname: Hot-to-Cold Ramp Identify Shape Import Import... Info Info about Thuban authors, version and modules Integer International 1909 (Hayford) Invalid color specification %s Invalid hexadecimal color specification %s Italian Join Join Failed Join Layer with Table Join Tables Join and attach a table to the selected layer Join failed:
  %s Join two tables creating a new one Label Label Values Labels Lambert Conic Conformal Latitude of first standard parallel: Latitude of origin: Latitude of second standard parallel: Latitude: Layer '%s' Layer Projection: %s Layer Properties Layer Table: %s Lead Developer:
 Legend Library not available Line Color Line Width:  Line Width: %s Longitude: Lower Layer Lower selected layer Make selected layer unvisible Make selected layer visible Map Map Projection: %s Map: %s Max: Min: Modified Move Down Move Up Multiple Projections selected Name: New No GDAL support because module '%s' cannot be imported. Python exception: '%s' No Projections selected No implementation of get_application_dir available for platform No implementation of relative_filename available for platform No thubanstart module available
 No ~/.thuban directory
 None Normal Number of Classes: Number of Groups: OK Open Session Open Table Open a DBF-table from a file Open a session file Other Contributors:
 Outer Join (preserves left table records) Password: Pick the table to rename: Pick the table to show: Pick the tables to close: Port: Preview: Prin&t Print the map Pro&jection... Problem with Quantiles Proceed Projection Projection: Projection: Propose UTM Zone Propose Quantiles from Table Query Radians Raise Layer Raise selected layer Range Re&name ... Red Ramp Refine Selection Remove Remove Database Connection Remove selected layer Rename Table Rename one or more tables Rename selected layer Rename the map Replace Selection Retrieve Retrieve From Table Retrieve from Table Reverse Revert Russian Save Session &As... Save Session As Save this session to a new file Save this session to the file it was opened from Scale Factor: Select Properties Select an image file Select one or more data files Selection Session Session &Tree Session: %s Set or change the map projection Shapefiles (*.shp) Shapes: %d Shapetype: %s Show EPSG: Show Layer Show Ta&ble Show Table Show one or more tables in a dialog Show the selected layer's table Shown Singleton Sort Source Data is in:  Source of Projection: %s Southern Hemisphere Spanish Specify projection for selected layer Start a new session Start: Stepping: Stop Identify Mode Submenu %s doesn't exist Switch to map-mode 'identify' Switch to map-mode 'pan' Switch to map-mode 'zoom-in' Switch to map-mode 'zoom-out' Symbol Table not compatible with shapestore. Table: Tables Take Text The Dialog named %s is already open The connection %s
is still in use The current map extent center lies in UTM Zone The current session contains Image layers,
but the GDAL library is not available to draw them. The following error occured:
 The joined table has %(joined)d rows but it must have %(needed)d rows to be used with the selected layer The session has been modified. Do you want to save it? This is the wxPython-based Graphical User Interface for exploring geographic data Thuban Thuban - %s Thuban does not know the parameters
for the current projection and cannot
display a configuration panel.

The unidentified set of parameters is:

 Thuban is a program for exploring geographic data.

 Thuban: Internal Error Title:  Toggle Legend on/off Toggle on/off the session tree analysis window Top Layer Translators:
 Transparent Transverse Mercator Try Undo the last join operation Undock Uniform Distribution Unique Values Universal Transverse Mercator Unknown Unmodified Unsupported group type in classification Update User: Value Visible WGS 84 Warning: %s.%s: %s%s
 Warnings Warnings when reading "%s":

%s Zone: Zoom &out Zoom to the full layer extent Zoom to the full map extent Zoom to the full selection extent first argument must be an absolute filename invalid index lineWidth < 1 no receivers for channel %s of %s receiver %s%s is not connected to channel %s of %s unnamed map unnamed session xml field type differs from database! Project-Id-Version: Thuban 1.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-08-18 15:17+0200
PO-Revision-Date: 2003-12-26 12:00+0400
Last-Translator: Alex Shevlakov <alex@motivation.ru>
Language-Team: RU
MIME-Version: 1.0
Content-Type: text/plain; charset=koi8-r
Content-Transfer-Encoding: 8bit
 	����� %s �� %s %i ����� (%i �������), %i �������� %s %s < %s %s (�����.) &� ���������... &�������� �����... &����� ���� &�������... &����� � �� &�������� ����� &���� ���� ����� ���� ���� ��� ������� &������ &�������� ������ &������������ ������� &������������... ����� &���� �������� &����� &����� ������ &�������... &������� &������� &��������... ������� &������ ���� �������������... &��������� &�������� &��������... &������� ����������� ��������� - ��� <���> <�����������> � ��������� Thuban �������� &�������� ���� �� �������� �� �������� ����� �������� ���� �������� ���� �� �� �������� ����� ���� �� �������� ��������� ����������� � ����� �������� ���� � ����� �������� � ������ �������� � ������� ��������/������ ����� Airy ��� ����� (*.*) ��� ����� (*.*)|*.* ��������� ������ � ���������:
%s
(����������, �������� �� http://thuban.intevation.org/bugtracker.html)

%s ��������� � ��������� ��������� ��������: ���������� ��������� ������ ��������,
����������� �� ��������� ������ � ������ �� �������.

������� � �������� �����������? Bessel 1841 ������� ����� ��� ��������� ������ ����� ����� �������� �������� ����� � ����� ��� ����� CSV (*.csv)|*.csv| ���������� ����������: �� ������ Bounding Box. �������� ���������� ������������� ������ thubanstart
 ����������� ��������: �������� �������� ���� �������� �������� ���� ����� ������� ���� �� ���� ������ Clarke 1866 Clarke 1880 ������������� �������� ������������� �� �������� ������! ������� ������� ������� ������� ���� ������� ���� ��� ��������� ������ �� ������ �������� �����: ������������� ��:
 ���������� � '%s' ��� ���������� ����� `%s' ���������� ��������� '%s' : %s ������������:
 �������� ����� ��������� ���������� � �� ����� DBF (*.dbf)  ����� DBF (*.dbf)|*.dbf| �� ����. ��� ������: %s ����������������� �� �������� ��: ���� ������ ���������� �� ��������� ����. �� ������������ ������������:
 ���������� ����������� ��������� ���� &����� ������� ������������� ������������� �������� ���� ������������� ������ ������������� �������� ������� ���� ���������: ���������: ������ ������ �������� '%s': %s ����� ������� ������� ����� ������� ������� ������� ������� �������������� ����� � ���� �������... ����������: %s ������ (���.-����.): ������ (���.-����.): (%g, %g, %g, %g) ������ (� ��������): (%g, %g, %g, %g) ��������� ����������: �������� ����������: ������� �������: �������:  ����: %s ����: ����: %s ��������� ��������� ������ � ���������� ���� ����� ����������� GRS 1980 (IUGG, 1980) ������������� ������������ ����� ������������� ������������: Geographic �������� ������� ����� ������-������� ����� ����� ����� ������� ������ ���� ��� �����: �������/�������� ����� ���������� �� ������� ������ ������... ���� ���������� �� �������, ������ � ������� Thuban ����� International 1909 (Hayford) �������� ������������ ����� %s �������� ����������������� ������������ ����� %s ����������� ������������ ���� ����� ��������� ���� � �������� ������������ ������� ���������� ������� � ������� ���� ���� �����:
  %s ��������� ��� ������� � ���� ����� �������� ����� ����� Lambert Conic Conformal ������ ������ ����������� ��������� �������� ������: ������ ������ ����������� ��������� ������: ���� '%s' �������� ����: %s �������� ���� ������� ����: %s ������� �����������:
 �������� ����������� ���������� ����������� ���� ����� ������ �����:  ������ �����: %s �������: �������� ���� �������� ��������� ���� �� ���������� ���� �������� ��������� ���� ����� �������� �����: %s �����: %s ����.: ���.: ��� ������� �������� ������� ������� ����� �������� ��������: ����� ��� ��������� GDAL, �.�. ������ '%s' ������ �������������. ������ Python: '%s' �������� �� ����������� ��� ��������� �� �������� get_application_dir ��� ��������� �� �������� relative_filename ������ thubanstart �����������
 ��� ���������� ~/.thuban
 ��� ���������� ����� �������: ����� �����: OK ������� ������� ������� ������� DBF-������� �� ����� ������� ���� ������ ����������:
 ������� ����� ������ (��������� ���������� ������) ������: ������� �������: ������� �������: ������� �������: ����: ��������: ������ ���������� ����� &��������... �������� � ���������� ���������� �������� ��������: ��������: ���������� ���� UTM ���������� �������� �� ������� ������ ���. ������� ���� ������� ��������� ���� �������� �������������... ������� ����� �������� ������� ������ ������� ���������� � �� ������ ��������� ���� ������������� ������� ������������� ���� ��� ��������� ������ ������������� ��������� ���� ������������� ����� ����� ������� �������� ����� �� ������� ����� �� ������� ������� ������� ������� ��������� ���... ��������� ��� ��������� � ����� ���� ��������� ������ � ���� ���������� ���������: ������� �������� ������� ����������� ���� ������� ���� ��� ��������� ������ � ������� ������� ������ ������ ������ ������: %s ���������� ��� �������� �������� ����� Shapefiles (*.shp) Shapes: %d Shapetype: %s �������� EPSG: �������� ���� �������� &������� �������� ������� �������� � ���� ���� ��� ��������� ������ �������� ������� ��� ������� ���� ������� ��� ����������� �������� ������ �:  �������� ��������: %s ����� ��������� ��������� ������� �������� ��� ���������� ���� ������ ����� ������ ������: ���: �� ����������� ������ ���� %s �� ���������� ����� �������� ����� �������� �� ����� ����� ���������� ����� ���������� ������ ������� ������������ � shapestore �������: &������� ������� ����� ������ %s ��� ������ ���������� %s
��� ������������ ����� ������������ ������ ����� ��������� � UTM Zone ������ ������ �������� ����������� ����,
�� ��� ����������� ���������� GDAL. ��������� ��������� ������: 
 �������������� ������� ����� %(joined)d �����, �� ������ ���� %(needed)d ��� ����������� ������������� ���������� ���� ������ ���� ��������. ���������? ����������� ��������� ������������ �� ������ wxPython ��� ������ � ��������������� �������  Thuban Thuban - %s Thuban �� ����� ���������� ��� ������ ��������
� �� ����� ��������
������ 
������������.

����������� ����� ����������:

 Thuban: ��������� ��� ������ � ��������������� �������. 

 Thuban: ���������� ������ ��������: ��������/������ �������� ����������� ��������/������ ������ ������ �� ����� ���� �������:
 ���������� Transverse Mercator ����� �������� ��������� �������� ������������� ������� ��������� ���������� ������������� ���������� �������� Universal Transverse Mercator ����������� �� ��������� ��� ������ ����������� � ������������� �������� ������������: �������� ��������� WGS 84 ��������������: %s.%s: %s%s
 �������������� �������������� '%s':

%s ����: ��������� ���������� ���� �� ����� ���� ���������� ���� �� ����� ������� ���������� ���� �� ���� ������� ������ ���������� ������ ���� ���������� ���� � ����� ������������ ������ lineWidth < 1 ��� ��������� ��� ������ %s �� %s �������� %s%s �� �������� � ������� %s �� %s ����� ��� ����. ������ ��� ����. ��� ���� xml ���������� �� ��!  