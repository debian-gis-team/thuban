��    �     $  	  ,      P      Q      d   !   u   
   �      �   	   �      �      �      �   	   �      �   
   !     !     !     $!     7!     N!     T!  	   Z!     d!     s!     |!     �!     �!     �!     �!     �!     �!     �!     �!     �!     �!  
   �!     �!     �!     "     "     "     "     +"     4"     ="     M"  	   T"     ^"     k"     o"     �"     �"  	   �"     �"  &   �"      �"     #     (#     4#     E#     W#     \#     l#  g   �#     �#  	   �#  	   $     $  |   !$     �$  	   �$  (   �$     �$     �$  '   %  "   +%     N%     h%  %   o%     �%     �%     �%     �%     �%     �%     �%     &  %   &     :&     @&     L&  $   Y&     ~&     �&     �&     �&     �&     �&     �&     �&     '     $'     6'     O'     W'     e'     y'  	   �'     �'     �'     �'  
   �'  '   �'     �'     �'     �'     (     (     (     (     1(  )   =(  
   g(     r(     w(     }(     �(     �(     �(  
   �(     �(     �(     �(  	   �(     )     )     )  "   0)  $   S)     x)     �)     �)     �)     �)  	   �)  	   �)     �)     �)     �)     �)     �)     *     *     $*     3*  	   K*  
   U*     `*     p*  
   w*     �*  	   �*     �*  
   �*  	   �*     �*  	   �*     �*     �*  	   �*     �*  .   �*     )+     1+     N+  *   m+     �+     �+     �+     �+     �+  -   �+     ,  "   ,     6,     <,     I,     P,  $   h,     �,  %   �,  	   �,  
   �,     �,     �,     -     -     -     0-     7-  
   M-     X-     e-  
   t-     -     �-     �-     �-     �-     �-  
   �-     �-     .     	.     .  	   .     !.     ).     G.     M.  N   Q.     �.  ?   �.  =   �.      6/     W/     o/     t/     {/     �/     �/     �/  
   �/     �/     �/     �/  )   0  	   +0     50     O0     g0     �0     �0     �0     �0     �0     �0     �0     �0  
   �0     �0     1     1      &1     G1     e1     z1  �   �1     e2     m2     y2     �2     �2     �2     �2     �2     �2     �2     �2  
   �2     
3     3     13     G3     V3     h3     q3     �3     �3     �3     �3     �3     �3     �3  0   �3     %4     34     @4     R4     g4  	   �4  	   �4     �4     �4     �4      �4     �4  
   �4     �4  
   5  
   5     5  
   *5  #   55     Y5     y5  	   5     �5     �5     �5     �5     �5     �5  %   �5     6     6  	   6     )6     <6     U6     s6     �6     �6     �6     �6  %   �6     7  	   7     7     7     7  #   #7  !   G7  .   i7  ^   �7     �7  h   8  6   ~8  Q   �8     9     9  �   9  4   �9  F   �9     ):     @:     H:  .   ]:  #   �:  	   �:     �:     �:     �:     �:     �:     	;     ;     %;     3;     Q;     Y;     l;  
   �;  (   �;     �;     �;     �;     �;     �;     �;     �;     �;     �;     <  	   !<     +<     I<  !   e<  +   �<     �<     �<  !   �<  2   �<     $=     -=     9=  %   I=  a  o=     �>     �>     �>  
   ?     '?     7?     C?     [?     m?  	   s?     }?  	   �?     �?     �?     �?     �?     �?  	   �?  	    @     
@     #@     3@     :@     A@     J@     R@     d@     �@     �@     �@  
   �@     �@     �@     �@     �@     �@  	   A  	   A     A     1A  	   :A     DA     SA     [A     hA     xA     �A     �A     �A     �A     �A  /   �A  #   'B     KB     hB     }B     �B     �B     �B     �B  k   �B     OC  	   cC     mC     vC  |   �C     D  
   D  <   D     ZD     hD  ,   �D  /   �D  $   �D     E  $   E     0E     DE     LE     dE     �E     �E     �E     �E  #   �E     �E     �E     �E  '   F  	   5F     ?F     _F     |F     �F     �F     �F     �F      �F     �F     G     G     -G     ;G     MG     ]G  	   iG     sG     �G     �G  )   �G     �G     �G     �G     �G     �G     H  #   H     8H  +   MH     yH     �H     �H     �H     �H     �H  
   �H     �H     �H     �H     I     *I     8I     II  "   ZI  2   }I  '   �I  '   �I  '    J     (J     -J     3J     9J     BJ     KJ     WJ     ^J     |J     �J     �J  
   �J     �J     �J  
   �J  	   �J     �J     	K     K     K     -K     ;K     CK     SK     ZK     mK     ~K  
   �K     �K  
   �K  :   �K  
   �K     �K     L  +   7L     cL     iL     xL     �L     �L  6   �L  !   M  +   $M     PM     VM     dM     kM  '   �M  
   �M  )   �M  
   �M  
   �M     �M     
N     N     .N     :N     MN     [N  	   sN     }N     �N  
   �N     �N     �N     �N     �N     O     O     O  
   ,O     7O     @O  	   IO     SO     _O     lO     �O     �O  R   �O     �O  /   P  -   3P  !   aP     �P     �P     �P     �P     �P     �P     �P     �P     �P     Q     7Q  2   LQ     Q     �Q  "   �Q     �Q     �Q     �Q  	    R     
R     R     %R     .R     HR     OR     WR     `R     {R     �R     �R     �R  
   �R  �   �R     �S     �S     �S  	   T  
   T     T     #T     8T      >T     _T     {T     �T     �T     �T     �T     �T     �T     U     U     +U     <U     CU     QU     WU     vU     �U  C   �U     �U     V     V     4V  $   IV     nV  	   {V     �V     �V     �V     �V     �V     �V     �V     W     W     1W     >W     JW     jW     �W  	   �W     �W     �W     �W     �W     �W     �W  ,   �W     X     +X     3X     :X     RX     oX     �X     �X     �X  	   �X     �X  )   �X     �X  	   Y     Y     Y     Y      Y  $   :Y  8   _Y  b   �Y     �Y  p   Z  *   �Z  Q   �Z     [     [  �   [  8   �[  L   �[     0\     C\  !   I\  @   k\  +   �\     �\  
   �\     �\     �\     ]  .   ]     D]     K]     _]     n]  
   �]     �]     �]     �]  +   �]  	   �]     ^     ^     ^     #^     +^     2^     O^     `^     ~^     �^  &   �^  '   �^  +   �^  ,   _     9_     K_  &   ^_  8   �_  	   �_     �_     �_  +   �_               +   /  �   �   6   ^   �   s   {  o     �   �       �          �   �   &  �   u       g   �   p  2  J  e   a  ^  �   %   C  �      �           �   x  H    �   k  M   �   w  �       �   	  6  j   �           �   �   )          �   R       z  �             �   G   �   �   �   [      &           %  �   D   f  	   �          `   )   
             �   `  y       W       �   /       �         b       ?     �   �   4  N      �   9       [   �   �   i  �   B   $      F           $      �                  b         �   (          x   �   @       �   Y           �   �   �           �   ,  |             t  {   �   �  �   3  �   f   �          �       ;   �       �           +      !      T    �       �       d  Q   �             !      q   a   =   �          m     �      :  2       "  ]  j  ]   �         r     Q  B  �   X       �   �         �   P      R  >  �       h               �   �     -   (      �   >   }       l      E  �           -     h  V  �   �   <  r   �     9  �   �   =  �   *             �   P  �   #      ~       S      �   �   �       �   n  �   S   g  q          �       "              �   �   K         L  �   _      �   �   �   i         T      �         0  Y  4   *   Z             '   d      �   �   �   8                              E   .  �   �   �       C     O      A   v   �   �          N   �   O   U     z     �           5   �   A  �           V   �       n          �   I   U    ~    J   �           <   �       ,      ?  F          �   W  �       _       t       o   1           �   Z   H   �   �   k   l   �       8       s  w     �   �               �   3   y  �             D      �   ;  '          \  }      v  p   �          @  �   7       M     G  �       u  �   c          #           e  �   1  I          0       m   L       \   c     |  �   �       
       7  X      �   5            K       �   :   �         �   �       .       �    	None registered.
 	method %s of %s %i rows (%i selected), %i columns %s %s < %s %s (current) &About... &Add Image Layer... &Add Layer... &Bottom &Close... &Database Connections... &Duplicate &File &Full extent &Full layer extent &Full selection extent &Help &Hide &Identify &Join Table... &Join... &Label &Layer &Lower &Map &New Session &Open Session... &Open... &Pan &Properties... &Raise &Remove Layer &Rename... &Save Session &Show &Show... &Table &Top &Unjoin Table... &Visible &Zoom in - not available <None> <Unknown> About Thuban Add Add &Database Layer... Add Database Add Image Layer Add Layer Add Layer from database Add a new database layer to active map Add a new image layer to the map Add a new layer to the map Add to List Add to Selection Add/Remove labels Airy All Files (*.*) All Files (*.*)|*.* An unhandled exception occurred:
%s
(please report to http://thuban.intevation.org/bugtracker.html)

%s Apply to Range Authors:
 Available Available Projections Based on the data from the table and the input
values, the exact quantiles could not be generated.

Accept a close estimate? Bessel 1841 Blue Ramp Both parameters must have a drive letter Bottom Layer CSV Files (*.csv)|*.csv| Can not propose: No bounding box found. Can't open the database table '%s' Can't open the file '%s'. Cancel Cannot import the thubanstart module
 Central Meridian: Change Change Fill Color Change Line Color Choose layer from database Clarke 1866 Clarke 1880 Classification Classification range is not a number! Close Close Table Close Window Close one or more tables from a list Color Scheme: Compiled for:
 Connection '%s' already exists Copy of `%s' Copyright %s
 Could not read "%s": %s Currently using:
 Custom Ramp DB Connection Parameters DBF Files (*.dbf) DBF Files (*.dbf)|*.dbf| DEFAULT Data Type: %s Database Management Database Name: Databases Decimal Default Degrees Deprecated Details on the registered extensions:

 Developers:
 Dock Duplicate selected layer E&xit E&xport Edit Edit Layer Properties Edit Symbol Edit the properties of the selected layer Ellipsoid: End: Error Error in projection "%s": %s Exit Exit Thuban now Export Export Map Export Selection Export Table To Export the map to file Export... Extension: %s Extensions:
 Extent (lat-lon): Extent (lat-lon): (%g, %g, %g, %g) Extent (projected): (%g, %g, %g, %g) False Easting: False Northing: Field Field: Field:  Field: %s Filename: Filename: %s Fill Finish working with Thuban Fix Border Color French GRS 1980 (IUGG, 1980) Generate Generate Class Generate Classification Generate: Geographic Geometry Column German Green Ramp Green-to-Red Ramp Grey Ramp Hidden Hide Layer Hostname: Hot-to-Cold Ramp ID Column Identify Shape Import Import... Info Info about Thuban authors, version and modules Integer International 1909 (Hayford) Invalid color specification %s Invalid hexadecimal color specification %s Italian Join Join Failed Join Layer with Table Join Tables Join and attach a table to the selected layer Join failed:
  %s Join two tables creating a new one Label Label Values Labels Lambert Conic Conformal Latitude of first standard parallel: Latitude of origin: Latitude of second standard parallel: Latitude: Layer '%s' Layer Projection: %s Layer Properties Layer Table: %s Layer Title: Lead Developer:
 Legend Library not available Line Color Line Width:  Line Width: %s Longitude: Lower Layer Lower selected layer Make selected layer unvisible Make selected layer visible Map Map Projection: %s Map Title: Map: %s Max: Min: Modified Move Down Move Up Multiple Projections selected Name: New No GDAL support because module '%s' cannot be imported. Python exception: '%s' No Projections selected No implementation of get_application_dir available for platform No implementation of relative_filename available for platform No thubanstart module available
 No ~/.thuban directory
 None Normal Number of Classes: Number of Groups: OK Open Session Open Table Open a DBF-table from a file Open a session file Other Contributors:
 Outer Join (preserves left table records) Password: Pick the table to rename: Pick the table to show: Pick the tables to close: Port: Portuguese (Brazilian) Preview: Prin&t Print the map Pro&jection... Problem with Quantiles Proceed Projection Projection: Projection: Propose UTM Zone Propose Put selected layer to the bottom Put selected layer to the top Quantiles from Table Query Questions and comments can be sent to the following addresses:
	General list (public):
		<thuban-list@intevation.de>
	Developers list (public):
		<thuban-devel@intevation.de>
	Thuban team at Intevation:
		<thuban@intevation.de>
 Radians Raise Layer Raise selected layer Range Re&name ... Red Ramp Refine Selection Remove Remove Database Connection Remove selected layer Rename Layer Rename Map Rename Table Rename one or more tables Rename selected layer Rename the map Replace Selection Retrieve Retrieve From Table Retrieve from Table Reverse Revert Russian Save Session &As... Save Session As Save this session to a new file Save this session to the file it was opened from Scale Factor: Select Color Select Properties Select an image file Select one or more data files Select... Selection Session Session &Tree Session: %s Set or change the map projection Shapefiles (*.shp) Shapes: %d Shapetype: %s Show EPSG: Show Layer Show Ta&ble Show Table Show one or more tables in a dialog Show the selected layer's table Shown Singleton Size:  Sort Source Data is in:  Source of Projection: %s Southern Hemisphere Spanish Specify projection for selected layer Start a new session Start: Stepping: Stop Identify Mode Submenu %s doesn't exist Switch to map-mode 'identify' Switch to map-mode 'pan' Switch to map-mode 'zoom-in' Switch to map-mode 'zoom-out' Symbol Table Title: Table not compatible with shapestore. Table: Table: %s Tables Take Text The Dialog named %s is already open The connection %s
is still in use The current map extent center lies in UTM Zone The current session contains Image layers,
but the GDAL library is not available to draw them. The following error occured:
 The joined table has %(joined)d rows but it must have %(needed)d rows to be used with the selected layer The session has been modified. Do you want to save it? This is the wxPython-based Graphical User Interface for exploring geographic data Thuban Thuban - %s Thuban does not know the parameters
for the current projection and cannot
display a configuration panel.

The unidentified set of parameters is:

 Thuban is a program for exploring geographic data.

 Thuban was compiled with wx %(wxproj-wx)s but wxPython is %(wxPython)s Thuban: Internal Error Title:  Toggle Legend on/off Toggle on/off the session tree analysis window Toggle visibility of selected layer Top Layer Translators:
 Transparent Transverse Mercator Try Undo the last join operation Undock Uniform Distribution Unique Values Universal Transverse Mercator Unknown Unknown command %s Unknown command ID %d Unmodified Unsupported group type in classification Update Use User: Value Visible WGS 84 Warning: %s.%s: %s%s
 Warnings Warnings when reading "%s":

%s Zone: Zoom &out Zoom to the full layer extent Zoom to the full map extent Zoom to the full selection extent first argument must be an absolute filename invalid index lineWidth < 1 no receivers for channel %s of %s receiver %s%s is not connected to channel %s of %s size < 1 unnamed map unnamed session xml field type differs from database! Project-Id-Version: Thuban 1.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-08-18 15:17+0200
PO-Revision-Date: 2004-11-09 11:31+0100
Last-Translator: Solymosi Norbert <Solymosi.Norbert@aotk.szie.hu>
Language-Team: hungarian <Solymosi.Norbert@aotk.szie.hu>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-2
Content-Transfer-Encoding: 8bit
 	Nincs regisztr�lva.
 	elj�r�s %s ehhez %s %i sor (%i kijel�lt), %i oszlop %s %s < %s %s (jelenlegi)  &N�vjegy... K�p-r�teg hozz�ad�sa... &R�teg hozz�ad�sa &Alap &Bez�r... A&datb�zis kapcsolatok &Kett�z�s &F�jl &Teljes terjedelem &R�teg teljes terjedelem &Kijel�l�s teljes terjedelemben &S�g� El&rejt�s &Azonos�t T�bla �ssz&ekapcsol�s... &Kapcsol�d�s... &C�mke &R�teg &Lejjebb &T�rk�p �j mu&nkafolyamat Munkaf&olyamat megnyit�sa... &Megnyit... &Mozgat &Tulajdons�gok... Fel&emel�s &R�teg elt�vol�t�sa &�tnevez... Munkafolyamat ment�&se Meg&jelen�t�s &Megjelen�t... &T�bl�zat &Legfels� T�bla le&v�laszt�sa... &L�that� &Nagy�t�s - nem el�rhet� <semmi> <ismeretlen> Thuban n�vjegye Hozz�ad Adatb�zis-r�teg hozz�ad�sa... Adatb�zis hozz�ad�sa K�p-r�teg hozz�ad�sa R�teg hozz�ad�sa R�teg hozz�ad�sa adatb�zisb�l Az akt�v t�rk�phez �j adatb�zis-rteg hozz�ad�sa �j k�p-r�teg hozz�ad�sa a t�rk�phez R�teg hozz�ad�sa a t�rk�phez Hozz�ad�s a list�hoz Hozz�ad�s a kijel�l�shez C�mke hozz�ad�sa/elt�vol�t�sa Airy Minden f�jlt (*.*) Minden f�jlt (*.*)|*.* Nem kezelt kiv�tel jelentkezett:
%s
(l�gyszi k�ldjed ide: http://thuban.intevation.org/bugtracker.html)

%s Tartom�nyhoz igaz�t Szerz�k:
 El�rhet� El�rhet� vet�letek Az egzakt kvantilisek nem gener�lhat�k a
t�bl�zatb�l sz�rmaz� �s beolvasott adatokra alapozva.

Elfogadsz egy j� k�zel�t�st? Bessel 1841 K�k k�sz�b Mindk�t param�ternek egy meghajt� bet�t kell tartalmaznia??? Legals� r�teg CSV-f�jlok (*.csv)|*.csv| Nincs aj�nlat: Nem tal�lhat� hat�rol� doboz. A(z) '%s' adatb�zis-t�bl�t nem lehet megnyitni. Nem tudom megnyitni a(z) '%s' f�jlt. M�gsem A thubanstart modul nem beolvashat�
 Centralis Meridi�n: M�dos�t Kit�lt� sz�n be�ll�t�sa Vonal sz�n�nek megv�ltoztat�sa R�teg v�laszt�sa adatb�zisb�l Clarke 1866 Clarke 1880 Oszt�lyoz�s Az oszt�lyoz�si tartom�ny nem sz�m! Bez�r T�bla bez�r�sa Ablak bez�r�sa Egy vagy t�bb t�bla bez�r�sa a list�b�l Sz�ns�ma: A ford�t�shoz alapul szolg�lt:
 A '%s' kapcsolat m�r l�tezik `%s' m�sol�sa Copyright: %s
 Olvashatalan "%s": %s Jelenleg haszn�lt:
 Egy�ni k�sz�b Adatb�zis-kapcsolati param�terek DBF-f�jlok(*.dbf) DBF-f�jlok(*.dbf)|*.dbf| ALAP�RTELMEZETT Adatt�pus: %s Adatb�zis-kezel�s Adatb�zis neve: Adatb�zisok Decim�lis Alap�rtelmezett Fok �rv�nytelen�tett A regisztr�lt kiterjeszt�sek r�szletei:

 Fejleszt�k:
 Dokkol Kijel�lt r�teg megkett�z�se &Kil�p E&xport�l�s... Szerkeszt�s R�teg tulajdons�gainak szerkeszt�se Szimb�lum be�ll�t�sa A kijel�lt r�teg jellemz�inek szerkeszt�se. Ellipszoid: V�g: Hiba Hiba a vet�letben "%s": %s Kil�p Kil�p�s a Thubanb�l most Export�l�s T�rk�p export�l�sa Kijel�ltek export�l�sa T�bla export�l�sa T�rk�p export�l�sa f�jlba Export�l�s... Kiterjeszt�s: %s Kiterjeszt�sek:
 Kiterjed�s (sz�less�g-hossz�s�g):  Kiterjed�s (sz�less�g-hossz�s�g): (%g, %g, %g, %g) Kiterjed�s (vet�leti): (%g, %g, %g, %g) A k�z�ppont keleti ir�ny� koordin�t�ja: A k�z�ppont �szaki ir�ny� koordin�t�ja: Mez� Mez�: Mez�: Mez�: %s F�jln�v: F�jln�v: %s Kit�lt A Thuban munk�nak befelyez�se Hat�rsz�n r�gz�t�se Francia GRS 1980 (IUGG, 1980) L�trehoz�s Oszt�lygener�l�s Oszt�lyoz�s gener�l�sa Gener�l�s: F�ldrajzi Geometrikus oszlop N�met Z�ld k�sz�b Z�ld-V�r�s k�sz�b Sz�rke k�sz�b Rejtett R�teg elrejt�se Gazda: Forr�-Hideg k�sz�b Azonos�t� oszlop Alakzat azonos�t�sa Import�l�s Import�l�s... Inform�ci� Inform�ci� a Thuban alkot�ir�l, verzi�j�r�l �s moduljair�l Eg�sz sz�m International 1909 (Hayford) Hib�s sz�nmeghat�roz�s: '%s'. Hib�s hexadecim�lis sz�nmeghat�roz�s: '%s'. Olasz �sszekapcsol�s Az �sszekapcsol�s sikertelen R�teg �sszekapcsol�sa t�bl�val T�bl�k �sszekapcsol�sa A kiv�lasztott r�teghez t�bla illeszt�se �s kapcsol�sa Az �sszekapcsol�s sikertelen:
 %s K�t �sszekapcsolt t�bla egy �jat eredm�nyez C�mke C�mke �rt�kek C�mk�k Lambert f�le k�pszeletes Az els� standard p�rhuzamos sz�less�ge: Sz�less�g: A m�sodik standard p�rhuzamos sz�less�ge: Sz�less�g: R�teg '%s' R�teg vet�lete: %s R�teg-tulajdons�gok R�teg-t�bla: %s R�teg c�me: Vezet� fejleszt�:
 Jelmagyar�zat A k�nyvt�r nem l�tezik. Vonalsz�n Vonalvastags�g: Vonalsz�less�g: %s Hossz�s�g: R�teg lejjebb Kije�lt r�teg lejjedd Kiv�lasztott r�teg elfed�se A kijel�lt r�teg l�that� T�rk�p T�rk�p vet�lete: %s T�rk�p c�me: T�rk�p: %s Maximum: Minimum: M�dos�tva Mozgat�s le Mozgat�s fel T�bb vet�lett lett kiv�lasztva N�v: �j Nincs GDAL t�mogat�s, mert a '%s' modult nem lehet bet�lteni. Python kiv�tel: '%s' Nem lett kiv�lasztva vet�let A platformra nem el�rhet� a get_application_dir A platformra nem el�rhet� a relative_filename Nincs el�rhet� thubanstart modul
 Nincs ~/.thuban k�nyvt�r
 Nincs Norm�l Oszt�lyok sz�ma: Csoportok sz�ma: OK Munkafolyamat megnyit�sa T�bla megnyit�sa Egy DBF-f�jl megnyit�sa f�jlb�l Munkafolyamat-f�jl megnyit�sa Egy�b k�zrem�k�d�k:
 K�ls� illeszt�s (a bal t�bla rekordjait megtartja) Jelsz�: T�bla kiv�laszt�sa �tnevez�sre: T�bla kiv�laszt�sa megjelen�t�sre: T�bla kiv�laszt�sa bez�r�sra: Kapu: Portug�l (Brazil) El�n�zet: &Nyomtat T�rk�p nyomtat�sa &Vet�let Probl�ma a kvantilisekkel Tov�bb Vet�let Vet�let: Vet�let: javasolt UTM Z�na Aj�nlat Kijel�lt r�teget a legalulra Kijel�lt r�teget a tetej�re Kvantilisek a t�bl�zatb�l Lek�rdez�s A felmer�l� k�rd�seket �s �szrev�teleket a k�vetkez� c�mre v�rjuk:
	�ltal�nos lista (nyilv�nos):
		<thuban-list@intevation.de>
	Fejleszt� lista (nyilv�nos):
		<thuban-devel@intevation.de>
	Intevation Thuban csoportja:
		<thuban@intevation.de>
 Radi�n R�teg feljebb Kijel�lt r�teg feljebb Tartom�ny �t&nevez�s V�r�s k�sz�b Kijel�l�s finom�t�sa T�r�l Adatb�zis-kapcsolat elt�vol�t�sa Kijel�lt r�teg elt�vol�t�sa R�teg �tnevez�se T�rk�p �tnevez�se T�bla �tnevez�se Egy vagy t�bb t�bla �tnevez�se Kijel�lt r�teg �tnevez�se T�rk�p �tnevez�se Kijel�l�s megsz�ntet�se Kigy�jt Kigy�jt t�bl�b�l Kigy�jt t�bl�b�l Vissza Vissza�ll�t�s Orosz Munk&afolyamat ment�se m�sk�nt Munkafolyamat ment�se m�sk�nt Munkafolyamat ment�se �j f�jlba Mentsed ezt a munkafolyamatot abba a f�jlba, amib�l meg lett nyitva Sk�la t�nyez�: Sz�n kiv�laszt�sa Tulajdons�gok kiv�laszt�sa K�pf�jl kiv�laszt�sa V�lasszon ki egy vagy t�bb adatf�jlt Kiv�laszt... Kijel�l�s Munkafolyamat Munkafolyamat &t�rt�nete Munkafolyamat: %s A t�rk�p vet�let�nek be�ll�t�sa Shape-f�jlok (*.shp) Alakzat: %d Alakzat-t�pus: %s EPSG mutat: R�teg megjelen�t�se T�&bla mutat T�bla mutat Egy vagy t�bb t�bla mutat�sa... A kijel�lt r�teg t�bl�j�t mutat Megjelen�tett Singleton M�ret: Rendez A forr�sadat: Vet�let forr�sa: %s D�li f�lteke Spanyol Hat�rozza meg a kiv�lasztott r�teg vet�let�t �j munkafolyamat indul Kezdet: L�p�s: Azonos�t� m�dot befejez A(z) '%s' almen� nem l�tezik Azonos�t�sra v�lt�s Mozgat�sra v�lt�s Nagy�t�sra v�lt�s Kicsiny�t�sre v�lt�s Szimb�lum T�bla c�me: A t�bla nem kompatibilis a shapesztorral. T�bla: T�bla: %s T�bl�k Vidd Sz�veg A %s ablak m�r nyitva van A kapcsolat %s
m�g haszn�latban van. Az aktu�lis t�rk�p kiterjed�s�nek k�zepe UTM Z�n�ba esik A jelenlegi munkafolyamat tartalmaz k�p-r�teget,
de GDAL k�nyvt�r nem el�rhet� a megjelen�t�s�hez. A k�vetkez� hiba t�rt�nt:
 Az kapcsolt t�bl�nak %(joined)d sora van, de %(needed)d sorra van sz�ks�g a kijel�lt r�teggel val� haszn�lat�hoz A munkafolymat neve megv�ltozott. Ments�k? Ez  a wxPython alap� grafikus felhaszn�l�i fel�let a f�ldrajzi adatok elemz�s�hez Thuban Thuban - %s A Thuban nem ismeri a jelenlegi vet�let
param�tereit �s nem tudja megjelen�teni
a konfigur�ci�s panelt.

A param�terek nem azonos�tott k�szlete:

 A Thuban f�ldrajzi adatok elemz�s�t szolg�l� szoftver.

 Thuban ford�t�s�hoz wx %(wxproj-wx)s haszn�ltak, m�g a wxPython %(wxPython)s Thuban: Bels� Hiba C�m:  Seg�dsz�vegek ki- �s bekapcsol�sa A munkafolyamat-f�j�nak elmz�s�re szolg�l� ablak ki/bekapcsol�ja A kijel�lt r�teg l�that�s�g�nak kapcsol�ja? Legfels� r�teg Ford�t�k:
 �tl�tsz� Transverse Mercator Pr�ba Az utols� �sszekapcsol�si m�velet visszavon�sa Felold Egyenletes eloszl�s Egyedi �rt�kek Universal Transverse Mercator Ismeretlen Ismeretlen parancs %s Ismeretlen parancs ID %d V�ltozatlan Oszt�lyoz�sban nem t�mogatott csoport t�pus Friss�t�s Haszn�l Felhaszn�l�: �rt�k L�that� WGS 84 Figyelmeztet�s: %s.%s: %s%s
 Figyelmeztet�sek F�jlbeolvas�si hiba "%s":

%s Z�na: &Kicsiny�t�s M�retez�s a r�teg teljes kiterjed�s�re M�retez�s a t�rk�p teljes kiterjed�s�re M�retez�s a kijel�ltek teljes kiterjed�s�re az els� r�sznek teljes f�jln�nek kell lennie �rv�nytelen index vonalvastags�g < 1 nincsenek vev�k a csatorn�hoz %s of %s a vev� %s%s nem kapcsol�dott a csatorn�j�hoz %s ennek %s m�ret < 1 elnevezend� t�rk�p elnevezend� munkafolyamat xml mez� t�pusa k�l�nb�zik az adatb�zist�l! 