��    �     �  -  �      �"     �"     �"     �"  !   �"  
   �"     �"  	   �"     #     #     ##  	   +#     5#  
   N#     Y#     _#     l#     #     �#     �#  	   �#     �#     �#     �#     �#     �#     �#     �#     �#     �#     $     
$     $      $  
   .$     9$     G$     M$     V$     ]$     b$     s$     |$     �$     �$  	   �$     �$     �$     �$     �$     �$  	   �$     �$  &   %      4%     U%     p%     |%     �%     �%     �%     �%     �%  g   �%     A&  	   P&  	   Z&     d&  |   z&     �&  	   '  (   '     6'     C'  '   \'  "   �'     �'     �'  %   �'     �'      (     (     (     +(     F(     R(     ^(  %   m(     �(     �(     �(  $   �(     �(     �(     �(     )      )     .)     F)     X)     d)     })     �)     �)     �)     �)     �)  	   �)     �)     �)     �)  
   *  '   *     6*     C*  
   H*     S*     l*     r*     z*     *     �*  )   �*  
   �*     �*     �*     �*     �*     +     +  
   +     %+     6+     F+  	   ]+     g+     u+     �+  "   �+     �+  $   �+     �+     ,     ,     ,     ,  	   ',  	   1,     ;,     H,     M,     h,  j   y,     �,  >   �,     *-     @-     I-     X-  	   p-  
   z-     �-     �-  
   �-     �-  	   �-     �-  
   �-  	   �-     �-  	   �-  	   �-     .     .     $.  	   +.     5.  .   :.  !   i.     �.     �.     �.     �.  *   �.     /     /     "/     ./     D/  -   P/     ~/  "   �/     �/     �/     �/     �/     �/  $   �/     0  %   .0  	   T0  
   ^0     i0     ~0     �0     �0     �0     �0     �0     �0  
   �0     �0     1  
   1     1     '1     <1     J1     h1     �1     �1  
   �1     �1     �1     �1     �1  	   �1     �1     �1     �1     �1  N   �1     J2  ?   b2  =   �2      �2     3     3     3     %3     93     L3     ^3     s3     v3     3  
   �3     �3     �3     �3  )   �3  	   4     4     4     34     K4     e4     k4     �4     �4     �4     �4     �4     �4  
   �4     �4     �4     �4     5     "5      *5     K5     i5     ~5  �   �5     i6     q6     }6     �6     �6     �6     �6     �6     �6     �6     �6  
   7     7     7     57     K7     Z7     l7     u7     �7     �7     �7     �7     �7     �7     �7  0   �7     )8     78     D8  &   V8     }8  A   �8     �8  	   �8  	   �8     9     9     9      (9     I9  
   \9     g9  
   u9  
   �9     �9  
   �9  #   �9     �9     �9  	   �9     �9     �9     	:     :     :     +:  
   D:     O:     c:  %   k:     �:     �:  	   �:     �:     �:     �:      ;     ;     6;     T;     [;  %   h;     �;  	   �;     �;     �;     �;  $   �;  #   �;  !   �;  .   <  ^   J<     �<  h   �<  6   0=  Q   g=     �=     �=  �   �=  4   _>  )   �>  F   �>     ?     ?     $?  .   9?  #   h?  	   �?     �?     �?     �?     �?     �?     �?     �?     @     @     -@     5@     H@  
   ^@  (   i@     �@     �@     �@     �@     �@     �@     �@     �@     �@     �@  	   �@     A     %A  !   AA  +   cA     �A     �A  !   �A  2   �A      B     	B     B  %   %B  N  KB     �C     �C     �C  *   �C  
   	D     D      D     1D     ID  	   ZD  
   dD     oD  	   �D     �D     �D     �D     �D     �D     �D     �D     	E     E  	   !E     +E     1E     8E     >E     ME  	   _E  
   iE     tE     �E     �E     �E     �E     �E     �E     �E  	   �E     �E     �E      F     F  	   F     !F     /F     @F  !   HF     jF     �F     �F  $   �F  0   �F     �F     G     3G     IG     aG     |G     �G     �G     �G  �   �G     @H  	   QH  
   [H     fH  |   H     �H  
   I  4   I     HI     VI  4   rI     �I      �I     �I  5   �I     &J     9J     AJ     ZJ  #   rJ     �J     �J     �J  1   �J     �J     �J     K  .   K     AK     UK     fK     �K     �K     �K     �K     �K  (   �K     L     L     3L     @L     RL     mL     �L     �L     �L     �L     �L  *   �L     �L  
   �L  
   M     M     1M  	   8M     BM     IM     gM  .   wM  
   �M     �M     �M      �M     �M     �M     �M     N     N     'N     6N     TN     `N     oN     }N  %   �N     �N  (   �N     �N     
O     O     O     %O  	   -O     7O     @O     LO     TO     sO  k   �O     �O  S   �O     RP     hP     pP     P     �P     �P     �P     �P     �P     �P  
   �P     �P     �P     Q     Q     /Q  
   8Q     CQ     OQ     hQ     qQ     }Q  =   �Q  ,   �Q     �Q     R     R  %   2R  1   XR     �R     �R     �R     �R     �R  /   �R     �R  "   S     3S     <S     PS  	   dS     nS  %   �S     �S  &   �S     �S  	   �S     �S     T  	   .T     8T     LT     ]T     uT     }T     �T     �T     �T  	   �T  
   �T  #   �T     U     $U  "   AU     dU     iU     �U     �U     �U     �U  
   �U     �U     �U  &   �U     �U     �U  h   �U     ^V  S   |V  G   �V  5   W  "   NW     qW     yW     �W     �W     �W     �W     �W  	   �W     �W     �W     X     X     ;X  =   QX     �X     �X     �X     �X     �X     �X     �X     Y  	   (Y     2Y     CY     SY     jY     sY     Y     �Y     �Y     �Y     �Y  ,   �Y  ,   Z     3Z     RZ  �   ^Z     [[     d[  $   p[     �[     �[  
   �[     �[     �[  "   �[     �[     \     6\     Q\  &   n\  (   �\     �\     �\  	   �\     �\     ]     ]     &]     /]     4]     M]  (   b]  6   �]     �]     �]     �]  2   �]      0^  K   Q^  '   �^     �^  
   �^     �^     �^     �^  )   _     ._     A_     O_     `_     n_     {_     �_     �_  5   �_     �_     �_     �_     `     `     !`      )`     J`  
   g`     r`  
   �`  2   �`     �`     �`  
   �`     �`     a     a     9a     Ta     ka     �a     �a  *   �a     �a  	   �a     �a     �a     �a  -   �a  *   b  "   Hb  +   kb  h   �b      c  �   c  1   �c  [   �c     5d     <d  �   Hd  C   �d  @   "e  ]   ce     �e  	   �e     �e  @   �e  .   =f     lf     zf     �f     �f     �f     �f     �f     �f     �f  #   �f     g     *g     Ag     ^g  2   lg  
   �g     �g     �g     �g     �g     �g     �g     �g  .   �g     h     $h  6   +h  6   bh  5   �h  :   �h     
i  	   i  (   'i  5   Pi     �i     �i     �i  ;   �i        R                       K       [  �           X         s  �      �     �       @      �           Q  f  _       B      �   �   �   d  �   �   �   F  �  �   n          �   |  ,       9       j   a   �  s   �   W   �  u    1   _  T  p  1         �      j      �         #              �   t       @   i                  �       /   ]   �      n   4             �   4  .           P  �   )   x  �   �   �   '  C  $  5         �     N   {   D      �  �   X  �   �   �                       g           �   �      �   �          H  �   �   w       W  2       v   �                 h   V  �   b      G  �   P       �   o  w         "  �   �       
  5           *  Q   *   ,     o   �   m  �      R  E      �  �   �   �      J  A  $        �      �   �   �   r  �   �       �      �   q       >     �   �   :   �   �  <  ?  +  S      &   �   B  �      �   �       %  �   (   �       �           �   �       �      �       �   �   O  	   e      �   k  b   +         8   
       �       ;       �   r   �       d   �       �   E                �       �      �  K  !  f   �  7           �   �   `        �           >       l           �   �   �   ~   C   J   �           '     S       x   0  �   %   3         z      "       A       ^                M   [       �   �   }          u       l  �   �  )  O   �      9  �   =   -            2  �   :      �      �   ~  �      !   �   �   �   ;  �   H   L         �   �      z   �   �   �  6    �  �  �              \       �   8       L   <       U   -   p                �                   �   �         	          7  �   3  U      F   V       Y  \  �   �   #              ]      M  I         �   �       �   ^       �   q  �   .   �  Z      �   Y           �   |       G   �      �   �               k           y  �    m   D  �          &  �  �       /      �   Z   �  t  �       �   6   �   =              �   g  0   �   i  c  {  �  }   N  ?   �   �   �   y   c       �   h  �   `   �   �   �       �                 a  e   v  I        (      T            �    	Internal encoding: %s
 	None registered.
 	method %s of %s %i rows (%i selected), %i columns %s %s < %s %s (current) &About... &Add Image Layer... &Add Layer... &Bottom &Close... &Database Connections... &Duplicate &File &Full extent &Full layer extent &Full selection extent &Help &Hide &Identify &Join Table... &Join... &Label &Layer &Lower &Map &New Session &Open Session... &Open... &Pan &Properties... &Raise &Remove Layer &Rename... &Save Session &Show &Show... &Table &Top &Unjoin Table... &Visible &Zoom in - not available <None> <Unknown> About Thuban Add Add &Database Layer... Add Database Add Image Layer Add Layer Add Layer from database Add a new database layer to active map Add a new image layer to the map Add a new layer to the map Add to List Add to Selection Add/Remove labels Airy All Files (*.*) All Files (*.*)|*.* Alternative Path An unhandled exception occurred:
%s
(please report to http://thuban.intevation.org/bugtracker.html)

%s Apply to Range Authors:
 Available Available Projections Based on the data from the table and the input
values, the exact quantiles could not be generated.

Accept a close estimate? Bessel 1841 Blue Ramp Both parameters must have a drive letter Bottom Layer CSV Files (*.csv)|*.csv| Can not propose: No bounding box found. Can't open the database table '%s' Can't open the file '%s'. Cancel Cannot import the thubanstart module
 Central Meridian: Change Change Fill Color Change Line Color Choose layer from database Clarke 1866 Clarke 1880 Classification Classification range is not a number! Close Close Table Close Window Close one or more tables from a list Color Scheme: Compiled for:
 Connection '%s' already exists Copy of `%s' Copyright %s
 Could not read "%s": %s Currently using:
 Custom Ramp DB Connection Parameters DBF Files (*.dbf) DBF Files (*.dbf)|*.dbf| DEFAULT Data Type: %s Database Management Database Name: Databases Decimal Default Degrees Deprecated Details on the registered extensions:

 Developers:
 Dock Driver: %s Duplicate selected layer E&xit E&xport Edit Edit Layer Properties Edit Symbol Edit the properties of the selected layer Ellipsoid: End: Error Error in projection "%s": %s Exit Exit Thuban now Export Export Map Export Selection Export Table To Export the map to file Export... Extension: %s Extensions:
 Extent (lat-lon): Extent (lat-lon): (%g, %g, %g, %g) Extent (lat-lon): None Extent (projected): (%g, %g, %g, %g) False Easting: False Northing: Field Field: Field:  Field: %s Filename: Filename: %s Fill Finish working with Thuban Fix Border Color Found the following as an alternative for '%s':
%s

Please confirm with Yes or select alternative with No. French GDAL image information unavailable. See About box for details. GRS 1980 (IUGG, 1980) Generate Generate Class Generate Classification Generate: Geographic Geometry Column German Green Ramp Green-to-Red Ramp Grey Ramp Hidden Hide Layer Hostname: Hot-to-Cold Ramp Hungarian ID Column Identify Shape Image Properties Import Import... Info Info about Thuban authors, version and modules Initialization not yet requested. Initialization successful. Integer International 1909 (Hayford) Invalid color specification %s Invalid hexadecimal color specification %s Italian Join Join Failed Join Layer with Table Join Tables Join and attach a table to the selected layer Join failed:
  %s Join two tables creating a new one Label Label Layer: %s Label Values Labels Lambert Conic Conformal Latitude of first standard parallel: Latitude of origin: Latitude of second standard parallel: Latitude: Layer '%s' Layer Projection: %s Layer Properties Layer Table: %s Layer Title: Layer Type: %s Lead Developer:
 Legend Library not available Line Color Line Width:  Line Width: %s Longitude: Lower Layer Lower selected layer Maintainers:
 Make selected layer unvisible Make selected layer visible Map Map Projection: %s Map Title: Map: %s Max: Min: Modified Move Down Move Up Multiple Projections selected Name: New No GDAL support because module '%s' cannot be imported. Python exception: '%s' No Projections selected No implementation of get_application_dir available for platform No implementation of relative_filename available for platform No thubanstart module available
 No ~/.thuban directory
 None Normal Number of Bands: %i Number of Classes: Number of Groups: Number of labels: %d OK Opacity: Open Session Open Table Open a DBF-table from a file Open a session file Other Contributors:
 Outer Join (preserves left table records) Password: Pattern Pick the table to rename: Pick the table to show: Pick the tables to close: Port: Portuguese (Brazilian) Preview: Prin&t Print the map Pro&jection... Problem with Quantiles Proceed Projection Projection: Projection: %s Projection: None Projection: Propose UTM Zone Propose Put selected layer to the bottom Put selected layer to the top Quantiles from Table Query Questions and comments can be sent to the following addresses:
	General list (public):
		<thuban-list@intevation.de>
	Developers list (public):
		<thuban-devel@intevation.de>
	Thuban team at Intevation:
		<thuban@intevation.de>
 Radians Raise Layer Raise selected layer Range Re&name ... Red Ramp Refine Selection Remove Remove Database Connection Remove selected layer Rename Layer Rename Map Rename Table Rename one or more tables Rename selected layer Rename the map Replace Selection Retrieve Retrieve From Table Retrieve from Table Reverse Revert Russian Save Session &As... Save Session As Save this session to a new file Save this session to the file it was opened from Scale Factor: Select Color Select Properties Select an alternative data file for %s Select an image file Select layer '%s' and pick a projection using Layer/Projection... Select one or more data files Select... Selection Session Session &Tree Session: %s Set or change the map projection Shapefiles (*.shp) Shapes: %d Shapetype: %s Show EPSG: Show Layer Show Ta&ble Show Table Show one or more tables in a dialog Show the selected layer's table Shown Singleton Size:  Size: %ix%i Size: %s Sort Source Data is in:  Source of Projection: %s Source: %s Southern Hemisphere Spanish Specify projection for selected layer Start a new session Start: Stepping: Stop Identify Mode Submenu %s doesn't exist Switch to map-mode 'identify' Switch to map-mode 'pan' Switch to map-mode 'zoom-in' Switch to map-mode 'zoom-out' Symbol Table Title: Table not compatible with shapestore. Table: Table: %s Tables Take Text The Default group cannot be removed. The Dialog named %s is already open The connection %s
is still in use The current map extent center lies in UTM Zone The current session contains Image layers,
but the GDAL library is not available to draw them. The following error occured:
 The joined table has %(joined)d rows but it must have %(needed)d rows to be used with the selected layer The session has been modified. Do you want to save it? This is the wxPython-based Graphical User Interface for exploring geographic data Thuban Thuban - %s Thuban does not know the parameters
for the current projection and cannot
display a configuration panel.

The unidentified set of parameters is:

 Thuban is a program for exploring geographic data.

 Thuban is licensed under the GNU GPL v>=2 Thuban was compiled with wx %(wxproj-wx)s but wxPython is %(wxPython)s Thuban: Internal Error Title:  Toggle Legend on/off Toggle on/off the session tree analysis window Toggle visibility of selected layer Top Layer Translators:
 Transparent Transverse Mercator Try Undo the last join operation Undock Uniform Distribution Unique Values Universal Transverse Mercator Unknown Unknown command %s Unknown command ID %d Unmodified Unsupported group type in classification Update Use User: Value Visible WGS 84 Warning: %s.%s: %s%s
 Warnings Warnings when reading "%s":

%s Zone: Zoom &out Zoom to the full layer extent Zoom to the full map extent Zoom to the full selection extent first argument must be an absolute filename invalid index lineWidth < 1 no receivers for channel %s of %s receiver %s%s is not connected to channel %s of %s size < 1 unnamed map unnamed session xml field type differs from database! Project-Id-Version: Thuban 1.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-08-18 15:17+0200
PO-Revision-Date: 2007-11-29 22:57-0500
Last-Translator: Daniel Calvelo Aros <dcalvelo@gmail.com>
Language-Team: Thuban <thuban-devel@intevation.de>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 	Codificación interna: %s
 	Ninguna registrada.
 	método %s de %s %i líneas (%i seleccionadas), %i columnas %s %s < %s %s (actual) &Sobre Thuban... Añadir Capa I&magen... &Añadir Capa... &Inferior &Cerrar... Conexiones a Bases de &Datos... &Duplicar &Archivo Área de &todas las capas Área de esta &Capa Cobertura de la &Selección A&yuda &Ocultar &Identificar &Unir Tabla... &Unir... &Etiqueta &Capa &Bajar &Mapa &Nueva Sesión &Abrir Sesión... &Abrir... Des&plazar &Propiedades... &Elevar Elimina&r Capa Cambiar nomb&re... &Guardar Sesión Mo&strar Mo&strar... &Tabla %Superior &Desunir Tabla... &Visible &Zoom+ - no disponible <Ninguno> <Desconocido> Acerca de Thuban Añadir Añadir Capa de Base de &Datos... Añadir Base de Datos Añadir Capa de Imagen Añadir capa Añadir Capa desde una base de datos Añadir una capa de base de datos al mapa activo Añadir una capa imagen al mapa Añadir una capa al mapa Añadir a disponibles Añadir a la selección Añadir/Eliminar etiquetas Airy Todos (*.*) Todos los archivos (*.*)|*.* Ruta alternativa Se activó una excepción no manejada:
%s
(por favior informe del error utilizando http://thuban.intevation.org/bugtracker.html)

%s Aplicar al Rango Autores:
 Disponible Proyecciones disponibles En base a los datos de la tabla y los valores
ingresados, no se pudo generar cuantiles exactos.

¿Acepta un buen estimado?  Bessel 1841 Rampa Azul Ambos parámetros deben contener una letra de unidad Capa Inferior Archivos CSV (*.csv)|*.csv| No se puede proponer: No hay una caja de contorno.XX No se pudo abrir la tabla '%s' No se pudo abrir el fichero '%s' Cancelar No se pudo importar el módulo de inicio thubanstart
 Meridiano Central: Cambiar Cambiar Color de Llenado Cambiar Color de Línea Escoja una capa de la base de datos Clarke 1866 Clarke 1880 Clasificación ¡El rango de la clasificación no es un número! Cerrar Cerrar Tabla Cerrar Ventana Cerrar una o más tablas a partir de una lista Esquema de Colores: Compilado para:
 La conexión '%s' ya existe Copia de `%s' Copyright %s
 No se pudo leer "%s": %s Usando actualmente:
 Rampa Propia Parámetros de Conexión a Base de Datos Archivos DBF (*.dbf) Archivos DBF (*.dbf)|*.dbf| POR OMISIÓN Tipo de Datos: %s Gestión de Bases de Datos Nombre de la Base de Datos: Bases de Datos Decimal Por Omisión Grados Descontinuado Detalles de las extensiones registradas:

 Programadores:
 Estacionar Piloto: %s Duplicar la capa seleccionada &Salir E&xportar Editar Editar Propiedades de la Capa Editar Símbolo Editar las propiedades de la capa seleccionada Elipsoide: Fin: Error Error en la Proyección "%s": %s Salir Salir de Thuban ahora Exportar Exportar Mapa Exportar la selección Exportar Tabla Exportar el mapa a un archivo Exportar... Extensión: %s Extensiones:
 Cobertura (lat-lon): Cobertura (lat-lon): (%g, %g, %g, %g) Cobertura (lat-lon): Ninguna Cobertura (proyectada): (%g, %g, %g, %g) Falso Este: Falso Norte: Campo Campo:  Campo:  Campo: %s Archivo: Archivo: %s Llenado Termina de trabajar con Thuban Fijar Color de Línea Las alternativas siguientes existen para '%s':
%s

¿Confirma su opción? ('No' selecciona la alternativa.) Francés La información de la imagen GDAL no está disponible. Ver detalles en 'Acerca de'. GRS 1980 (IUGG, 1980) Generar Generar Clases Generar Clasificación Generar: Geográfica Columna de geometría Alemán Rampa Verde Rampa Verde a Azul Rampa Gris Oculto Ocultar Capa Nombre de Host: Rampa Cálidos a Fríos Húngaro Columna ID Identificar Propiedades de la Imagen Importar Importar... Información Información sobre los autores, versión y módulos de Thuban Aún no se ha solicitado la inicialización. Inicilaización exitosa. Entero Internacional 1909 (Hayford) Especificación de color %s inválida Especificación hexadecimal de color %s inválida Italiano Unir La unión falló Unir Capa con Tabla Unir Tablas Unir y agregar una tabla a la capa seleccionada La unión falló:
  %s Unir dos tablas en una tabla nueva Etiqueta Capa etiquetada: %s Valores de Etiqueta Etiquetas Lambert Cónica Conforme Latitud del primer paralelo estándar Latitud del orígen: Latitud del segundo paralelo estándar Latitud: Capa '%s' Proyección de la Capa: %s Propiedades de la Capa Tabla: %s Título de la Capa: Tipo de Capa: %s Programador Principal:
 Leyenda Biblioteca no disponible Color de Línea Ancho de Línea:  Ancho de Línea: %s Longitud: Bajar Capa Bajar de nivel la capa seleccionada Encargados del mantenimiento:
 Ocultar la capa seleccionada Hacer visible la capa seleccionada Mapa Proyección del Mapa: %s Título del Mapa:  Mapa: %s Max: Min: Modificado Bajar Elevar Se seleccionó Proyecciones múltiples Nombre: Nueva No hay soporte para GDAL, porque el módulo '%s' no pudo ser importado. La excepción de Python fue '%s' No se selccionó Proyecciones No existe una implementación de nombre de get_application_dir para esta plataforma No existe una implementación de relative_filename para esta plataforma El módulo de inicio thubanstart no está disponible
 No existe el directorio ~/.thuban
 Ninguno Normal Número de Bandas: %i Número de Grupos: Número de Grupos: Número de etiquetas: %d Aceptar Opacidad: Abrir sesión Abrir Tabla Abrir un archivo de tabla DBF Abrir un archivo de sesión Otros Colaboradores:
 Unión externa (conserva los registros de la tabla izquierda) Contraseña: Patrón Elija qué tabla renombrar: Elija qué tablas mostrar: Elija qué tablas cerrar: Puerto: Portugués (Brasileño) Previsualización: Imp&rimir Imprimir el mapa Pro&yección... Problemas en cuantiles Ejecutar Proyección Proyección: Proyección: %s Proyección: Ninguna Proyección: Proponer Zona UTM Proponer Mover la capa seleccionada al nivel inferior Mover la capa seleccionada al nivel superior Cuantiles a partir de la Tabla Seleccionar Envíe preguntas y comentarios a las siguientes direcciones electrónicas:
	Lista de interés general (pública):
		<thuban-list@intevation.de>	Desarrollo de Thuban:
		<thuban-devel@intevation.de>
	Equipo Thuban en Intevation:
		<thuban@intevation.de>
 Radianes Elevar Capa Elevar de nivel la capa seleccionada Rango Cambiar nomb&re... Rampa Roja Refinar la selección Eliminar Eliminar Conexión a Base de Datos Eliminar la capa seleccionada Cambiar el nombre de la Capa Cambiar el nombre del mapa Cambia el nombre de la Tabla Cambiar el nombre de una o más tablas Cambia el nombre de la capa seleccionada Cambiar el nombre del mapa Reemplazar selección Recuperar Tomar de la Tabla Tomar de la Tabla Invertir Revertir Ruso Guardar Sesión &Como... Guardar sesión como Guardar esta sesión en un archivo nuevo Guardar esta sesión en el archivo del que fue abierta Factor de Escala: Seleccione color Seleccionar Propiedades Seleccione un archivo de datos alternativo para %s Seleccionar un fichero de imagen Seleccione la capa '%s' y escoja una proyección usando Capa/Proyección... Seleccione uno o más archivos de datos Seleccione... Selección Sesión A&rbol de Sesión Sesión: %s Definir o cambiar la proyección del mapa Shapefiles (*.shp) Elementos: %d Tipo de capa: %s Mostrar EPSG: Mostrar Capa Mostrar Ta&bla Mostrar Tabla Mostrar una o más tablas Mostrar la tabla de atributos de la capa seleccionada Visible Valor Único Tamaño: Tamaño: %ix%i Tamaño: %s Ordenar Los datos originales están en:  Fuente de la Proyección: %s Fuente: %s Hemisferio Sur Castellano Especificar la proyección de la capa seleccionada Iniciar nueva sesión Inicio: Espaciado: Desactivar Modo Identificar El submenu %s no existe Cambiar a modo 'identificar' Cambiar a modo 'desplazar' Cambiar a modo 'zoom+' Cambiar a modo 'zoom-' Símbolo Título de la Tabla: Tabla no compatible con almacén de capas. Tabla: Tabla: %s Tablas Tomar Texto El grupo por omisión no puede ser eliminado. La Ventana de Diálogo %s ya está abierta La Conexión %s
está siendo usada El centro del mapa actual está en zona UTM La sesión actual contiene capas de imagen,
pero la biblioteca GDAL no está disponible para dibujarlas. Ocurrió el error siguiente:
 La table que está uniendo tiene %(joined)d líneas pero debe tener %(needed)d líneas para poder ser utilizada con la capa seleccionada La sesión ha sido modificada. ¿Desea guardarla? Esta es la Interfase Gráfica basada en wxPython para la exploración de datos geográficos Thuban Thuban - %s Thuban desconoce los parámetros 
de la proyección actual y no puede 
mostrar un panel de configuración.

El juego de parámetros desconocido es:

 Thuban es un programa para la exploración de datos geográficos.

 Thuban es distribuido bajo la licencia GPL de GNU, versión >= 2 Thuban fue compilado para wx versión %(wxproj-wx)s pero wxPython es la versión %(wxPython)s Thuban: Error Interno Título:  Activar/desactivar leyenda Activar/desactivar la ventana de análisis del árbol de sesión Cambiar la visibilidad de la capa seleccionada Capa Superior Traductores:
 Transparente Mercator Transversa Probar Deshacer la última unión Liberar Distribución Uniforme Valores únicos Mercator Transversa Universal (UTM) Desconocido Comando Desconocido %s ID de comando desconocida %d No modificado Tipo de Grupo no implementado en la clasificación Actualizar Usar Usuario: Valor Visible WGS 84 Advertencia: %s.%s: %s%s
 Alertas Se detectó alertas en la lectura de "%s":

%s Zona: Z&oom- Amplía el mapa para cubrir el área de la capa actual Amplía el mapa para cubrir el área de todas las capa Amplía el mapa para cubrir el área de la selección el primer argumento debe ser un nombre de fichero absoluto índice no válido ancho < 1 no hay receptores para el canal %s de %s el receptor %s%s no está conectado al canal %s de %s tamaño < 1 mapa sin título sesión sin nombre ¡El tipo de campo xml es diferente al de la base de datos! 